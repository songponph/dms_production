
install rvm:

    bash -s stable < <(curl -s https://raw.github.com/wayneeseguin/rvm/master/binscripts/rvm-installer )

install jruby:

    rvm install jruby

use jruby:

    rvm use jruby


2016-10-03

curl https://raw.githubusercontent.com/rvm/rvm/master/binscripts/rvm-installer | bash -s stable

install jruby:

    rvm install jruby-bin-1.7.4

use jruby:

    rvm use --default jruby-1.7.4
