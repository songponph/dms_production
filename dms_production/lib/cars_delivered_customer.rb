require "dms"
require "erp"
require "pp"
require "importer"
require "date"

class Importer_CarsDeliveredCustomer < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        #puts "query get", number ,date_from, date_to
        cause= date_from!=false ?  " AND i.REGISTRATION_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND i.REGISTRATION_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( i.FRAME_NO='#{number}' or i.INVOICE_NO='#{number}' or i.BOOKING_NO='#{number}' ) ": "")
        cause << (last_update!=false ?  " AND i.LAST_UPDATE_DATE>='#{last_update}' " : "")
        #query="select
            #i.REGISTRATION_DATE as DATE_DONE
            #,i.INVOICE_NO,i.BOOKING_NO,i.FRAME_NO
            #,i.CUSTOMER_NO,d.DELIVERY_NO ,d.DELIVERY_DATE as DATE,i.MTOC_CD
                       #from ST_INVOICE i,st_delivery d
                       #where i.FRAME_NO=d.FRAME_NO and i.BOOKING_NO=d.BOOKING_NO
                            #"+cause+"
                       #order by i.INVOICE_NO"
        query="select
            i.REGISTRATION_DATE as DATE_DONE
            ,i.INVOICE_NO,i.BOOKING_NO,i.FRAME_NO
            ,i.CUSTOMER_NO,i.REGISTRATION_DATE as DATE,i.MTOC_CD
                       from ST_INVOICE i
                       where i.FRAME_NO is not null
                            "+cause+"
                       order by i.INVOICE_NO"

        return query
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)

        res=@dms.get q
        raise "Delivery not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q=self.query_get false,date_from,date_to
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_cars_delivered_customer",date_from,date_to
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="SI%"
        for row in rows
            invoice=row["INVOICE_NO"].strip+" "+row["BOOKING_NO"].strip.upcase
            dms_docnos.push(invoice)
        end
        pp dms_docnos
        puts "################################################"
        erp_docnos=@erp.get_picking_docnos inv_no,"out",date_from,date_to,@company_id
        pp erp_docnos
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        #pick_no=row["DELIVERY_NO"].strip
        booking_no=row["BOOKING_NO"].strip.upcase
        inv_no=row["INVOICE_NO"].strip
        origin=inv_no+" "+booking_no
        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "UPDATED : Picking already created: #{origin}"
            #@erp.update_picking(res,inv_no,@company_id)
            return
        end

        date=row["DATE"].strftime "%Y-%m-%d %H:%M:%S"
        date_done=row["DATE_DONE"].strftime "%Y-%m-%d %H:%M:%S"

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)
        if not address_id
            address_id=@erp.get_partner_address(partner_id,:required=>true)
        end

        invoice_id=@erp.get_invoice_id(inv_no,@company_id)

        frame_no=row["FRAME_NO"].strip

        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true

        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        prodlot_id=@erp.get_prodlot frame_no
        res=@dms.get_car_model frame_no
        model_no=res["MTOC_CD"].strip
        model_name=res["MTO_NM"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        car_analytic=@dms.get_car_product model_no
        inport_flg=car_analytic["INPORT_FLG"]
        analytic_id=@erp.get_analytic_from_code "COST_" ,model_name,@company_id,inport_flg

        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done"=>date_done,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>"out",
            "address_id"=>address_id,
            "stock_journal_id"=>@erp.get_stock_journal("CARS_DELIVERED_CUSTOMER_JOURNAL",@company_id), #CAR_SI
            "invoice_id" => invoice_id,
            "move_lines"=>[[0,0,{
                    "name"=>frame_no,
                    "date_expected"=>date,
                    "date"=>date_done,
                    "product_id"=>product_id,
                    "product_uom"=>@erp.get_uom("PCE",:required=>true),
                    "product_qty"=>1,
                    "location_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
                    "location_dest_id"=>@erp.get_location("Customers",:required=>true),
                    "state"=>"assigned",
                    "analytic_id"=>analytic_id,
                    "prodlot_id"=>prodlot_id,
                    "booking_id"=>booking_id,
                    "vehicle_id"=>vehicle_id
                    }]]
        }
        pp pick_vals
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created picking #{inv_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        return pick_id
    end
end
