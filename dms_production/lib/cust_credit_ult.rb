require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CustCreditUltimate < Importer
    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND c.CREDIT_NOTE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND c.CREDIT_NOTE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( c.CREDIT_NOTE_NO='#{number}' or c.UC_NO='#{number}' or c.INVOICE_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND c.LAST_UPDATE_DATE>='#{last_update}' ": "")
        query="SELECT c.*,u.SECTION_CD
        FROM
        RU_ULTIMATE_CREDIT c,
        RU_ULTIMATE_CARE i,
                 CM_STAFF u

        where
            i.uc_no=c.uc_no
        and i.user_id=u.user_id
        " + cause +"
        order by c.CREDIT_NOTE_NO"
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["CREDIT_NOTE_NO"].strip}

        #check all invoice with that origin
        erp_docnos=@erp.get_invoice_docnos "",date_from,date_to,"UQ%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        credit_no=row["CREDIT_NOTE_NO"].strip
        inv_no=row["INVOICE_NO"].strip
        uc_no=row["UC_NO"].strip
        origin=credit_no+" "+inv_no+" "+uc_no
        pp(row)

        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end

        inv_no=row["INVOICE_NO"].strip
        customer=@dms.get_ult_partner_no inv_no,:required=>true
        partner_no=customer["CUSTOMER_NO"].strip.upcase
        p partner_no
        pm_no=customer["PRE_INVOICE_NO"].strip
        uc_no=row["UC_NO"].strip
        original_invoice=pm_no+" "+uc_no
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        frame_no=row["FRAME_NO"].strip
        #vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        vehicle_id=@erp.get_vehicle frame_no,@branch_code #XXX: incase wrong frame no input

        original_invoice_id=@erp.get_origin_invoice original_invoice,@company_id,@branch_code,nil,"cust_invoice_ult",pm_no,:required=>true

        res=@erp.execute "account.invoice","read",[original_invoice_id],["address_invoice_id","user_id"]
        address_id=res[0]["address_invoice_id"][0]
        user_id=res[0]["user_id"][0]

        tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
        date=row["CREDIT_NOTE_DATE"].strftime("%m/%d/%y")
        comment=row["MEMO"]
        vals={
            "origin"=>origin,
            "reference"=> credit_no,
            "comment"=>comment,
            "journal_id"=>@erp.get_journal("CUST_CREDIT_ULTCARE_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>"out_refund",
            "account_id" => @erp.get_account("CUST_CREDIT_ULTCARE_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date,
            "doc_date" => date,
            "orig_invoice_id"=> original_invoice_id,
            "address_invoice_id" => address_id,
            "user_id"=> user_id,
            "vehicle_id"=>vehicle_id
           }

        #product=@dms.get_car_model frame_no,:required=>true
        product=@dms.get_car_model frame_no #XXX tempolary not required
        #mtoc_cd=product["MTOC_CD"].strip
        product_id=@erp.get_product "ULTC",:required=>true
        line_name=row["CREDIT_DETAILS"].strip

        income_code = "CUST_CREDIT_ULTCARE_INCOME_SV"
        if row["SECTION_CD"]=="SL"
            income_code = "CUST_CREDIT_ULTCARE_INCOME"
        end
        # income service/sale

        account_id = @erp.get_account(income_code,@parent_company_id,:required=>true)

        lines_= {
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => account_id,
            "name" => line_name,
            "quantity" => 1,
            "price_unit" => row["CREDIT_AMOUNT"],
            "product_id" => product_id,
            "invoice_line_tax_id" => [[6,0,[tax_id]]],
        }

        vals["invoice_line"]=[[0,0,lines_]]
        pp vals
        inv_id=@erp.execute "account.invoice","create",vals
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["CREDIT_TOTAL"]).abs>0.25
            msg="wrong total in #{invoice_name}: #{amt}/#{row["CREDIT_TOTAL"]}"
            @logger.error msg
            raise msg
        end 
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
