require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CarsReturnedBranch < Importer

    def import_by_no(creditnote_no)
        q="SELECT * FROM ST_RORDERINV WHERE INVOICE_CN_NO='#{creditnote_no}' OR INVOICE_NO='#{creditnote_no}'"
        res=@dms.get q
        raise "Invoice not found: #{creditnote_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        q="SELECT * FROM ST_RORDERINV  WHERE INV_CN_PRINT_DATE>='#{date_from}' AND INV_CN_PRINT_DATE<='#{date_to}' AND INVOICE_CN_NO LIKE 'S0%' AND INVOICE_NO LIKE 'SF%'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_RORDERINV WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_CN_NO LIKE 'S0%' AND INVOICE_NO LIKE 'SF%'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q="SELECT INVOICE_NO,INVOICE_CN_NO FROM ST_RORDERINV WHERE INVOICE_CN_NO LIKE 'S0%' AND INV_CN_PRINT_DATE>='#{date_from} 00:00:00' AND INV_CN_PRINT_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_NO"
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="S0%"
        for row in rows
            invoice=row["INVOICE_CN_NO"].strip+" "+row["RCVORDER_NO"].strip+" "+row["INVOICE_NO"].strip
            dms_docnos.push(invoice)
        end
        erp_docnos=@erp.get_picking_docnos inv_no,"internal",date_from,date_to,@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        pick_no=row["INVOICE_CN_NO"].strip
        inv_no=row["INVOICE_NO"].strip
        origin=pick_no+" "+row["RCVORDER_NO"].strip+" "+inv_no

        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "SKIP : Picking already created:#{origin}"
            return
        end

        partner_no=row["CLAIM_CUST_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)
        if not address_id
            address_id=@erp.get_partner_address(partner_id,:required=>true)
        end

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        date_done=row["INV_CN_PRINT_DATE"].strftime("%Y-%m-%d %H:%M:%S")
        date=row["CANCEL_DATE"].strftime("%Y-%m-%d %H:%M:%S")#cannot use

        cancel_cd=row["CANCEL_CD"]
        credit_reason=@dms.get_credit_note_reason cancel_cd,:required=>true

        pick_vals={
            "origin"=>origin,
            "date"=>date_done,
            "date_done"=>date_done,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "partner_id"=>partner_id,
            "type"=>"internal",
            "address_id"=>address_id,
            "stock_journal_id"=>@erp.get_stock_journal("CARS_RETURNED_BRANCH_JOURNAL",@company_id), #CAR_S0
            "note"=>credit_reason,
        }

        frame_no=row["FRAME_NO"].strip
        model=@dms.get_car_model frame_no,:required=>true
        prodlot_id=@erp.get_prodlot frame_no,:required=>true
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        line_vals={
            "name"=>frame_no,
            "date_expected"=>date_done,
            "date"=>date_done,
            "product_id"=>product_id,
            "product_uom"=>@erp.get_uom("PCE",:required=>true),
            "product_qty"=>1,
            "location_id"=>@erp.get_location(@branch_code+"-Transfer",:required=>true),
            "location_dest_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
            "state"=>"assigned",
            "prodlot_id"=>prodlot_id,
            "vehicle_id"=>vehicle_id,
            "price_unit"=>row["TOTAL_CR"],
        }

        pick_vals["move_lines"]=[[0,0,line_vals]]
        pp(pick_vals)
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created picking #{pick_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end
end
