require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CustCreditNotesPreventiveMaintenance< Importer
    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND CREDIT_NOTE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND CREDIT_NOTE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( CREDIT_NOTE_NO='#{number}' or INVOICE_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND LAST_UPDATE_DATE>='#{last_update}' ": "")
        query="SELECT
            CREDIT_NOTE_NO as ref,
            CREDIT_NOTE_DATE as date,
            INVOICE_NO,
            rtrim(rtrim(CREDIT_NOTE_NO)+' '+rtrim(INVOICE_NO)) AS ORIGIN,
            CREDIT_AMOUNT as AMOUNT_UNTAXED ,CREDIT_VAT as AMOUNT_TAX,CREDIT_TOTAL as AMOUNT_TOTAL,
            CREDIT_DETAILS as name,
            MEMO as comment
        FROM RT_PM_CN
        where CREDIT_NOTE_NO is not null
        "+cause+"
        ORDER BY CREDIT_NOTE_NO"
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["ref"].strip}

        #check all invoice with that origin
        erp_docnos=@erp.get_invoice_docnos "",date_from,date_to,"%Q%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        ref =row["ref"].strip
        orig_invoice_no=row["INVOICE_NO"].strip
        origin=row["ORIGIN"]

        res=@erp.execute "account.invoice","search",[["reference","=",ref],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "CN already created: #{ref}"
            return
        end

        # find or create orig invoice
        original_invoice_id=@erp.get_origin_invoice(orig_invoice_no,@company_id,@branch_code,
                                                    nil,"cust_invoice_pm",orig_invoice_no,:required=>true)

        # read require data
        orig_res=@erp.execute "account.invoice","read",[original_invoice_id],["amount_untaxed"]

        # create refund invoice
        journal_id=@erp.get_journal("CUST_INV_PM_JOURNAL_CREDIT",@company_id,:required=>true)
        inv_id=@erp.execute("account.invoice","refund",[original_invoice_id],
                            row["date"].strftime("%Y-%m-%d"),false,"#{row["name"].strip} #{row["comment"].strip}",
                            "credit",{"reference"=>row["ref"].strip,
                                      "journal_id"=>journal_id})[0]

        @logger.info "invoice created: #{ref} =>#{inv_id}"
        # update amount to refund if amount is not equeal
        if orig_res[0]["amount_untaxed"]!=row["AMOUNT_UNTAXED"]
            puts " credit note diff amount"
        end

        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["AMOUNT_TOTAL"]).abs>0.25
            msg="wrong total in #{ref}: #{amt}/#{row["AMOUNT_TOTAL"]}"
            @logger.error msg
            raise msg
        end

        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
