require "dms"
require "erp"
require "importer"

class Importer_RedPlate < Importer
    def import_by_no(doc_no)
        puts "Importer_RedPlate.import_by_no",doc_no
        q="SELECT * FROM ST_RENTAL WHERE TLP_NO='#{doc_no}' OR SHEET_NO='#{doc_no}' OR BOOKING_NO='#{doc_no}'"
        res=@dms.get q
        raise "Red plate not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_RedPlate.import_by_date",date_from,date_to
        q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{date_from}' AND LAST_UPDATE_DATE<='#{date_to}'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_RedPlate.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:00"
        q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        puts "Importer_RedPlate.import_row"
        plate_no=row["TLP_NO"].strip
        plate_id=@erp.get_red_plate(plate_no)
        if plate_id
            @logger.info "Red plate already created: #{plate_no}"
            return
        end
        product_id=@erp.get_product("RED_PLATE",:required=>true)
        prodlot_id=@erp.get_prodlot(plate_no)
        if not prodlot_id
            puts "creating lot"
            prodlot_vals={
                "name"=>plate_no,
                "product_id"=>product_id,
            }
            prodlot_id=@erp.execute "stock.production.lot","create",prodlot_vals
        else
            puts "lot already exists"
        end

        vals={
            "name"=>plate_no,
            "lot_id"=>prodlot_id,
            "product_id"=>product_id,
        }
        plate_id=@erp.execute "ac.red.plate","create",vals
        @logger.info "Created red plate: #{plate_no}->#{plate_id}"
        plate_id
    end
end
