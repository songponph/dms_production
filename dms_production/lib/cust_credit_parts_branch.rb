require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CustCreditPartsBranch < Importer
    def import_by_no(creditnote_no)
        puts "Importer_CustCreditParts.import_by_no",creditnote_no
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO='#{creditnote_no}'"
        res=@dms.get q
        raise "Invoice not found: #{creditnote_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        puts "Importer_CustCreditParts.import_by_date",date_from,date_to
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE DELIVERED_DATE>='#{date_from}' AND DELIVERED_DATE<='#{date_to}' AND IO_NO LIKE 'PU%' AND INVOICE_NO LIKE 'PZ%'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustCreditParts.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PU%' AND INVOICE_NO LIKE 'PZ%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows
    end

    def check_import(date_from,date_to)
        puts "Check_import_cust_credit_part_branch",date_from,date_to
        q="SELECT IO_NO FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PU%' AND INVOICE_NO LIKE 'PZ%' AND DELIVERED_DATE>='#{date_from} 00:00:00' AND DELIVERED_DATE<='#{date_to} 23:59:59' ORDER BY IO_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["IO_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_refund",date_from,date_to,"PU%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end 

    def import_row(row)
        puts "Importer_CustCreditParts.import_row"
        pp "row:",row

        io_no=row["IO_NO"].strip
        inv_no=row["INVOICE_NO"].strip
        origin=inv_no+" "+io_no
        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end

        customer=@dms.get_parts_customer(inv_no)
        partner_no=customer["CUSTOMER_NO"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        proforma_inv=@dms.get_proforma_invoice(inv_no)
        order_nos=@dms.get_part_order_nos inv_no

        original_invoice=proforma_inv["PROFORMA_INVOICE"].strip+ " " +inv_no+" "+order_nos.join(" ")
        original_invoice_id=@erp.get_origin_invoice original_invoice,@company_id,@branch_code,"out_invoice","cust_invoice_part_branch",inv_no,:required=>true

        res=@erp.execute "account.invoice","read",[original_invoice_id],["address_invoice_id","user_id"]
        address_id=res[0]["address_invoice_id"][0]
        user_id=res[0]["user_id"][0]


        part=@dms.get_part_no_branch inv_no
        if not part.empty?
            product_id=@erp.get_product part,@branch_code,"part_product",:required=>true
        end

        date=row["DELIVERED_DATE"].strftime("%m/%d/%y")

        lines=[]
        vals={
            "origin"=>origin,
            "reference"=>io_no,
            "journal_id"=>@erp.get_journal("CUST_CREDIT_PART_BRANCH_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>"out_refund",
            "account_id" => @erp.get_account("CUST_CREDIT_PART_BRANCH_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date,
            "doc_date" => date,
            "orig_invoice_id"=> original_invoice_id,
            "address_invoice_id" => address_id,
            "user_id"=>user_id,
           }
        lines+=get_lines(inv_no,product_id)
        raise "No lines in invoice...." if lines.empty?
        vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}
        pp "vals:",vals
        inv_id=@erp.execute "account.invoice","create",vals
        t2=Time.now()
        @logger.info "invoice created: #{io_no}=>#{inv_id}"
        puts "Computing invoice..."
        @erp.execute "account.invoice","button_compute",[inv_id]
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["TOTAL_AMOUNT"]).abs>0.25
            msg="wrong total in #{io_no}: #{amt}/#{row["TOTAL_AMOUNT"]}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
    end

    def get_lines(inv_no,product_id)
        q="
        SELECT PRD.*,PRH.*
        FROM PT_RETURNINVOICE_D PRD
        JOIN PT_RETURNINVOICE_H PRH
        ON PRH.IO_NO = PRD.IO_NO 
        WHERE PRH.INVOICE_NO= '#{inv_no}'"

        rows=@dms.query(q)
        lines=[]
        uom_id=@erp.get_uom "PCE",:required=>true

        for row in rows
            line_name = row["IO_NO"].strip+" "+row["LINE_NO"].to_s()
            res=@erp.execute "product.product","read",[product_id],["name"]
            prod_name=res[0]["name"]
            val = {
                "uos_id" => uom_id,
                "account_id" => @erp.get_account("CUST_CREDIT_PART_BRANCH_INCOME",@parent_company_id,:required=>true),
                "name" => prod_name,
                "price_unit" => row["SALES_PRICE"],
                "quantity" => Float(row["RETURN_INPUT_QTY"]),
                "product_id" => product_id,
            }
            pp "lines:"
            pp val
            lines<<val
        end
        lines
    end
end
