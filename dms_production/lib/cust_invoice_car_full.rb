require "dms"
require "erp"
require "pp"
require "importer"

class Importer_InvoiceCarFull < Importer
    def import_by_no(invoice_no)
        q="SELECT * FROM AT_UNSETTLE_MAIN WHERE INVOICE_NO LIKE 'SI%' and INVOICE_NO='#{invoice_no}'"
        res=@dms.get q
        raise "Booking invoice not found: #{invoice_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM AT_UNSETTLE_MAIN WHERE INVOICE_NO LIKE 'SI%' AND TRAN_DATE>='#{date_from} 00:00:00' AND TRAN_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM AT_UNSETTLE_MAIN WHERE INVOICE_NO LIKE 'SI%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:00"
        q="SELECT * FROM AT_UNSETTLE_MAIN WHERE INVOICE_NO LIKE 'SI%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q="SELECT INVOICE_NO FROM AT_UNSETTLE_MAIN WHERE INVOICE_NO LIKE 'SI%' AND TRAN_DATE>='#{date_from} 00:00:00' AND TRAN_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_NO"
        rows=@dms.query q
        dms_docnos=[]
        for row in rows
            invoice=row["INVOICE_NO"].strip
            if invoice[5,1] == '/' or invoice[3,1] == '/' or invoice[6,1] == '/'
                next
            end
            dms_docnos.push(invoice)
        end
        erp_docnos=@erp.get_invoice_docnos "out_invoice",date_from,date_to,"SI%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row) 
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        t0=Time.now()
        invoice=row["INVOICE_NO"].strip
        if invoice[5,1] == '/' or invoice[6,1] == '/' or invoice[3,1] == '/'
            @logger.info "Skip making invoice for this number...#{invoice}.."
            return
        end

        car_inv=@dms.get_car_invoice invoice,:required=>true
        if car_inv.nil?
            return
        end
        booking_no=car_inv["BOOKING_NO"].strip
        bill_no=car_inv["BILL_NO"].strip
        down_pay=car_inv["DOWN_PAY_CR"]
        total_cr=car_inv["TOTAL_CR"]

        if not booking_no
            raise "Booking not found"
        else
            origin = invoice + " " + booking_no
        end

        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true


        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "SKIPPED : Invoice already created:#{origin}"
            return
        end

        datetime=row["TRAN_DATE"]
        date_invoice=datetime.strftime("%m/%d/%y")

        partner_no=row["CUST_CD"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_row=@dms.get_address_row partner_no,invoice
        address_id=@erp.create_address_invoice_car_full address_row 

        rela_tran_no=row["RELA_TRAN_NO"].strip
        frame_no=row["FRAME_NO"]
        car_model=@dms.get_car_model frame_no
        if not car_model
            @logger.info "Product could not be found......"
            return
        end
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        model_no=car_model["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        res=@erp.execute "product.product","read",[product_id],["name"]
        model_name=res[0]["name"]
        color_name=car_model["COLOR_NM"].strip

        user_code=car_inv["STAFF_CD"].strip
        user_id=@erp.get_user user_code,:required=>true

        inv_val={
            "origin" => origin,
            "reference" => invoice,
            "journal_id" => @erp.get_journal("CUST_INV_CAR_FULL_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_invoice",
            "account_id" => @erp.get_account("CUST_INV_CAR_FULL_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date" => date_invoice,
            "address_invoice_id" => address_id,
            "booking_id"=>booking_id,
            "user_id" => user_id,
            "vehicle_id" => vehicle_id,
        }
        lines=[]

        if rela_tran_no and rela_tran_no[0,2] == 'S3'
            #inv_val["inv_deposit_lines"]=[[0,0,deposit_line_]]
            res=@dms.get_car_invoice_from_booking booking_no,:required=>true
            if res.nil?
                return
            end
            partner_no=res["CUSTOMER_NO"].strip.upcase
            partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
            res1=@erp.execute "res.partner","read",[partner_id],["name"]
            partner_name=res1[0]["name"]

            line_1={
                "account_id" => @erp.get_account("CUST_INV_CAR_FULL_DEDUCT",@parent_company_id,:required=>true),
                "name" => partner_name,
                "quantity" => 1,
                "price_unit" => -(down_pay),
                "product_id" => product_id,
            }
            lines<<line_1
        end

        car_analytic=@dms.get_car_product model_no
        inport_flg=car_analytic["INPORT_FLG"]
        analytic_account_id=@erp.get_analytic_from_code "SALE_" ,model_name,@company_id,inport_flg

        tax_id= @erp.get_tax("OUT_VAT7_INCL",:required=>true)

        line_2 = {
            "account_id" => @erp.get_account("CUST_INV_CAR_FULL_INCOME",@parent_company_id,:required=>true),
            "name" => model_name+" "+color_name,
            "product_id" => product_id, 
            "quantity" => 1,
            "account_analytic_id" => analytic_account_id,
            "invoice_line_tax_id" => [[6,0,[tax_id]]],
            "price_unit" => total_cr,
        }

        if rela_tran_no and rela_tran_no[0,2] == 'S3'
            check_amount = total_cr - down_pay
        else
            check_amount = total_cr
        end

        lines<<line_2
        raise "no lines in invoice" if lines.empty?
        inv_val["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}

        inv_id=@erp.execute "account.invoice","create",inv_val
        @logger.info "CREATED: invoice : #{origin}=>#{inv_id}"

        @erp.execute "account.invoice","button_compute",[inv_id]

        #@logger.info "tax computed:"

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total","amount_tax"]
        amt=res[0]["amount_total"]
        if (amt-check_amount).abs>0.25
            msg="wrong total in #{bill_no}: #{amt}/#{check_amount}"
            @logger.error msg
            raise msg
        end

        if row["VAT_CR"] !=res[0]["amount_tax"]
            #print "diff "
            @erp.execute "account.invoice","set_manual_vat",[inv_id],false,row["BALANCE_CR"],row["VAT_CR"],@company_id
        end

        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        #@erp.execute "ac.booking","write",[booking_id],{"vehicle_id"=>vehicle_id}
        return inv_id
    end
end 
