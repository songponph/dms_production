require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CustCreditParts < Importer
    #status=99, cancel
    #30=open

    def import_by_no(creditnote_no)
        puts "Importer_CustCreditParts.import_by_no",creditnote_no
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO='#{creditnote_no}' and status='30'"
        res=@dms.get q
        raise "Invoice not found: #{creditnote_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_CustCreditParts.import_by_date",date_from,date_to
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE REGISTRATION_DATE>='#{date_from} 00:00:00' AND REGISTRATION_DATE<='#{date_to} 23:59:59' AND IO_NO LIKE 'PT%' AND INVOICE_NO LIKE 'PI%'  and status='30' ORDER BY IO_NO,REGISTRATION_DATE"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustCreditParts.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PT%' AND INVOICE_NO LIKE 'PI%' AND LAST_UPDATE_DATE>='#{t0}' and status='30' order by IO_NO,REGISTRATION_DATE"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PT%' AND INVOICE_NO LIKE 'PI%' AND LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_cust_credit_parts",date_from,date_to
        q="SELECT IO_NO FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PT%' AND INVOICE_NO LIKE 'PI%' AND DELIVERED_DATE>='#{date_from} 00:00:00' AND DELIVERED_DATE<='#{date_to} 23:59:59' ORDER BY IO_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["IO_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_refund",date_from,date_to,"PT%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        puts "Importer_CustCreditParts.import_row"
        io_no=row["IO_NO"].strip
        inv_no=row["INVOICE_NO"].upcase.strip
        order_nos=@dms.get_part_order_nos inv_no
        order_no=order_nos.join(" ")
        origin=inv_no+" "+io_no+" "+order_no

        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end

        customer=@dms.get_parts_customer(inv_no)
        partner_no=customer["CUSTOMER_NO"].strip
        puts "partner_no #{partner_no}"
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        proforma_inv=@dms.get_proforma_invoice(inv_no)
        pm_no=proforma_inv["PROFORMA_INVOICE"].strip
        original_invoice=pm_no+ " " +inv_no+ " " +order_no

        original_invoice_id=@erp.get_origin_invoice(inv_no,@company_id,@branch_code,nil,"cust_invoice_parts",inv_no,:required=>true)

        res=@erp.execute "account.invoice","read",[original_invoice_id],["address_invoice_id","user_id"]
        puts "orig invoice",res
        address_id=res[0]["address_invoice_id"][0]
        user_id=res[0]["user_id"][0]


        #check customer type
        part_cust=@dms.get_parts_customer inv_no,:required=>true

        cust_type=part_cust["CUSTOMER_TYPE"]
        if cust_type.match /^610|^615|^620|^622|^623|^625/
            type="dealer"
        else
            type="customer"
        end

        comment=""
        date=row["REGISTRATION_DATE"].strftime("%m/%d/%y")
        comment+=row["MEMO1"]+" "+row["MEMO2"]+" "+row["MEMO3"]

        lines=[]
        vals={
            "origin"=>origin,
            "reference"=>io_no,
            "comment"=>comment,
            #TODO: Checking which journal
            "journal_id"=>@erp.get_journal("CUST_CREDIT_PART_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>"out_refund",
            "account_id" => @erp.get_account("CUST_CREDIT_PART_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date,
            "doc_date" => date,
            "orig_invoice_id"=> original_invoice_id,
            "address_invoice_id" => address_id,
            "user_id"=>user_id,
           }
        lines+=get_lines(io_no,type)
        raise "No lines in invoice...." if lines.empty?
        vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}
        inv_id=@erp.execute "account.invoice","create",vals
        t2=Time.now()
        @logger.info "invoice created: #{io_no}=>#{inv_id}"
        puts "Computing invoice..."
        @erp.execute "account.invoice","button_compute",[inv_id]
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["TOTAL_AMOUNT"]).abs>0.25        
            msg="wrong total in #{io_no}: #{amt}/#{row["TOTAL_AMOUNT"]}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
    end

    def get_lines(io_no,type)
        q="
        SELECT PRD.*,PRH.*,T2.*
        FROM PT_RETURNINVOICE_D PRD
        JOIN PM_SPMAST T2 ON PRD.PART_NO = T2.PART_NO
        JOIN PT_RETURNINVOICE_H PRH ON PRH.IO_NO = PRD.IO_NO 
        WHERE PRH.IO_NO= '#{io_no}'"

        rows=@dms.query(q)
        lines=[]
        uom_id=@erp.get_uom "PCE",:required=>true
        tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
        #analytic_id=@erp.get_analytic_account "PT_COUNTER",@company_id,:required=>true
        for row in rows
            line_name = row["IO_NO"].strip+" "+row["LINE_NO"].to_s()
            account_id=get_account row,type
            analytic_id=get_analytic_account row
            part_no=row["PART_NO"].strip
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            part_name=@dms.get_part_name part_no,:required=>true

            res=@erp.execute "product.product","read",[product_id],["name"]
            prod_name=res[0]["name"]
            if row["RETURN_INPUT_QTY"] == 0 #cancel return case
                qty=row["POSSIBLE_QTY"]
            else
                qty=row["RETURN_INPUT_QTY"]
            end
            val = {
                "uos_id" => uom_id,
                "account_id" => account_id,
                "name" => prod_name,
                "price_unit" => row["SALES_PRICE"],
                "product_id"=>product_id,
                "quantity" => qty,
                "invoice_line_tax_id" => [[6,0,[tax_id]]],
                "account_analytic_id"=>analytic_id
            }
            lines<<val
        end
        lines
    end

    def get_account(row,type)
        part_type=row["PART_TYPE"]
        if type=="customer"
            code="CUST_CREDIT_PART_INCOME_OTHER"
            if part_type=="6"
                code="CUST_CREDIT_PART_INCOME_ACC"
            end
        else
            code="CUST_CREDIT_PART_DEALER_INCOME"
            if part_type=="6"
                code="CUST_CREDIT_PART_DEALER_INCOME_ACC"
            end
        end
        @erp.get_account code,@parent_company_id,:required=>true
    end

    def get_analytic_account(row)
        part_type=row["PART_TYPE"]
        part_type_group=case part_type
        when "1","2","3","4" then "GEN"
        when "5" then "OIL"
        when "6" then "ACC"
        else
            raise "Invalid part_type: #{part_type}"
        end
        code="CT_"+part_type_group
        @erp.get_analytic_account code,@company_id,:required=>true
    end
end
