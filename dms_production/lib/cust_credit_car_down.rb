require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustCreditCarDown < Importer

    def import_by_no(doc_no)
        puts "Importer_CustCreditCarDown.import_by_no",doc_no
        q="SELECT * FROM ST_INVOICE WHERE BILL_CAN_NO='#{doc_no}'"
        res=@dms.get q
        raise "Customer invoice for car to other dealer not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_CustCreditCarDown.import_by_date",date_from,date_to
        q="SELECT * FROM ST_INVOICE WHERE BILL_CAN_PRINT_DATE>='#{date_from} 00:00:00' AND BILL_CAN_PRINT_DATE<='#{date_to} 23:59:59' AND BILL_CAN_NO LIKE 'S4%' ORDER BY BILL_CAN_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustCreditCarDown.import_daily"
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM ST_INVOICE WHERE BILL_CAN_NO LIKE 'S4%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_INVOICE WHERE BILL_CAN_NO LIKE 'S4%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_S2",date_from,date_to
        q="SELECT BILL_CAN_NO FROM ST_INVOICE WHERE BILL_CAN_NO LIKE 'S4%' AND BILL_CAN_PRINT_DATE>='#{date_from} 00:00:00' AND BILL_CAN_PRINT_DATE<='#{date_to} 23:59:59' ORDER BY BILL_CAN_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["BILL_CAN_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_refund",date_from,date_to,"S4%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        puts "Importer_CustInvoiceClaimFreeService.import_row"
        pp "row:",row
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        puts "Customer credit note for car down payment....."
        origin=row["BILL_CAN_NO"].strip + " " + row["BILL_NO"].strip+ " " + row["BOOKING_NO"].strip

        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created:#{origin}"
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        booking_no=row["BOOKING_NO"].strip
        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true

        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,:required=>true

        date_invoice=row["BILL_CAN_PRINT_DATE"].strftime("%m/%d/%y")

        original_invoice=row["BILL_NO"].strip + " " + row['BOOKING_NO'].strip + " " + row["INVOICE_NO"].strip
        bill_no=row["BILL_NO"].strip
        original_invoice_id=@erp.get_origin_invoice original_invoice,@company_id,@branch_code,"out_invoice","cust_invoice_car_down",bill_no,:required=>true

        res=@erp.execute "account.invoice","read",[original_invoice_id],["address_invoice_id","user_id"]
        address_id=res[0]["address_invoice_id"][0]
        user_id=res[0]["user_id"][0]

        if not row["FRAME_NO"].empty?
            frame_no=row["FRAME_NO"].strip
            vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        end

        invoice_name=row["BILL_CAN_NO"].strip
        cancel_cd=row["CANCEL_CD"]
        credit_reason=@dms.get_credit_note_reason cancel_cd,:required=>true

        vals_={

            "origin" => origin,
            "reference" => invoice_name,
            "comment"=>credit_reason,
            "journal_id" => @erp.get_journal("CUST_CREDIT_CAR_DOWN_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_refund",
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_DOWN_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" =>date_invoice,
            "doc_date"=>date_invoice,
            "booking_id" => booking_id,
            "user_id"=>user_id,
            "orig_invoice_id" => original_invoice_id,
            "address_invoice_id" => address_id,
        }

        if vehicle_id
            vals_["vehicle_id"]=vehicle_id
        end

        car_name=@dms.get_car_name row["MTOC_CD"],:required=>true

        lines_= {
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_DOWN_INCOME",@parent_company_id,:required=>true),
            "name" => car_name,
            "quantity" => 1,
            "price_unit" => row["DOWN_PAY_CR"],
            "product_id" => product_id
        }

        vals_["invoice_line"]=[[0,0,lines_]]

        pp "vals:",vals_
        pp "lines:",lines_

        inv_id=@erp.execute "account.invoice","create",vals_
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["DOWN_PAY_CR"]).abs>0.25
            msg="wrong total in #{invoice_name}: #{amt}/#{row["DOWN_PAY_CR"]}"
            @logger.error msg
            raise msg
        end
        puts "Posting invoice............."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        #if vehicle_id
            #@erp.execute "ac.booking","write",[booking_id],{"vehicle_id"=>vehicle_id}
        #end
        return inv_id
    end
end
