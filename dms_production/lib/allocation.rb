require "dms"
require "erp"
require "pp"
require "importer"

class Importer_Allocation < Importer
    def import_by_no(doc_no)
        puts "Importer_Booking.import_by_no",doc_no
        q="SELECT * FROM ST_ALLOCATION WHERE BOOKING_NO='#{doc_no}' OR ORDER_NO='#{doc_no}'"
        res=@dms.get q
        raise "Allocation not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_Booking.import_by_date",date_from,date_to
        q="SELECT * FROM ST_ALLOCATION WHERE ALLOCATION_DATE>='#{date_from} 00:00:00' AND ALLOCATION_DATE<='#{date_to} 23:59:59'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_Booking.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM ST_ALLOCATION WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:00:00"
        q="SELECT * FROM ST_ALLOCATION WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_allocation",date_from,date_to
        q="SELECT * FROM ST_ALLOCATION WHERE ORDER_NO IS NOT NULL AND LAST_UPDATE_DATE>='#{date_from} 00:00:00' AND LAST_UPDATE_DATE<='#{date_to} 23:59:59'"
        rows=@dms.query q
        dms_docnos=Array.new
        booking_docnos=Array.new
        full_docnos={}
        for row in rows
            booking=row["BOOKING_NO"].strip
            order_no=row["ORDER_NO"].strip
            booking_docnos.push(booking)
            frame_no=self.get_frame_no_from_purchase order_no,:required=>true
            origin=booking+" "+ frame_no
            full_docnos[frame_no]=origin
            dms_docnos.push(frame_no)
        end
        pp full_docnos
        puts "#################################################"
        erp_docnos=@erp.get_frame_nos_from_booking booking_docnos
        pp erp_docnos
        puts "##################################################"
        miss=check_missing dms_docnos,erp_docnos 
        pp miss
        miss_full=[]
        for i in miss
            miss_full=full_docnos.select {|k,v| k==i}
        end
        puts "################################################"
        pp miss_full
        return miss_full
    end

    def import_row(row)
        booking_no=row["BOOKING_NO"].strip.upcase

        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true

        if row["ORDER_NO"].nil? or row["ORDER_NO"].empty?
            @logger.info "SKIPPED : Order number not created "
            return
        end
        order_no=row["ORDER_NO"].strip
        frame_no=self.get_frame_no_from_purchase order_no,:required=>true
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        @erp.execute "ac.booking","write",[booking_id],{"vehicle_id"=>vehicle_id}
        @logger.info "booking updated: #{booking_no}"
    end

    def get_frame_no_from_purchase(order_no,options={})
        puts "DMS.get_frame_no_from_purchase #{order_no}"
        q="
SELECT *
FROM ST_PURCHASE
WHERE RECEIVING_NO='#{order_no}'
"
        row=@dms.get(q)
        if not row
            raise "Frame number cannot be found for this order_no: #{order_no}" if options[:required]
            return nil
        end
        frame_no=row["FRAME_NO"].strip
        return frame_no
    end
end 
