require "dms"
require "erp"

class Importer_Vehicle < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND LAST_UPDATE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND LAST_UPDATE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND FRAME_NO='#{number}' ": "")
        cause << (last_update!=false ? " AND LAST_UPDATE_DATE>='#{last_update}' ": "")
        query="SELECT FRAME_NO,MTOC_CD,ENGINE_NO
                FROM SM_VEHICLEINFO
                WHERE ACTIVE_FLG=1 and FRAME_NO!=''
                    and FRAME_NO is not null "+cause
        return query
    end

    def import_by_no(frame_no)
        q=self.query_get(frame_no)
        res=@dms.get q
        raise "Vehicle not found: #{frame_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        #t0=(Time.now+3600*24*2).strftime "%Y-%m-%d"
        #q=self.query_get(false,t0,false)
        #rows=@dms.query q
        #self.import_rows rows,progress
        return
    end

    def import_realtime(from_t,progress=nil)

        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        q = self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no
        model_no=row["MTOC_CD"].strip

        if vehicle_id
            @logger.info "SKIPPED : Vehicle already created: #{frame_no}"

            vehicle_res =@erp.execute("ac.vehicle","read",vehicle_id,["product_id"])
            product_id=@erp.get_product model_no,:required=>true
            if vehicle_res["product_id"][0]!=product_id
                @logger.info "UPDATED: Vehicle product_id change: #{frame_no}"
                @erp.execute("ac.vehicle","write",vehicle_id,{"product_id"=>product_id})
            end

            return
        end

        product_id=@erp.get_product model_no,:required=>true
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        prodlot_id=@erp.get_prodlot frame_no
        if not prodlot_id
            prodlot_vals={
                "name"=>frame_no,
                "product_id"=>product_id,
            }
            prodlot_id=@erp.execute "stock.production.lot","create",prodlot_vals
        end

        vehicle_vals={
            "name"=>frame_no,
            "lot_id"=>prodlot_id,
            "product_id"=>product_id,
            "engine_no"=>(row["ENGINE_NO"] or "").strip,
        }
        vehicle_id=@erp.execute "ac.vehicle","create",vehicle_vals
        @logger.info "created vehicle #{frame_no} => #{vehicle_id}"
        return vehicle_id
    end
end
