require "dms"
require "erp"
require "importer"
require "pp"
require "date"

class Importer_CarsReturnedCustomer < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND i.CANCEL_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND i.CANCEL_DATE<='#{date_to} 23:59:59' ": "")
        cause <<  (number!=false ?  " AND i.INVOICE_CN_NO='#{number}' ": "")
        cause <<  (last_update!=false ?  " AND i.LAST_UPDATE_DATE>='#{last_update}' " : "")

        # cancel in the same date + 1 min
        # cancel difference date
        query=" select
            case
            when
            convert(varchar,i.REGISTRATION_DATE,112) = convert(varchar,i.CANCEL_DATE ,112)
            then DATEADD(n,1,i.REGISTRATION_DATE)
            when convert(varchar,i.REGISTRATION_DATE,112) < convert(varchar,i.CANCEL_DATE ,112)
            then i.CANCEL_DATE
            end as DATE_DONE
            ,i.INVOICE_CN_NO,i.INVOICE_NO,i.BOOKING_NO,i.FRAME_NO
            ,i.CUSTOMER_NO,d.DELIVERY_NO ,d.DELIVERY_DATE as DATE
            ,i.MTOC_CD,i.CANCEL_CD,p.CARPRICE_CR as PRICE_UNIT
                       from ST_INVOICE i
                       left join ST_delivery d on (i.FRAME_NO = d.FRAME_NO and i.BOOKING_NO = d.BOOKING_NO)
                       left join ST_PURCHASE p on (i.FRAME_NO = p.FRAME_NO)
                       where i.INVOICE_CN_NO like 'S1%'
                            and i.ACTIVE_FLG=0
                            "+cause+"
                       order by i.INVOICE_CN_NO"
        return query
    end

    def import_by_no(number)
        q=self.query_get(number)
        res=@dms.get q
        raise "Picking not found: #{number}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        q=self.query_get false, date_from, date_to
        rows=@dms.query q
        if rows.empty?
            @logger.info "SKIP: Cust Return Car Picking No data to import: #{date_from} - #{date_to}"
            return
        end
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q=self.query_get false, date_from=date_from , date_to=date_to

        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="S1%"
        for row in rows
            invoice=row["INVOICE_CN_NO"].strip+" "+row["INVOICE_NO"].strip+" "+row["BOOKING_NO"].strip
            dms_docnos.push(invoice)
        end
        erp_docnos=@erp.get_picking_docnos inv_no,"in",date_from,date_to,@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        picking_id=self.create_picking(row)
        return picking_id
    end

    def create_picking(row)
        inv_no=row['INVOICE_NO'].strip
        booking_no=row["BOOKING_NO"].strip
        cn_no=row["INVOICE_CN_NO"].strip

        origin=cn_no + " " + inv_no + " " + booking_no

        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "Picking already created try to update invoice_id:#{origin}"
            #@erp.update_picking(res,cn_no,@company_id)
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)
        if not address_id
            address_id=@erp.get_partner_address(partner_id,:required=>true)
        end

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true

        date = Time.now.strftime("%Y-%m-%d %H:%M:%S")
        if row["DATE"]!=nil
            date=row["DATE"].strftime "%Y-%m-%d %H:%M:%S"
        end

        date_done=row["DATE_DONE"].strftime "%Y-%m-%d %H:%M:%S"

        frame_no=row["FRAME_NO"]
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        credit_reason=@dms.get_credit_note_reason row["CANCEL_CD"],:required=>true

        res=@dms.get_car_model frame_no
        model_name=res["MTO_NM"]
        model_no=res["MTOC_CD"].strip
        car_analytic=@dms.get_car_product model_no
        inport_flg=car_analytic["INPORT_FLG"]
        analytic_id=@erp.get_analytic_from_code "SALE_" ,model_name,@company_id,inport_flg

        prodlot_id=@erp.get_prodlot frame_no,:required=>true
        invoice_id=@erp.get_invoice_id(cn_no,@company_id)

        pick_vals={
            "origin" => origin,
            "note" => credit_reason,
            "stock_journal_id"=>@erp.get_stock_journal("CARS_RETURNED_CUSTOMER_JOURNAL",@company_id), #CAR_S1

            "company_id" => @company_id,
            "type" => "in",
            "date" =>date,
            "date_done" =>date_done,
            "address_id" => address_id,
            "move_type"=>"direct",
            "invoice_id"=>invoice_id,
            "move_lines" => [[0,0,{
                "name"=> frame_no,
                "date_expected"=>date,
                "date"=>date,
                "product_id"=>product_id,
                "product_uom"=>@erp.get_uom("PCE",:required=>true),
                "product_qty"=>1,
                "location_id"=>@erp.get_location("Customers",:required=>true),
                "location_dest_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
                "state"=>"assigned",
                "price_unit"=>row["PRICE_UNIT"],
                "prodlot_id"=>prodlot_id,
                "vehicle_id"=>vehicle_id,
                "booking_id"=>booking_id,
                "analytic_id"=>analytic_id,
                } ]]
        }
        pp(pick_vals)
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "CREATED : Created picking return from customer #{origin}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id

        return pick_id
    end
end
