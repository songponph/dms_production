require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CustCreditService < Importer
    def import_by_no(creditnote_no)
        puts "Importer_CustCreditService.import_by_no",creditnote_no
        q="SELECT * FROM RT_CREDIT_H WHERE CREDIT_NOTE_NO='#{creditnote_no}'"
        res=@dms.get q
        raise "Invoice not found: #{creditnote_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_CustCreditServer.import_by_date",date_from,date_to
        q="SELECT * FROM RT_CREDIT_H WHERE CONFIRM_DATE>='#{date_from} 00:00:00' AND CONFIRM_DATE<='#{date_to} 23:59:59' AND CREDIT_NOTE_NO LIKE 'RQ%' ORDER BY CREDIT_NOTE_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustCreditService.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:00:00"
        q="SELECT * FROM RT_CREDIT_H WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM RT_CREDIT_H WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
        return
    end

    def check_import(date_from,date_to)
        puts "Check_import_RQ",date_from,date_to
        q="SELECT CREDIT_NOTE_NO FROM RT_CREDIT_H WHERE CREDIT_NOTE_NO LIKE 'RQ%' AND CONFIRM_DATE>='#{date_from} 00:00:00' AND CONFIRM_DATE<='#{date_to} 23:59:59' ORDER BY CREDIT_NOTE_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["CREDIT_NOTE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_refund",date_from,date_to,"RQ%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end 

    def import_row(row)
        puts "Importer_CustCreditService.import_row"
        pp "row:",row
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)

        #pp(row)
        t0=Time.now()
        credit_no=row["CREDIT_NOTE_NO"].strip
        if credit_no.empty?
            @logger.info "No RQ number,nothing to import, #{credit_no}"
            return
        end
        inv_no=row["TAX_INVOICE_NO"].strip
        ro_no=row["RO_NO"].strip
        origin=credit_no+" "+ro_no+" "+inv_no

        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end

        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        comment=""
        for flg in ["GR","PDI","PM","ACC","AM"]
            comment+=" #{flg}" if row["#{flg}_FLG"]==1
        end
        comment+="\n #{row["MEMO"]}"
        proforma=@dms.get_service_pm(inv_no)
        if proforma.nil?
            @logger.info "No RM number => skipped"
        end
        pm_no=proforma["INVOICE_NO"]

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        doc_date=row["CONFIRM_DATE"].strftime("%m-%d-%y")
        date_invoice=row["REGISTRATION_DATE"].strftime("%Y-%m-%d")
        original_invoice =ro_no #for credit sale

        original_invoice=@erp.execute("account.invoice","search",
            [["origin","like",ro_no],["company_id","=",@company_id],["partner_id","=",partner_id],["type","in",["out_invoice","out_cash"]]])
        #original_invoice=@erp.execute "account.invoice","search",[["origin","like",ro_no],["company_id","=",@company_id],['partner_id','=',partner_id]]
        if original_invoice.empty?
            original_invoice=@erp.execute "account.invoice","search",[["origin","like",ro_no],["company_id","=",@company_id]]
        end

        puts "original_invoice_id",original_invoice

        #try to create  original invoice
        if original_invoice.empty?
            original_invoice_id=@erp.get_origin_invoice_like original_invoice,@company_id,@branch_code,"cust_invoice_service",ro_no,partner_id,:required=>true
        end
        if original_invoice.empty?
            @logger.info "Missing original invoice: #{origin}"
            return
        end

        res=@erp.execute "account.invoice","read",original_invoice,["address_invoice_id","user_id"]
        address_id=res[0]["address_invoice_id"][0]
        user_id=res[0]["user_id"][0]


        vals={
            "type" => "out_refund",
            "origin" => origin,
            "reference" => credit_no,
            "comment" => comment,
            "journal_id" => @erp.get_journal("CUST_CREDIT_SERVICE_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "account_id" => @erp.get_account("CUST_CREDIT_SERVICE_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date"=>doc_date,
            "orig_invoice_id" => original_invoice[0],
            "vehicle_id" => vehicle_id,
            "address_invoice_id" => address_id,
            "user_id"=>user_id,
            "refund_type"=>"regular",
        }

        lines=[]
        t0_1=Time.now()
        lines+=get_job_lines(credit_no)
        t0_2=Time.now()
        lines+=get_part_lines(credit_no)
        t0_3=Time.now()
        lines+=get_other_lines(credit_no)
        t0_4=Time.now()

        raise "No lines in invoice" if lines.empty?
        vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}

        pp "vals:",vals
        t1=Time.now()
        inv_id=@erp.execute "account.invoice","create",vals
        t2=Time.now()
        @logger.info "invoice created: #{pm_no}=>#{inv_id}"
        puts "Computing invoice...", inv_id
        @erp.execute "account.invoice","button_compute",[inv_id]

        t2_1=Time.now()
        res=@erp.execute "account.invoice","read",[inv_id],["amount_untaxed"]
        amt=res[0]["amount_untaxed"]
        if (amt-row["CREDIT_AMOUNT"]).abs>0.25
            msg="wrong total in #{pm_no}: #{amt}/#{row["CREDIT_AMOUNT"]}"
            @logger.error msg
            raise msg
        end
        t2_2=Time.now()

        t3=Time.now()
        puts "Posting invoice..."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        t4=Time.now()
        @logger.info "times: total=#{t4-t0} prepare=#{t1-t0} create=#{t2-t1} compute=#{t3-t2} post=#{t4-t3} job_lines=#{t0_2-t0_1} part_lines=#{t0_3-t0_2} other_lines=#{t0_4-t0_3} checking=#{t2_2-t2_1}"
        return inv_id
    end
    
    def get_job_lines(inv_no)
        q="
SELECT RJ.*,RH.*,RI.*
 FROM RT_CREDIT_J RJ
 JOIN RT_CREDIT_I RI ON RI.RO_NO = RJ.RO_NO AND RI.RQMT_NO=RJ.RQMT_NO AND RI.JOB_INST_NO=RJ.JOB_INST_NO AND RI.CREDIT_NOTE_NO=RJ.CREDIT_NOTE_NO
 JOIN RT_CREDIT_H RH ON RH.RO_NO = RJ.RO_NO AND CREDIT_FLG=1 AND RH.CREDIT_NOTE_NO=RJ.CREDIT_NOTE_NO AND RH.TAX_INVOICE_NO=RJ.TAX_INVOICE_NO
 WHERE RH.CREDIT_NOTE_NO='#{inv_no} '
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            name="#{row['RQMT_NO']}J#{row['JOB_INST_SEQ']} #{row['JOB_NM'].strip}"
            account_id=self.get_account_job row["WORK_TYPE"]
            analytic_id=self.get_analytic_job row["WORK_TYPE"]
            note=self.get_line_note row
            tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
            qty=row["QTY"]
            amt=row["CREDIT_AMT"]
            price=amt/qty
            vals={
                "account_id" => account_id,
                "account_analytic_id" => analytic_id,
                "name" => name,
                "company_id" => @company_id,
                "note" => note,
                "quantity" => qty,
                "price_unit" => price,
                "invoice_line_tax_id"=>[[6,0,[tax_id]]],
            }
            lines<<vals
        end
        lines
    end

    def get_part_lines(inv_no)
        q="
SELECT RP.*,RH.*,RI.*,PM.*
 FROM RT_CREDIT_P RP
 JOIN RT_CREDIT_I RI ON RI.RO_NO = RP.RO_NO AND RI.RQMT_NO=RP.RQMT_NO AND RP.JOB_INST_NO = RI.JOB_INST_NO AND RP.CREDIT_NOTE_NO=RI.CREDIT_NOTE_NO
 JOIN RT_CREDIT_H RH ON RH.RO_NO = RP.RO_NO AND CREDIT_FLG=1 AND RH.CREDIT_NOTE_NO=RP.CREDIT_NOTE_NO AND RH.TAX_INVOICE_NO=RP.TAX_INVOICE_NO
 JOIN PM_SPMAST PM ON PM.PART_NO = RP.PARTS_NO
 WHERE RH.CREDIT_NOTE_NO='#{inv_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            name="#{row['RQMT_NO']}P#{row['PARTS_SEQ']} #{row['PARTS_NM'].strip}"
            account_id=self.get_account_part row["WORK_TYPE"],row["PART_TYPE"]
            analytic_id=self.get_analytic_part row["WORK_TYPE"],row["PART_TYPE"]
            tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
            uom_id=@erp.get_uom "PCE",:required=>true
            note=self.get_line_note row
            amt=row["CREDIT_AMT"]
            qty=row["QTY"]
            price=amt/qty
            vals={
                "name"=>name,
                "price_unit"=>price,
                "quantity"=>qty,
                "invoice_line_tax_id"=>[[6,0,[tax_id]]],
                "account_id"=>account_id,
                "uos_id"=>uom_id,
                "note"=>note,
                "account_analytic_id" => analytic_id,
            }
            lines<<vals
        end
        lines
    end

    def get_other_lines(inv_no)
        q="
SELECT RO.*,RH.*,RI.*
 FROM RT_CREDIT_O RO
 JOIN RT_CREDIT_I RI ON RI.RO_NO = RO.RO_NO AND RI.RQMT_NO=RO.RQMT_NO AND RI.JOB_INST_NO=RO.JOB_INST_NO AND RI.CREDIT_NOTE_NO=RO.CREDIT_NOTE_NO
 JOIN RT_CREDIT_H RH ON RH.RO_NO = RO.RO_NO AND RO.CREDIT_FLG=1 AND RH.CREDIT_NOTE_NO=RO.CREDIT_NOTE_NO
 WHERE RH.CREDIT_NOTE_NO='#{inv_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            pp row
            name="#{row['RQMT_NO']}O#{row['OTHERS_COST_SEQ']} #{row['OTHERS_COST_NM'].strip}"
            account_id=self.get_account_other row
            tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
            uom_id=@erp.get_uom "PCE",:required=>true
            analytic_id=self.get_analytic_other row["WORK_TYPE"]
            note=self.get_line_note row
            amt=row["CREDIT_AMT"]
            qty=row["QTY"]
            price=amt/qty
            vals={
                "name"=>name,
                "quantity"=>qty,
                "price_unit"=>price,
                "invoice_line_tax_id"=>[[6,0,[tax_id]]],
                "uos_id"=>uom_id,
                "note"=>note,
                "account_id"=>account_id,
                "account_analytic_id"=>analytic_id,
            }
            lines<<vals
        end
        lines
    end

    def get_line_note(row)
        note=case row["WORK_TYPE"]
        when 1 then "PDI"
        when 2 then "PM"
        when 3 then "GR"
        when 4 then "BP"
        when 5 then "ACC"
        when 6 then "ANN"
        else ""
        end
        for flag in ["RPT","WARR","FOC","SUBLET","SHARED"]
            note+=" "+flag if row[flag+"_FLG"]
        end
        note
    end

    def get_account_job(work_type)
        puts "get_account_job",work_type
        code=case work_type
        when 1,2,3 then "CUST_CREDIT_SERVICE_INCOME_JOB_GR"
        when 4 then "CUST_CREDIT_SERVICE_INCOME_JOB_BP"
        when 5,6 then "CUST_CREDIT_SERVICE_INCOME_JOB_FOC"
        else
            raise "Invalid work_type: #{work_type}"
        end
        @erp.get_account code,@parent_company_id,:required=>true
    end

    def get_account_part(work_type,part_type)
        puts "get_account_part",work_type,part_type
        code=case part_type
        when "1","2","3","4"
            case work_type
            when 1,2,3 then "CUST_CREDIT_SERVICE_INCOME_PART_GEN_GR"
            when 4 then "CUST_CREDIT_SERVICE_INCOME_PART_GEN_BP"
            when 5,6 then "CUST_CREDIT_SERVICE_INCOME_PART_GEN_FOC"
            else
                raise "Invalid work_type: #{work_type}"
            end
        when "5"
            case work_type
            when 1,2,3 then "CUST_CREDIT_SERVICE_INCOME_PART_OIL_GR"
            when 4 then "CUST_CREDIT_SERVICE_INCOME_PART_OIL_BP"
            when 5,6 then "CUST_CREDIT_SERVICE_INCOME_PART_OIL_FOC"
            else
                raise "Invalid work_type: #{work_type}"
            end
        when "6"
            case work_type
            when 1,2,3 then "CUST_CREDIT_SERVICE_INCOME_PART_ACC_GR"
            when 4 then "CUST_CREDIT_SERVICE_INCOME_PART_ACC_BP"
            when 5,6 then "CUST_CREDIT_SERVICE_INCOME_PART_ACC_FOC"
            else
                raise "Invalid work_type: #{work_type}"
            end
        else
            raise "Invalid part type: #{part_type}"
        end
        @erp.get_account code,@parent_company_id,:required=>true
    end
    
    def get_account_other(row)
        work_type=row["WORK_TYPE"] 
        puts "get_account_other",work_type
        code=case work_type
        when 1,2,3,6 then "CUST_CREDIT_SERVICE_INCOME_OTHER_GR"
        when 4 then "CUST_CREDIT_SERVICE_INCOME_OTHER_BP"
        when 5 then "CUST_CREDIT_SERVICE_INCOME_OTHER_ACC"
        else
            raise "Invalid work_type: #{work_type}"
        end
        @erp.get_account code,@parent_company_id,:required=>true 
    end 

    def get_analytic_job(work_type)
        puts "get_analytic_job",work_type
        work_type_group=case work_type
        when 1,2,3 then "GR"
        when 4 then "BP"
        when 5,6 then "FOC"
        else
            raise "Invalid work_type: #{work_type}"
        end
        code="SERV_LABOR_"+work_type_group
        @erp.get_analytic_account code,@company_id,:required=>true
    end

    def get_analytic_part(work_type,part_type)
        work_type_group=case work_type
        when 1,2,3 then "GR"
        when 4 then "BP"
        when 5,6 then "FOC"
        else
            raise "Invalid work_type: #{work_type}"
        end
        part_type_group=case part_type
        when "1","2","3","4" then "GEN"
        when "5" then "OIL"
        when "6" then "ACC"
        else
            raise "Invalid part_type: #{part_type}"
        end
        if work_type_group=="FOC"
            code="SERV_PART_"+work_type_group
        else
            code="SERV_PART_"+work_type_group+"_"+part_type_group
        end
        @erp.get_analytic_account code,@company_id,:required=>true
    end

    def get_analytic_other(work_type)
        puts "get_analytic_other",work_type
        work_type_group=case work_type
        when 1,2,3 then "GR"
        when 4 then "BP"
        when 5,6 then "FOC"
        else
            raise "Invalid work_type: #{work_type}"
        end
        code="SERV_OTHER_"+work_type_group
        @erp.get_analytic_account code,@company_id,:required=>true
    end
end

