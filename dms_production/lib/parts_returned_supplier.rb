require "dms"
require "erp"
require "importer"
require "pp"

class Importer_PartsReturnedSupplier < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM PT_EXCEPTIONIO_H WHERE IO_NO='#{doc_no}'"
        res=@dms.get q
        raise "Job order not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM PT_EXCEPTIONIO_H WHERE IO_DATE >='#{date_from} 00:00:00' AND IO_DATE <='#{date_to} 23:59:59' AND IO_NO LIKE 'PX%' AND EXCEP_STOCK_CODE=3 AND REASON_CODE='RT'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PT_EXCEPTIONIO_H WHERE IO_NO LIKE 'PX%' AND EXCEP_STOCK_CODE=3 AND REASON_CODE='RT' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_parts_received_branch",date_from,date_to
        q="SELECT IO_NO FROM PT_EXCEPTIONIO_H WHERE IO_NO LIKE 'PX%' AND EXCEP_STOCK_CODE=3 AND REASON_CODE='RT' AND IO_DATE>='#{date_from} 00:00:00' AND IO_DATE<='#{date_to} 23:59:59' ORDER BY IO_NO"
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="PX%"
        for row in rows
            invoice=row["IO_NO"].strip
            dms_docnos.push(invoice)
        end
        pp dms_docnos
        puts "################################################"
        erp_docnos=@erp.get_picking_docnos inv_no,"out",date_from,date_to,@company_id
        pp erp_docnos
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        io_no=row["IO_NO"].strip
        origin=io_no

        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "Picking already created: #{origin}"
            return
        end
        date=row["IO_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done"=>date,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>"out",
            "stock_journal_id"=>@erp.get_stock_journal("PARTS_RETURNED_SUPPLIER_JOURNAL",@company_id), #PART_PX
        }

        lines=get_lines(io_no,row)
        pick_vals["move_lines"]=lines.collect {|line_vals| [0,0,line_vals]}

        pp "vals",pick_vals
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created picking #{io_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end

    def get_lines(io_no,job_row)
        q="
SELECT *
 FROM PT_EXCEPTIONIO_D
 WHERE IO_NO='#{io_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            pp "line_row",row
            part_no=row["PART_NO"].strip
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            date=row["REGISTRATION_DATE"].strftime "%Y-%m-%d %H:%M:%S"
            line_no=row["LINE_NO"].to_s
            qty=row["CONFIRMED_QTY"]
            vals={
                "name"=>io_no+" "+line_no,
                "date"=>date,
                "date_expected"=>date,
                "product_id"=>product_id,
                "product_uom"=>@erp.get_uom("PCE",:required=>true),
                "product_qty"=>qty,
                "location_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
                "location_dest_id"=>@erp.get_location("Suppliers",:required=>true),
                "state"=>"assigned",
                "price_unit"=>row["COST_PRICE"],
            }
            lines<<vals
        end
        lines
    end
end
