require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CarsDeliveredBranch < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM ST_RORDERINV WHERE INVOICE_NO='#{doc_no}'"
        res=@dms.get q
        raise "Purchase not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM ST_RORDERINV WHERE INV_PRINT_DATE >='#{date_from} 00:00:00' AND INV_PRINT_DATE <='#{date_to} 23:59:59' AND INVOICE_NO LIKE 'SF%'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_RORDERINV WHERE INVOICE_NO LIKE 'SF%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        inv_no=row["INVOICE_NO"].strip
        order_no=row["RCVORDER_NO"].strip
        origin=inv_no+" "+order_no
        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "Picking already created: #{origin}"
            return
        end

        partner_no=row["CLAIM_CUST_NO"].strip
        date=row["INV_PRINT_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)
        if not address_id
            address_id=@erp.get_partner_address(partner_id,:required=>true)
        end

        invoice_id=@erp.get_invoice_id(inv_no,@company_id)
        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done"=>date,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "partner_id"=>partner_id,
            "type"=>"internal",
            "address_id"=>address_id,
            "stock_journal_id"=>@erp.get_stock_journal("CARS_DELIVERED_OTHER_BRANCH_JOURNAL",@company_id), # CAR_SF_OUT
            "invoice_id"=>invoice_id,
        }

        frame_no=row["FRAME_NO"].strip
        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        prodlot_id=@erp.get_prodlot frame_no
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        line_vals={
            "name"=>frame_no,
            "date_expected"=>date,
            "date"=>date,
            "product_id"=>product_id,
            "product_uom"=>@erp.get_uom("PCE",:required=>true),
            "product_qty"=>1,
            "location_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
            "location_dest_id"=>@erp.get_location(@branch_code+"-Transfer",:required=>true),
            "state"=>"assigned",
            "prodlot_id"=>prodlot_id,
            "vehicle_id"=>vehicle_id,
        }
        pick_vals["move_lines"]=[[0,0,line_vals]]
        pp "vals",pick_vals
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created picking #{inv_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end
end
