require "dms"
require "erp"
require "importer"
require "pp"

class Importer_SaleUser < Importer
    def import_by_no(user_code)
        puts "Importer_Saleuser.import_by_no",user_code
        q="SELECT * FROM CM_STAFF WHERE USER_ID='#{user_code}'"
        res=@dms.get q
        raise "Sale user not found: #{user_code}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_Saleuser.import_by_date",date_from,date_to
        q="SELECT * FROM CM_STAFF WHERE LAST_UPDATE_DATE>='#{date_from}' AND LAST_UPDATE_DATE<='#{date_to}'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_Saleuser.import_daily"
        t0=(Time.now+3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM CM_STAFF WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q="SELECT USER_ID FROM CM_STAFF WHERE LAST_UPDATE_DATE>='#{date_from} 00:00:00' AND LAST_UPDATE_DATE<='#{date_to} 23:59:59' ORDER BY LAST_UPDATE_DATE"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["USER_ID"].strip}
        missing=[]
        for docno in dms_docnos
            docno=docno.strip.upcase
            user_id=@erp.get_user docno
            if not user_id
                missing.push docno
            end
        end
        return missing
    end

    def import_row(row)
        puts "Importer_Saleuser.import_row"
        name=row["FIRST_NAME"].strip+" "+row["LAST_NAME"].strip
        login= row["USER_ID"].strip
        vals={
            "name" => name,
            "login" => login,
            "active" => true,
            "company_id"=>@company_id,
            "company_ids"=>[[6,0,[@company_id]]],
            "dms_user_ok"=>1,

            }
        res=@erp.execute "res.users","search",[["login","=",login],["company_id","=",@company_id]]
        if res.empty?
            res=@erp.execute "res.users","search",[["login","=",login],["active","=",0],["company_id","=",@company_id]]
        end

        if not res.empty?
            user_id=res[0]
            @erp.execute "res.users","write",res,vals
            @logger.info "user updated: #{login}"
        else
            user_id =@erp.execute "res.users","create",vals
            @logger.info "user created: #{login}"
        end
        return user_id
    end
end
