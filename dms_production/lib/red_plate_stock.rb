require "dms"
require "erp"
require "importer"
require "pp"

class Importer_RedPlateStock < Importer
    def import_by_no(doc_no)
        puts "Importer_RedPlateStock.import_by_no",doc_no
        q="SELECT * FROM ST_RENTAL WHERE TLP_NO='#{doc_no}' OR SHEET_NO='#{doc_no}' OR BOOKING_NO='#{doc_no}'"
        res=@dms.get q
        raise "Red plate not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_RedPlateStock.import_by_date",date_from,date_to
        q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{date_from}' AND LAST_UPDATE_DATE<='#{date_to}' ORDER BY SHEET_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_RedPlateStock.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:00"
        q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_redplate_stock",date_from,date_to
        q="SELECT TLP_NO,SHEET_NO,BOOKING_NO FROM ST_RENTAL WHERE RENTAL_DATE>='#{date_from} 00:00:00' AND RENTAL_DATE<='#{date_to} 23:59:59' ORDER BY SHEET_NO"
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="S7%"
        for row in rows
            invoice=row["SHEET_NO"].strip+" "+row["BOOKING_NO"].strip
            dms_docnos.push(invoice)
        end
        pp dms_docnos
        puts "################################################"
        erp_docnos=@erp.get_picking_docnos inv_no,"out",date_from,date_to,@company_id
        pp erp_docnos
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        puts "Importer_RedPlateStock.import_row"
        if row["RENTAL_DATE"] # rental date means red plate is out of stock
            create_out_picking row
        else # no rental_date means red plate is in stock
            create_in_picking row
        end
    end

    def create_out_picking(row)
        origin=row["SHEET_NO"].strip+" "+row["BOOKING_NO"].strip

        res=@erp.execute "stock.picking","search",[["origin","=",origin],["type","=","out"],['company_id','=',@company_id]]
        invoice_id=@erp.get_invoice_id(row["SHEET_NO"].strip,@company_id,"out_invoice")
        if not res.empty?
            puts "Out-picking already created: #{origin}"
            @logger.info "SKIPPED : Redplate-picking #{origin}"
            return
        end

        plate_no=row["TLP_NO"].strip
        product_id=@erp.get_product("RED_PLATE",:required=>true)
        prodlot_id=@erp.get_prodlot plate_no
        if not prodlot_id
            imp=DMS_OpenERP.get_importer "red_plate"
            imp.start_import "branch_code"=>@branch_code,"no_update"=>"-c"
            imp.import_by_no plate_no
            prodlot_id=@erp.get_prodlot(plate_no,:required=>true)
        end

        date=row["RENTAL_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        pick_vals={
            "origin"=>origin,
            "name"=>row["SHEET_NO"].strip,
            "date"=>date,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>"out",
            "invoice_id" => invoice_id,
        }

        line_vals={
            "name"=>origin,
            "date_expected"=>date,
            "date_planned"=>date,
            "product_id"=>product_id,
            "product_uom"=>@erp.get_uom("PCE",:required=>true),
            "product_qty"=>1,
            "location_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
            "location_dest_id"=>@erp.get_location("Customers",:required=>true),
            "state"=>"assigned",
            "prodlot_id"=>prodlot_id,
        }
        pick_vals["move_lines"]=[[0,0,line_vals]]

        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created out-picking #{origin}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
    end

    def create_in_picking(row)
        plate_no=row["TLP_NO"].strip
        product_id=@erp.get_product("RED_PLATE",:required=>true)
        prodlot_id=@erp.get_prodlot plate_no
        invoice_id=@erp.get_invoice_id(row["SHEET_NO"].strip,@company_id,"in_invoice")
        if not prodlot_id
            imp=DMS_OpenERP.get_importer "red_plate"
            imp.start_import "branch_code"=>@branch_code,"no_update"=>"-c"
            imp.import_by_no plate_no
            prodlot_id=@erp.get_prodlot(plate_no,:required=>true)
        end 

        # find last picking
        res=@erp.execute "stock.move","search",[["prodlot_id","=",prodlot_id]],0,1,"date desc"
        if res.empty?
            puts "Out-picking not found => don't create in-picking"
            return
        end
        move_id=res[0]
        res=@erp.execute "stock.move","read",[move_id],["picking_id"]
        picking_id=res[0]["picking_id"][0]
        res=@erp.execute "stock.picking","read",[picking_id],["type","origin"]
        type=res[0]["type"]
        origin=res[0]["origin"]
        if type!="out"
            puts "Last picking is not out-picking => don't create in-picking"
            return
        end

        date=row["RETURN_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        pick_vals={
            "origin"=>origin,
            "name"=>row["SHEET_NO"].strip,
            "date"=>date,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>"in",
            "invoice_id"=>invoice_id,
        }

        line_vals={
            "name"=>origin,
            "date_expected"=>date,
            "date_planned"=>date,
            "product_id"=>product_id,
            "product_uom"=>@erp.get_uom("PCE",:required=>true),
            "product_qty"=>1,
            "location_id"=>@erp.get_location("Customers",:required=>true),
            "location_dest_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
            "state"=>"assigned",
            "prodlot_id"=>prodlot_id,
        }
        pick_vals["move_lines"]=[[0,0,line_vals]]

        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "CREATED : in-picking #{origin}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
    end
end
