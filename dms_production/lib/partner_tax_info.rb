require "dms"
require "erp"
require "pp"
require "importer"

class Importer_Partner_Tax_Info< Importer

    def query(number,date_from=false,date_to=false,last_update=false)

        cause= date_from!=false ?  " AND REGISTRATION_DATE >='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND REGISTRATION_DATE <='#{date_to} 23:59:59' ": "")
        cause << (last_update!=false ? " AND LAST_UPDATE_DATE>='#{last_update}' ": "")

        cause_customer = (number!=false ?  " AND ( CUSTOMER_NO='#{number}' ) ": "")
        cause_supplier = (number!=false ?  " AND ( SUPPLIER_NO='#{number}' ) ": "")
        cause_supplier_part = (number!=false ?  " AND ( SUPPLIER_CODE='#{number}' ) ": "")

        customer="
        SELECT rtrim (upper(customer_no)) AS REF,
            HEAD_OFFICE_FLAG as HQ_OK,
            BRANCH_NO as BRANCH_REF,
            TAXID_NO as TIN
        FROM SM_CUSTOMERBASIC_EXT
        WHERE ( TAXID_NO is not null or TAXID_NO!='')
            #{cause_customer} #{cause}
        "

        supplier_part="
        SELECT UPPER(SUPPLIER_CODE) AS REF,
        HEAD_OFFICE_FLAG as HQ_OK,
        BRANCH_NO as BRANCH_REF,
        TAXID_NO as TIN
        FROM PM_VENDOR
        WHERE active_flag=1
        and ( TAXID_NO is not null or TAXID_NO!='')
            #{cause_supplier_part} #{cause}
        "

        supplier="
        SELECT rtrim (upper (s.SUPPLIER_NO)) AS REF,
              s.TAXID_NO AS TIN,
              s.HEAD_OFFICE_FLAG as HQ_OK,
              s.BRANCH_NO as BRANCH_REF
         FROM SM_SUPPLIER s
        WHERE s.ACTIVE_FLG = 1
        and ( s.TAXID_NO is not null or s.TAXID_NO!='' )
         #{cause_supplier} #{cause}
        "
        rows=[]
        sqls=[customer,supplier,supplier_part]
        for sql in sqls
            rows+=@dms.query(sql)
        end
        return rows
    end

    def import_by_no(number)
        rows = self.query(number)
        self.import_rows rows
    end

    def import_by_date(date_from,date_to)
        rows=self.query(false,date_from,date_to)
        self.import_rows rows
        return true
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*1).strftime "%Y-%m-%d"
        #q=self.query(false,false,false,t0)
        #rows=@dms.query q
        rows=self.query(false,t0,t0)
        self.import_rows rows,progress
        return
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        rows=self.query(false,false,false,t0)
        self.import_rows rows,progress
    end

    def import_rows(rows,progress=nil)

        for r in rows
            ref = r["REF"].strip()

            vals={
                #"wht_type"=>"pnd3"
                "wht_type"=>"pnd53"
            }

            if (r["TIN"] =~ /^\d{13}/).nil?
                # skip import
                next
            end

            vals["tin"]=r["TIN"].strip

            if ! r["BRANCH_REF"].empty?
                vals["branch_ref"]= r["BRANCH_REF"].strip
            end

            if r["BRANCH_REF"].empty?
                vals["wht_type"]= "pnd3"
            end

            if  r["HQ_OK"]=="0"
                vals["branch_ref"]= "00000"
                vals["wht_type"]= "pnd53"
            end


            partner_id=@erp.execute "res.partner","search",[["ref","=",ref],["dms_company_id","=",@company_id]]

            if partner_id.empty?
                puts "skip"
                @logger.info "SKIP: Partner not exist: #{}"
                next
            else
                puts vals
                #next #<<<

                @logger.info "UPDATED: Partner: #{ref}=>#{partner_id}"
                @erp.execute "res.partner","write",partner_id,vals
            end

        end
        return true
    end

end
