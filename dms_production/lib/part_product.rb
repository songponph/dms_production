require "dms"
require "erp"
require "importer"
require "pp"

class Importer_PartProduct < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM PM_SPMAST WHERE PART_NO='#{doc_no}'"
        res=@dms.get q
        raise "Part not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM PM_SPMAST WHERE REGISTRATION_DATE>='#{date_from} 00:00:00' AND REGISTRATION_DATE<='#{date_to} 23:59:59'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PM_SPMAST WHERE REGISTRATION_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
        return
    end

    def import_row(row)
        pp row
        part_no= @erp.convert_product_code(row["PART_NO"].strip.upcase)


        product_id=@erp.get_product(part_no)
        if product_id and @no_update
            @logger.info "SKIPPED : Product #{part_no} already created"
            return
        end
        vals={
            "default_code"=>part_no,
            "sale_ok"=>false,
            "purchase_ok"=>false,
            "name"=>row["PART_NAME_L"]||row["PART_NAME_E"],
            "description"=>row["PART_NAME_E"]||"",
            "type"=>"consu",
            "categ_id"=>@erp.get_product_category("Parts",:required=>true),
            "list_price"=>row["RTL_PRICE"]||0.0,
            "part_cost"=>row["COST_PRICE"],
            "part_dnp"=>row["DEALER_NET_PRICE"],
            "part_type"=>row["PART_TYPE"].to_s,
        }
        if product_id
            #return #<<< skip for now
            @erp.execute "product.product","write",[product_id],vals
            @logger.info "Updated product #{part_no}"
        else
            @erp.execute "product.product","create",vals
            @logger.info "Created product #{part_no}"
        end
        return product_id
    end
end
