require "dms"
require "erp"
require "pp"
require "importer"

class Importer_PartsReturnedJobs < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO='#{doc_no}' OR INVOICE_NO='#{doc_no}'"
        res=@dms.get q
        raise "Job order not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PS%' AND IO_DATE>='#{date_from} 00:00:00' AND IO_DATE<='#{date_to} 23:59:59'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        #q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PS%' AND LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
        return
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PS%' AND LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def check_import(date_from,date_to)
        puts "Check_import_parts_returned_branch",date_from,date_to
        q="SELECT IO_NO,INVOICE_NO FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PS%' AND IO_DATE>='#{date_from} 00:00:00' AND IO_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_NO"
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="PS%"
        for row in rows
            invoice=row["IO_NO"].strip+" "+row["INVOICE_NO"].strip.upcase
            dms_docnos.push(invoice)
        end
        pp dms_docnos
        puts "################################################"
        erp_docnos=@erp.get_picking_docnos inv_no,"internal",date_from,date_to,@company_id
        pp erp_docnos
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        io_no=row["IO_NO"].strip
        invoice_no=row["INVOICE_NO"].strip.upcase

        origin=io_no+" "+invoice_no
        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "Picking already created: #{origin}"
            return
        end

        date=row["IO_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        date_done=row["DELIVERED_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done"=>date_done,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>"internal",
            "stock_journal_id"=>@erp.get_stock_journal("PARTS_RETURNED_JOBS_JOURNAL",@company_id), #PART_JOB_RETRUN
        }

        lines=get_lines(io_no,row)
        pick_vals["move_lines"]=lines.collect {|line_vals| [0,0,line_vals]}

        pp "vals",pick_vals
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created picking #{io_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end

    def get_lines(io_no,job_row)
        q="
SELECT *
 FROM PT_RETURNINVOICE_D
 WHERE IO_NO='#{io_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            part_no=row["PART_NO"].strip.upcase
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            date=job_row["DELIVERED_DATE"].strftime "%Y-%m-%d %H:%M:%S"
            line_no=row["LINE_NO"].to_s
            qty=row["RETURN_QTY"]
            vals={
                "name"=>io_no+" "+line_no,
                "date"=>date,
                "date_expected"=>date,
                "product_id"=>product_id,
                "product_uom"=>@erp.get_uom("PCE",:required=>true),
                "product_qty"=>qty,
                "location_id"=>@erp.get_location(@branch_code+"-Production",:required=>true),
                "location_dest_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
                "state"=>"assigned",
                "price_unit"=>row["COST_PRICE"],
            }
            lines<<vals
        end
        lines
    end
end
