require "dms"
require "erp"
require "pp"
require "importer"

#STATUS 04 : only Invoice
#STATUS 05 :  has credit note for S8
class Importer_CustInvoiceCarBooking < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND BOOKINGVAT_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND BOOKINGVAT_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( BOOKINGVAT_NO='#{number}' or BOOKING_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND LAST_UPDATE_DATE>='#{last_update}' ": "")

        query="
            SELECT * FROM ST_BOOKINGVAT
                WHERE STATUS is not null
                "+cause+"
            order by BOOKINGVAT_DATE,BOOKINGVAT_NO
        "
        return query
    end

    def import_by_no(doc_no)
        q = self.query_get(doc_no)
        res=@dms.get q
        raise "Booking invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #t0=from_t.strftime "%Y-%m-%d %H:00:00"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def check_import(date_from,date_to)
        q = self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["BOOKINGVAT_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_invoice",date_from,date_to,"SD%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def create_invoice(row)
        t0=Time.now()
        bookingvat_no=row["BOOKINGVAT_NO"].strip
        booking_no=row["BOOKING_NO"].strip

        #100% if status='04' it will have bookingvat_no
        #following condition nto use
        #if bookingvat_no.empty?
        #    @logger.info "SKIPPED : No booking invoice no........"
        #    return
        #end

        origin=bookingvat_no+ " " + booking_no
        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "SKIPPED : Invoice already created:#{origin}"
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.create_address_invoice_booking row

        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true

        user_id=@erp.get_user row["STAFF_CD"].strip,@branch_code,:required=>true

        tax_id=@erp.get_tax "OUT_VAT7_INCL",:required=>true

        date_invoice=row["BOOKINGVAT_DATE"].strftime("%m/%d/%y")

        vals_={
            "origin" => origin,
            "reference" => bookingvat_no,
            "journal_id" => @erp.get_journal("CUST_INV_BOOKING_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_invoice",
            "account_id" => @erp.get_account("CUST_INV_BOOKING_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date" => date_invoice,
            "booking_id" => booking_id,
            "user_id" => user_id,
            "address_invoice_id" => address_id
        }

        model_no=row["MTOC_CD"].strip
        prod_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        res=@erp.execute "product.product","read",[prod_id],["name","variants"]
        name=res[0]["name"]
        variant=(res[0]["variants"] or "")
        prod_name=name+" "+variant
        price_unit=row["DEPOSIT_CR"]+row["DEPOSIT_VAT"]

        lines_={
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_INV_BOOKING_INCOME",@parent_company_id,:required=>true),
            "name" => prod_name,
            "product_id" => prod_id,
            "quantity" => 1,
            "price_unit" => price_unit,
            "invoice_line_tax_id" => [[6,0,[tax_id]]],
            }

        vals_["invoice_line"]=[[0,0,lines_]]


        t1=Time.now()

        inv_id=@erp.execute "account.invoice","create",vals_
        t2=Time.now()
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        @erp.execute "account.invoice","button_compute",[inv_id]


        t2_2=Time.now()
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total","amount_tax"]

        if row["DEPOSIT_VAT"] !=res[0]["amount_tax"]
            @erp.execute "account.invoice","set_manual_vat",[inv_id],false,row["DEPOSIT_CR"],row["DEPOSIT_VAT"],@company_id
        end

        amt=res[0]["amount_total"]
        if (amt-price_unit).abs>0.25
            msg="wrong total in #{bookingvat_no}: #{amt}/#{price_unit}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        t4=Time.now()
        return inv_id
    end
end
