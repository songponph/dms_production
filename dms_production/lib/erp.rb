# -*- encoding: utf-8 -*-
require "xmlrpc/client"
require "dms"
require "pp"
require "importer"
require "logger"

class ERPConnection
    def connect()
        puts "ERP.connect"
        @host="192.168.80.72"
        @db="hd_clean"
        @password="honda770"
        @uid=1
        @port='28069'
        @server=XMLRPC::Client.new2("http://#{@host}:#{@port}/xmlrpc/object")
        @cache={}
        @logger=DMS_OpenERP.get_logger
    end

    def get_cache(key)
        val=@cache[key]
        return val
    end

    def set_cache(key,val)
        @cache[key]=val
    end

    def execute(model,method,*args)
        begin
            res=@server.call "execute",@db,@uid,@password,model,method,*args
        rescue
            puts "ERP execution error, trying again"
            puts "ERP.execute #{model} #{method} #{args.inspect()}"
            sleep 10
            res=@server.call "execute",@db,@uid,@password,model,method,*args
        end
        return res
    end

    def exec_workflow(model,method,*args)
        res=@server.call "exec_workflow",@db,@uid,@password,model,method,*args
        res
    end

    def get_account(code,company_id,options={})
        id=self.get_cache(["account",code,company_id])
        return id if id
        res=self.execute "ac.import.map","search",[["code","=",code],["company_id","=",company_id]]
        if res[0].nil?
            raise "Account not found: #{code}" if options[:required]
            return nil
        end
        import_id=res[0]
        res=self.execute "ac.import.map","read",[import_id],["account_id"]
        id=res[0]["account_id"][0]
        self.set_cache(["account",code,company_id],id)
        id
    end

    def get_journal(code,company_id,options={})
        id=self.get_cache(["journal",code,company_id])
        return id if id
        res=self.execute "ac.import.map","search",[["code","=",code],["company_id","=",company_id]]
        if res.empty?
            raise "Journal not found: #{code}" if options[:required]
            return nil
        end
        import_id=res[0]
        res=self.execute "ac.import.map","read",[import_id],["journal_id"]
        id=res[0]["journal_id"][0]
        self.set_cache(["journal",code,company_id],id)
        id
    end

    def get_stock_journal(code,company_id,options={})
        id=self.get_cache(["stock_journal",code,company_id])
        return id if id
        res=self.execute "ac.import.map","search",[["code","=",code],["company_id","=",company_id]]
        if res.empty?
            raise "Stock journal not found: #{code}" if options[:required]
            return nil
        end
        import_id=res[0]
        res=self.execute "ac.import.map","read",[import_id],["stock_journal_id"]
        id=res[0]["stock_journal_id"][0]
        self.set_cache(["stock_journal",code,company_id],id)
        id
    end

    def get_period(date,company_id,options={})
        id=self.get_cache(["period",date,company_id])
        return id if id
        res=self.execute "account.period","search",[["date_start","<=",date],["date_stop",">=",date],["company_id","=",company_id]]
        if res.empty?
            raise "Period not found: #{date} #{company_id}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["period",date,company_id],id)
        id
    end

    def get_bank(code,options={})

        if code=="STC"
            code="SCNB"
        end
        id=self.get_cache(["bank",code])
        return id if id
        res=self.execute "res.bank","search",[["code","=",code]]
        if res.empty?
            raise "bank not found: #{code}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["bank",code],id)
        id
    end    

    def get_analytic_account(code,company_id,options={})
        id=self.get_cache(["analytic",code,company_id])
        return id if id
        res=self.execute "account.analytic.account","search",[["code","=",code],["company_id","=",company_id]]
        if res.empty?
            raise "Analytic account not found: #{code}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["analytic",code,company_id],id)
        id
    end

    def get_tax(code,options={})
        id=self.get_cache(["tax",code])
        return id if id
        res=self.execute "account.tax","search",[["description","=",code]]
        if res.empty?
            raise "Tax not found" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["tax",code],id)
        id
    end

    def get_name_title(title,options={})
        id=self.get_cache(["shorcut",title])
        return id if id
        res=self.execute "res.partner.title","search",[["shortcut","=",title]]
        if res.empty?
            title_vals={
                "name"=>title,
                "shortcut"=>title,
                "domain"=>"partner"
            }
            id=self.execute "res.partner.title","create",title_vals
        else
            id=res[0]
        end
        self.set_cache(["shorcut",title],id)
        id
    end

    def get_uom(name,options={})
        id=self.get_cache(["uom",name])
        return id if id
        res=self.execute "product.uom","search",[["name","=",name]]
        if res.empty?
            raise "UoM not found: #{name}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["uom",name],id)
        id
    end

    def check_booking_import_missing(doc_nos,date_from,date_to,doc_no)
        ids=self.execute "ac.booking","search",[["name","ilike",doc_no],["date",">=",date_from],["date","<=",date_to]]
        res=self.execute "ac.booking","read",ids,["name"]
        j=0
        refs=Array.new
        res.each do |i|
            refs.push(res[j]["name"])
            j=j+1
        end
        references=Set.new
        references=refs.to_set 
        missing=Array.new
        if res.empty?
            return nil
        end
        dms_count=0
        miss_count=0
        doc_nos.each do |i|
            dms_count=dms_count+1
            exist=references.include? i
            if not exist
                missing.push(i)
                miss_count+=1
            end
        end
        return missing
    end

    def check_stock_import_missing(doc_nos,date_from,date_to,doc_no,company_id,type)
        if type=="out"
            ids=self.execute "stock.picking","search",[["origin","ilike",doc_no],["date",">=",date_from],["date","<=",date_to],["company_id","=",company_id],["type","=","out"]]
        elsif type=="in"
            ids=self.execute "stock.picking","search",[["origin","ilike",doc_no],["date",">=",date_from],["date","<=",date_to],["company_id","=",company_id],["type","=","in"]]
        else
            ids=self.execute "stock.picking","search",[["origin","ilike",doc_no],["date",">=",date_from],["date","<=",date_to],["company_id","=",company_id],["type","=","internal"]]
        end
        res=self.execute "stock.picking","read",ids,["origin"]
        j=0
        refs=Array.new
        res.each do |i|
            refs.push(res[j]["origin"])
            j=j+1
        end
        references=Set.new
        references=refs.to_set 
        missing=Array.new
        if res.empty?
            return nil
        end
        dms_count=0
        miss_count=0
        doc_nos.each do |i|
            dms_count+=1
            exist=references.include? i
            if not exist 
                missing.push(i)
                miss_count+=1
            end
        end
        return missing
    end

    def get_invoice_reference(inv_type,date_from,date_to,reference,company_id)
        domain=[["reference","=",reference],["doc_date",">=",date_from],["doc_date","<=",date_to],["company_id","=",company_id]]
        if not inv_type.empty?
            domain+=[["type","=",inv_type]]
        end
        ids=self.execute "account.invoice","search",domain
        res=self.execute "account.invoice","read",ids,["reference"]
        refs=res.collect {|row| row["reference"]}
        return refs
    end

    def get_invoice_docnos(inv_type,date_from,date_to,origin,company_id)
        #domain=[["origin","ilike",origin],["date_invoice",">=",date_from],["date_invoice","<=",date_to],["company_id","=",company_id]]
        domain=[["origin","ilike",origin],["doc_date",">=",date_from],["doc_date","<=",date_to],["company_id","=",company_id]]
        if not inv_type.empty?
            domain+=[["type","=",inv_type]]
        end
        ids=self.execute "account.invoice","search",domain
        res=self.execute "account.invoice","read",ids,["reference"]
        refs=res.collect {|row| row["reference"]}
        return refs
    end

    def get_frame_nos_from_booking(bookings)
        ids=[]
        for booking in bookings
            id=self.execute "ac.booking","search",[["name","=",booking]]
            ids.push(id.pop)
        end
        res=self.execute "ac.booking","read",ids,["vehicle_id"]
        vehicle_ids=res.collect {|row| row["vehicle_id"]}
        pp vehicle_ids
        frame_nos=[]
        count=0
        for i in vehicle_ids
            if i
                frame_nos.push vehicle_ids[count][1]
            end
            count+=1
        end
        print frame_nos
        return frame_nos
    end

    def get_booking_docnos(date_from,date_to,origin)
        domain=[["name","ilike",origin],["date",">=",date_from],["date","<=",date_to]]
        ids=self.execute "ac.booking","search",domain
        res=self.execute "ac.booking","read",ids,["name"]
        refs=res.collect {|row| row["name"]}
        return refs
    end

    def get_picking_docnos(doc_no,type,date_from,date_to,company_id)
        domain=[["origin","ilike",doc_no],["date",">=",date_from],["date","<=",date_to],["type ","=",type],["company_id","=",company_id]]
        ids=self.execute "stock.picking","search",domain
        res=self.execute "stock.picking","read",ids,["origin"]
        refs=res.collect {|row| row["origin"]}
        return refs
    end

    def get_picking_id(doc_no,type,company_id,options={})
        id=self.get_cache(["picking_id",doc_no])
        return id if id
        domain=[]
        if not type.nil?
            domain=[["type","=",type]]
        end

        res=self.execute "stock.picking","search",[["origin","=",doc_no],["company_id","=",company_id]]+domain
        if res.empty?
            raise "invoice not found: #{doc_no}" if options[:required]
            return nil
        end 
        id=res[0]
        self.set_cache(["picking_id",doc_no],id)
        return id
    end

    def query_get_service(tables,cause)
        list=[]
        for table in tables
            list<<"
        (
            SELECT at.VAT_CR,at.rela_tran_no AS RO_NO ,
               at.tran_no AS INVOICE_NO,
               CASE
                   WHEN at.tran_no=at.invoice_no THEN NULL
                   ELSE at.invoice_no
               END AS TAX_INVOICE_NO ,
               at.cust_cd AS CUSTOMER_NO ,
               SV.FRAME_NO,SV.TOTAL_AMT,SV.TAX_AMT,SV.G_TOTAL_AMT,SV.GR_FLG,SV.BP_FLG,
               SV.ACC_FLG,SV.PM_FLG,SV.PDI_FLG,
               REPLACE(CONVERT(VARCHAR(10),SV.REGISTRATION_DATE, 111), '/', '-') AS REGISTRATION_DATE,
               REPLACE(CONVERT(VARCHAR(10),SV.CONFIRM_DATE, 111), '/', '-') as CONFIRM_DATE,
               REPLACE(CONVERT(VARCHAR(10),at.TRAN_DATE , 111), '/', '-') AS INVOICE_DATE ,
               REPLACE(CONVERT(VARCHAR(10),at.INVOICE_DATE, 111), '/', '-') AS TAX_INVOICE_DATE ,
               at.INVOICE_DATE as DATE,
               SV.CUSTOMER_NM,SV.SIR_NM,SV.ADDRESS_DETAILS,SV.ADDRESS_TOWNCITY,SV.ZIP_CD,
              SV.ADDRESS_DISTRICT,ADDRESS_PROVINCE,SV.TEL_HOME_NO,
              '#{table[0,2]}' as SOURCE_TABLE,
              SV.LICENSE_NO as PLATE_REF
            FROM at_unsettle_main AT
            JOIN #{table} SV ON (AT.tran_no = SV.PRE_INVOCE_NO)
            WHERE AT.tran_no LIKE 'RM%' and SV.FRAME_NO is not null
            #{cause}
        )

            "
        end
        sub_query = list.map! { |k| "#{k}" }.join(" UNION ")
        query = " SELECT * from (#{sub_query}) m ORDER BY m.INVOICE_DATE,m.INVOICE_NO"
        return query

    end

    def query_get_supp_inv_car(tables,cause)
        #tables = ["ST_PURCHASE","ST_PURCHASEHISTORY"]
        list=[]
        for table in tables
            list<<" (
                    SELECT
                        UPPER(c.SUPPLIER_CD) AS PARTNER_NO,
                         c.FRAME_NO,
                         o.MTOC_CD,c.TRUE_RECEIVING_NO as PICK_NO,
                         c.DISTINVOICE_NO AS INVOICE_NO,c.DISTCN_NO as CN_NO,
                         c.RECEIVING_DATE AS INV_DOC_DATE,
                         c.RECEIVING_CANCEL_DATE as CN_DOC_DATE,
                         c.DISTCN_DATE as DATE_CN,
                         c.DISTINVOICE_DATE as DATE_INVOICE,
                         rtrim(c.DISTINVOICE_NO)+' '+o.ORDER_NO AS ORIGIN,
                         RTRIM(c.DISTINVOICE_NO)+' '+c.RECEIVING_NO AS ORIGIN_STOCK,
                         c.CARPRICE_CR as AMOUNT_UNTAXED,c.CARPRICEVAT_CR as AMOUNT_TAX,c.REGISTRATION_DATE,
                         case when c.SUPPLIER_KBN='01' then 'honda'
                      when c.SUPPLIER_KBN='02' then 'dealer'
                      when c.SUPPLIER_KBN='12' then 'branch'
                      else null end as CATEGORY
                    FROM st_order o
                         JOIN #{table} c
                            ON (c.RECEIVING_NO = o.ORDER_NO)
                   WHERE o.supplier_kbn in ('01','02','12')
                        #{cause}
                    )"
        end
        sub_query = list.map! { |k| "#{k}" }.join(" UNION ")
        query = " SELECT * from (#{sub_query}) m order by m.REGISTRATION_DATE "
        return query
    end


    def query_get_supp_credit_car(tables,cause)
        #tables = ["ST_PURCHASE","ST_PURCHASEHISTORY"]
        #XXX: repeate statement : query_get_supp_inv_car
        list=[]
        for table in tables
            list<<" (
                    SELECT
                        UPPER(c.SUPPLIER_CD) AS PARTNER_NO,
                         c.FRAME_NO,
                         c.MTOC_CD,c.TRUE_RECEIVING_NO as PICK_NO,
                         c.DISTINVOICE_NO AS INVOICE_NO,c.DISTCN_NO as CN_NO,
                         c.RECEIVING_DATE AS INV_DOC_DATE,
                         c.RECEIVING_CANCEL_DATE as CN_DOC_DATE,
                         c.DISTCN_DATE as DATE_CN,
                         c.DISTINVOICE_DATE as DATE_INVOICE,
                         rtrim(c.DISTINVOICE_NO)+' '+o.ORDER_NO AS ORIGIN,
                         RTRIM(c.DISTINVOICE_NO)+' '+c.RECEIVING_NO AS ORIGIN_STOCK,
                         c.CARPRICE_CR as AMOUNT_UNTAXED,c.CARPRICEVAT_CR as AMOUNT_TAX,c.REGISTRATION_DATE,
                         case when c.SUPPLIER_KBN='01' then 'honda'
                      when c.SUPPLIER_KBN='02' then 'dealer'
                      when c.SUPPLIER_KBN='12' then 'branch'
                      else null end as CATEGORY
                    FROM st_order o
                         JOIN #{table} c
                            ON (c.RECEIVING_NO = o.ORDER_NO)
                   WHERE o.supplier_kbn in ('01','02')
                        #{cause}
                    )"
        end
        sub_query = list.map! { |k| "#{k}" }.join(" UNION ")
        query = " SELECT * from (#{sub_query}) m order by m.REGISTRATION_DATE "
        return query
    end

    def check_supp_invoice_car_import_missing(doc_nos,date_from,date_to,doc_no1,doc_no2,company_id,branch_code)
        ids_1=self.execute "account.invoice","search",[["reference","ilike",doc_no1],["date_invoice",">=",date_from],["date_invoice","<=",date_to],["company_id","=",company_id],["type","=","in_invoice"]]
        ids_2=self.execute "account.invoice","search",[["reference","ilike",doc_no2],["date_invoice",">=",date_from],["date_invoice","<=",date_to],["company_id","=",company_id],["type","=","in_invoice"]]

        iw_nos=self.execute "account.invoice","read",ids_1,["reference"] 
        iw_refs=Array.new
        j=0
        iw_nos.each do |i| 
            iw_refs.push(iw_nos[j] ["reference"])
            j=j+1
        end
        pm_references=iw_refs.to_set
        k=0
        sz_nos=self.execute "account.invoice","read",ids_2,["reference"]
        sz_refs=Array.new 
        sz_nos.each do |i| 
            sz_refs.push(inv_nos[k] ["reference"])
            k=k+1
        end
        sz_references=inv_refs.to_set
        missing=Array.new
        if iw_nos.empty? and sz_nos.empty?
            return nil
        end
        dms_count=0
        for row in rows
            dms_count=dms_count+1
            pp row
            inv_no=row["DISTINVOICE_NO"].strip
            exist_iw=iw_references.include? pm_no
            if not exist_iw
                exist_sz=sz_references.include? inv_no
                if not exist_inv
                    missing.push(pm_no)
                end
            end
        end
        return missing
    end

    def get_partner(code,company_id,branch_code=nil,options={})
        code = code.upcase

        partner_id=self.get_cache(["partner",code,company_id])
        return partner_id if partner_id


        #First: Try to search with ref and company
        #Get original partner
        domain=["&",["ref","=",code],"|",["company_id","=",company_id],["dms_company_id","=",company_id]]
        #if dummy name and nocompany passed search with no company_id
        if options[:nocompany] or code=="999999999999"
            domain=[["ref","=",code]]
        end

        res=self.execute "res.partner","search",domain

        #Try to search with ref

        if not res.empty?
            partner_id=res[0]
        else
            #Try with inactive partner
            #TODO: check finding condition
            res=self.execute "res.partner","search",[["ref","=",code],["active","=",0],"|",["dms_company_id","=",company_id],["company_id","=",company_id],]
            if not res.empty?
                partner_id=res[0]
            else
                #if not any required params passed report missing
                if branch_code.nil?
                    DMS_OpenERP.record_missing "partner",code
                    raise "Partner code not found: #{code}" if options[:required]
                    return nil
                else
                    imp=DMS_OpenERP.get_importer "partner" # import new partner
                    imp.start_import "branch_code"=>branch_code,"no_update"=>"-c"
                    partner_id=imp.import_by_no code
                end
            end
        end

        #{{{ Tempolary used
        if branch_code and code!="999999999999"
            imp=DMS_OpenERP.get_importer "partner" # import new partner
            imp.start_import "branch_code"=>branch_code,"no_update"=>"-c"
            imp.import_by_no code
        end
        #}}}

        self.set_cache(["partner",code,company_id],partner_id)
        return partner_id
    end

    def get_partner_address(partner_id,options={})

        type=options[:type]
        id=self.get_cache(["address",partner_id,type])
        return id if id
        id=nil
        if type
            #If address type provided search with type
            res=self.execute "res.partner.address","search",[["partner_id","=",partner_id],["type","=",type]]
            if not res.empty?
                id=res[0]
            else
                return nil
            end
        else
            #if no address type search and pick one
            res=self.execute "res.partner.address","search",[["partner_id","=",partner_id]]
            if not res.empty?
                id=res[0]
            end
        end
        if not id
            res= self.execute "res.partner","address_get",partner_id,[type]
            id= (not res.empty?) ? res[type] : nil

        end
        if not id
            raise "Address not found: #{partner_id}" if options[:required]
            return nil
        end
        self.set_cache(["address",partner_id,type],id)
        return id
    end

    def get_partner_name(partner_id)
        p "Get_partner_name"
        id=self.get_cache(["partner_name",partner_id])
        return id if id
        res=self.execute "res.partner","name_get",[partner_id]
        pp res
        name=res[0][1]
        self.set_cache(["partner_name",partner_id],name)
        name
    end

    def get_company(code,options={})
        id=self.get_cache(["company",code])
        return id if id
        #partner_id=self.get_partner code,nil,options=options
        res=self.execute "res.company","search",[["name","like",code]]
        if res.empty?
            raise "Company not found: #{code}" if options[:required]
            return nil
        end
        company_id=res[0]
        self.set_cache(["company",code],company_id)
        return company_id
    end

    def get_parent_company(company_id,options={})
        id=self.get_cache(["parent_company",company_id])
        return id if id
        res=self.execute "res.company","read",[company_id],["parent_id"]
        parent_id=res[0]["parent_id"][0]
        self.set_cache(["parent_company",company_id],parent_id)
        return parent_id
    end

    def get_vehicle(frame_no,branch_code=nil,options={})
        vehicle_id=self.get_cache(["vehicle",frame_no])
        return vehicle_id if vehicle_id

        if frame_no.empty? or frame_no.nil?
            return nil
        end

        res=self.execute "ac.vehicle","search",[["name","=",frame_no]]
        if res.empty?
            if branch_code.nil?
                DMS_OpenERP.record_missing "vehicle",frame_no
                raise "Vehicle not found: #{frame_no}" if options[:required]
                return nil
            else
                imp=DMS_OpenERP.get_importer "vehicle"
                imp.start_import "branch_code"=>branch_code,"no_update"=>"-c"
                vehicle_id=imp.import_by_no frame_no
            end
        else
            vehicle_id=res[0]
        end
        self.set_cache(["vehicle",frame_no],vehicle_id)
        return vehicle_id
    end

    def is_ascii(str)
        str.each_byte {|c| return false if c>=128}
        true
    end

    def convert_product_code(code)
        # remove special non-ascii
        if not is_ascii(code)
            code = code.gsub!(/\P{ASCII}/, '')
        end

        return code
    end


    def get_product(code,branch_code=nil,type=nil,options={})

        code = convert_product_code(code)

        product_id=self.get_cache(["product",code])
        return product_id if product_id
        res=self.execute "product.product","search",[["default_code","=",code]]
        if res.empty?
            if branch_code.nil? or type.nil?
                DMS_OpenERP.record_missing "product",code
                raise "Product not found: #{code}" if options[:required]
                return nil
            else
                imp=DMS_OpenERP.get_importer type
                imp.start_import "branch_code"=>branch_code,"no_update"=>"-c"
                product_id=imp.import_by_no code
            end
        end
        product_id=res[0]
        self.set_cache(["product",code],product_id)
        return product_id
    end

    def get_production_lot(name,product_id,options={})
        id=self.get_cache(["prodlot",name])
        return id if id
        res=self.execute "stock.production.lot","search",[["name","=",name],["product_id","=",product_id]]
        if res.empty?
            DMS_OpenERP.record_missing "prodlot",name
            raise "Production lot not found: #{name}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["prodlot",name],id)
        return id
    end

    def get_prodlot(name,options={})
        id=self.get_cache(["prodlot",name])
        return id if id
        res=self.execute "stock.production.lot","search",[["name","=",name]]
        if res.empty?
            DMS_OpenERP.record_missing "prodlot",name
            raise "Production lot not found: #{name}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["prodlot",name],id)
        return id
    end

    def get_product_category(name,options={})
        id=self.get_cache(["product_category",name])
        return id if id
        res=self.execute "product.category","search",[["name","=",name]]
        if res.empty?
            raise "Product category not found: #{name}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["product_category",name],id)
        return id
    end

    def get_partner_category(name,options={})
        id=self.get_cache(["partner_category",name])
        return id if id
        res=self.execute "res.partner.category","search",[["name","=",name]]
        if res.empty?
            raise "Partner category not found: #{name}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["partner_category",name],id)
        return id
    end

    def get_country(code,options={})
        id=self.get_cache(["country",code])
        return id if id
        res=self.execute "res.country","search",[["code","=",code]]
        if res.empty?
            raise "Country not found: #{code}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["country",code],id)
        id
    end

    def get_location(name,options={})
        id=self.get_cache(["location",name])
        return id if id
        res=self.execute "stock.location","search",[["name","=",name]]
        if res.empty?
            raise "Location not found: #{name}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["location",name],id)
        id
    end

    def get_red_plate(plate_no,options={})
        id=self.get_cache(["red_plate",plate_no])
        return id if id
        res=self.execute "ac.red.plate","search",[["name","=",plate_no]]
        if res.empty?
            raise "Red plate not found: #{plate_no}" if options[:required]
            return nil
        end
        id=res[0]
        self.set_cache(["red_plate",plate_no],id)
        return id
    end

    def get_user(user_code,branch_code=nil,options={})
        user_id=self.get_cache(["user",user_code])
        return user_id if user_id
        res=self.execute "res.users","search",[["login","=",user_code],["active","=",0]]

        # not found by inactive try to search with active
        if res.empty?
            res=self.execute "res.users","search",[["login","=",user_code],["active","=",1]]
        end

        if res.empty?
            if branch_code.nil?
                DMS_OpenERP.record_missing "sale_user",user_code
                raise "User not found: #{user_code}" if options[:required]
                return nil
            else
                imp=DMS_OpenERP.get_importer "sale_user"
                imp.start_import "branch_code"=>branch_code,"no_update"=>"-c"
                user_id=imp.import_by_no user_code
            end
        else
            user_id=res[0]
        end
        self.set_cache(["user",user_code],user_id)
        return user_id
    end

    def get_booking(booking_no,branch_code=nil,options={})
        booking_id=self.get_cache(["booking",booking_no])
        return booking_id if booking_id

        res=self.execute "ac.booking","search",[["name","=",booking_no]]
        if res.empty?
            res=self.execute( "ac.booking","search",[["name","=",booking_no],["active","=",false]])
        end

        #if not found booking try to create new
        if res.empty?

            #if required params doesn't passed , report missing
            if branch_code.nil?
                DMS_OpenERP.record_missing "booking",booking_no
                raise "Booking not found: #{booking_no}" if options[:required]
                return nil

            # try to import new booking
            else
                imp=DMS_OpenERP.get_importer "booking"
                imp.start_import "branch_code"=>branch_code,"no_update"=>"-c"
                booking_id=imp.import_by_no booking_no
            end

        #found booking in db
        else
            booking_id=res[0]
        end
        self.set_cache(["booking",booking_no],booking_id)
        return booking_id
    end

    def get_origin_invoice_like(origin,company_id,branch_code=nil,type=nil,doc_no=nil,partner_id=nil,options={})
        invoice_id=self.get_cache(["original_invoice",origin])
        return invoice_id if invoice_id

        doc_no = origin if doc_no.nil?
        #try to search using origin
        domain=[["origin","like",origin],["type","in",["out_invoice","out_cash"]],["company_id","=",company_id]]
        if not partner_id.nil?
            domain += [["partner_id","=",partner_id]]
        end
        #
        res=self.execute "account.invoice","search",domain

        if res.empty?
            if branch_code.nil? or type.nil? or doc_no.nil?
                raise "Original invoice not found: #{origin}" if options[:required]
                return nil
            else
                imp=DMS_OpenERP.get_importer type
                imp.start_import "branch_code"=>branch_code,"no_update"=>"-c"
                invoice_id=imp.import_by_no doc_no
            end
        else
            invoice_id=res[0]
        end

        self.set_cache(["original_invoice",origin],invoice_id)
        return invoice_id
    end


    def get_origin_invoice(origin,company_id,branch_code=nil,type=nil,doc_type=nil,doc_no=nil,options={})
        invoice_id=self.get_cache(["original_invoice",origin])
        return invoice_id if invoice_id

        doc_no = origin if doc_no.nil?

        domain=[["company_id","=",company_id]]
        if not type.nil?
            domain += [["type","=",type]]
        end

        #try to search using origin
        res=self.execute "account.invoice","search",[["origin","=",origin]]+domain
        #if not found using reference
        if res.empty?
            res=self.execute "account.invoice","search",[["reference","=",origin]]+domain
        end

        if res.empty?
            if branch_code.nil? or doc_type.nil? or doc_no.nil?
                raise "Original invoice not found: #{origin}" if options[:required]
                return nil
            else
                imp=DMS_OpenERP.get_importer doc_type
                imp.start_import "branch_code"=>branch_code,"no_update"=>"-c"
                invoice_id=imp.import_by_no doc_no
            end
        else
            invoice_id=res[0]
        end

        self.set_cache(["original_invoice",origin],invoice_id)
        return invoice_id
    end

    def get_invoice_id(reference,company_id,type=nil,options={})
        id=self.get_cache(["invoice_id",reference])
        return id if id

        domain=[]
        if not type.nil?
            domain=[["type","=",type]]
        end
        res=self.execute "account.invoice","search",[["reference","=",reference],['company_id','=',company_id]]+domain

        if res.empty?
            res=self.execute "account.invoice","search",[["origin","=",reference],['company_id','=',company_id]]+domain
        end
        if res.empty?
            raise "invoice not found: #{origin}" if options[:required]
            return false
        end
        id=res[0]
        self.set_cache(["invoice_id",reference],id)
        return id
    end

    # try to merge partners (called when importing vehicle owner and user history)
    def try_merge_partner(new_partner_id,old_partner_id)
        res=execute "res.partner","read",[new_partner_id,old_partner_id],["name"]
        new_name=res[0]["name"]
        old_name=res[1]["name"]
        return false if new_name!=old_name # name is not the same => don't merge
        #comment="Merged with #{old_partner_id}"
        #execute "res.partner","write",[new_partner_id],{"active"=>false,"comment"=>comment,"merged_id"=>old_partner_id}
        #ids=execute "partner.branch.code","search",[["partner_id","=",new_partner_id]]
        #execute "partner.branch.code","write",ids,{"partner_id"=>old_partner_id}

        execute "res.partner","do_merge",old_partner_id,[new_partner_id]

        return true
    end

    def create_address_invoice_booking(row)
        vals={
            "name"=>(row["FIRST_NM"] or "").strip+" "+(row["LAST_NM"] or "").strip,
            "function"=>(row["SIR_SHORT_NM"] or "").strip,
            "street"=>(row["DETAILS"] or "").strip,
            "street2"=>(row["TOWNCITY"] or "").strip,
            "zip"=>(row["ZIP_CD"] or "").strip,
            "country_id"=>get_country("TH",:required=>true),
            "city"=>(row["DISTRICT"] or "").strip+" "+(row["PROVINCE"] or row["PROVINVE"] or "").strip,
            "phone"=>(row["TEL_NO"] or "").strip,
            "mobile"=>(row["MOBILE_NO"] or "").strip,
            "fax"=>(row["FAX_NO"] or "").strip,
        }
        addr_id=execute "res.partner.address","create",vals
        addr_id
    end

    def create_address_invoice_part(row)
        vals={
            "name"=>(row["FIRST_NM"] or "").strip+" "+(row["LAST_NM"] or "").strip,
            "function"=>(row["SIR_NM"] or "").strip,
            "street"=>(row["DETAILS_NM"] or "").strip,
            "street2"=>(row["CITYTOWN_NM"] or "").strip,
            "zip"=>(row["ZIP_CD"] or "").strip,
            "country_id"=>get_country("TH",:required=>true),
            "city"=>(row["DISTRICT_NM"] or "").strip+" "+(row["PROVINCE_NM"] or "").strip,
            "phone"=>(row["TELEPHONE_NO"] or "").strip,
            "fax"=>(row["FAX_NO"] or "").strip,
        }
        addr_id=execute "res.partner.address","create",vals
        addr_id
    end

    def create_address_invoice_service(row)

        # TODO: should use the same function as partner.rb

        street = row["ADDRESS_DETAILS"].strip.gsub("\u001E","")

        # hot fix for RI1410V1210
        #if street.match(/111/)
            #street = "111/33 หมู่ที่ 11 ซ.ธนสิทธิ์ ถ.เทพารักษ์"
        #end


        vals={
            "name"=>(row["CUSTOMER_NM"] or "").strip,
            "function"=>(row["SIR_NM"] or "").strip,
            "street"=>street,
            "street2"=>(row["ADDRESS_TOWNCITY"] or "").strip,
            "zip"=>(row["ZIP_CD"] or "").strip,
            "country_id"=>get_country("TH",:required=>true),
            "city"=>(row["ADDRESS_DISTRICT"] or "").strip+" "+(row["ADDRESS_PROVINCE"]).strip,
            "phone"=>(row["TEL_HOME_NO"] or "").strip,
            "mobile"=>(row["TEL_MOBILE_NO"] or "").strip,
        }
        addr_id=execute "res.partner.address","create",vals
        addr_id
    end

    def create_address_credit_booking(row)
        vals={
            "name"=>(row["FIRST_NM"] or "").strip+" "+(row["LAST_NM"] or "").strip,
            "function"=>(row["SIR_SHORT_NM"] or "").strip,
            "street"=>(row["DETAILS"] or "").strip,
            "street2"=>(row["TOWNCITY"] or "").strip,
            "zip"=>(row["ZIP_CD"] or "").strip,
            "country_id"=>get_country("TH",:required=>true),
            "city"=>(row["DISTRICT"] or "").strip+" "+(row["PROVINCE"] or row["PROVINVE"] or "").strip,
            "phone"=>(row["TEL_NO"] or "").strip,
        }
        addr_id=execute "res.partner.address","create",vals
        addr_id
    end

    def create_address_invoice(row)
        street = row["STREET"].strip
        # Hot Fix KI1502V0051
        #if street.match(/111/)
            #street = "111/33 หมู่ที่ 11 ซ.ธนสิทธิ์ ถ.เทพารักษ์"
        #end
        vals={
            "name"=>(row["NAME"] or "").strip,
            "street"=>street,
            "street2"=> (row["STREET2"] or "").strip,
            "zip"=>(row["ZIP"] or "").strip,
            "country_id"=>get_country("TH",:required=>true),
            "city"=>(row["CITY"] or "").strip,
            "phone"=>(row["PHONE"] or "").strip,
            "mobile"=>(row["MOBILE"] or "").strip,
        }
        addr_id=execute "res.partner.address","create",vals
        return addr_id
    end

    def create_address_invoice_car_full(row)
        code=(row["SUPPLIER_NO"] or row["CUSTOMER_NO"]).strip
        if code[0,1]=='S'
            street=(row["ADDRESS_NO"] or "").strip+" "+(row["VILLAGE"] or "").strip+" "+(row["SOI"] or "").strip+" "+(row["ROAD"] or "").strip
        else
            street=(row["DETAILS"] or "").strip
        end

        vals={
            "name"=> (row["FIRST_NM"] or "").strip+" "+(row["LAST_NM"] or "").strip, 
            "function"=>(row["SIR_NM"] or "").strip,
            "street"=>street,
            "street2"=>(row["TOWNCITY"] or "").strip,
            "zip"=>(row["ZIP_CD"] or "").strip,
            "country_id"=>get_country("TH",:required=>true),
            "city"=>(row["DISTRICT"] or "").strip+" "+(row["PROVINCE"] or "").strip,
            "phone"=>(row["TEL_NO"] or "").strip,
            "mobile"=>(row["MOBILE_NO"] or "").strip,
            "fax"=>(row["FAX_NO"] or "").strip,
        }
        addr_id=execute "res.partner.address","create",vals
        addr_id
    end

    def create_address_invoice_claim_free_service(row)
        vals={
            "name"=>(row["FIRST_NM"] or "").strip+" "+(row["LAST_NM"] or "").strip,
            "function"=>(row["SIR_NM"] or "").strip,
            "street"=>(row["DETAILS_NM"] or "").strip,
            "street2"=>(row["CITYTOWN_NM"] or "").strip,
            "zip"=>(row["ZIP_CD"] or "").strip,
            "country_id"=>get_country("TH",:required=>true),
            "city"=>(row["DISTRICT_NM"] or "").strip+" "+(row["PROVINCE_NM"] or "").strip,
            "phone"=>(row["TELEPHONE_NO"] or "").strip,
            "fax"=>(row["FAX_NO"] or "").strip,
        }
        addr_id=execute "res.partner.address","create",vals
        addr_id
    end

    def update_picking(pick,reference,company_id)
        invoice_id=self.get_invoice_id(reference,company_id)
        self.execute "stock.picking","write",pick, {"invoice_id"=>invoice_id}
    end

    def get_analytic_from_code(prefix, model_name,company_id,import_flg=false)

        analytic_id = self.get_cache(["analyic",model_name,company_id,import_flg])
        return analytic_id if analytic_id

        code=prefix
        p model_name
        if model_name.match(/CITY/)
            code+="CITY"
        elsif model_name.match(/CR-V/) or model_name.match(/CRV/)
            code+="CRV"
        elsif model_name.match(/HR-V/)
            code+="HRV"
        elsif model_name.match(/BR-V/)
            code+="BRV"
        elsif model_name.match(/CR-Z/)
            code+="CRZ"
        elsif model_name.match(/JAZZ/)
            if import_flg == "Y"
                code+="JAZZ_JP"
            else
                code+="JAZZ"
            end
        elsif model_name.match(/BRIO/)
            code+="BRIO"
        elsif model_name.match(/CIVIC/)
            code+="CIVIC"
        elsif model_name.match(/ODYSSEY/)
            code+="ODYSSEY"
        elsif model_name.match(/ACCORD/) or model_name.match(/ACCPRD/) #with honda typo
            if import_flg == 'Y'
                code+="ACCORD_JP"
            else
                code+="ACCORD"
            end
        elsif model_name.match(/FREED/)
            code+="FREED"

        elsif model_name.match(/MUV/) or model_name.match(/MOBILIO/)
            code+="MOBILIO"
        #STEP WGN SPADA JP
        elsif model_name.match(/STEP/) or  model_name.match(/WGNSPADA/)
            code+="STEP_WAGON"
        else
            #raise "Invalid model name: #{model_name}"
            code+="OTHER"

        end
        analytic_id = self.get_analytic_account code,company_id,:required=>true

        self.set_cache(["analyic",model_name,company_id,import_flg],analytic_id)

        return analytic_id
    end

end

module ERP
    @@con=nil

    def self.get_connection
        return @@con if @@con
        @@con=ERPConnection.new
        @@con.connect
        return @@con
    end
end
