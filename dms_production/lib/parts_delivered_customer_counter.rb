require "dms"
require "erp"
require "importer"
require "pp"

class Importer_PartsDeliveredCustomerCounter < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM PT_PKHMST WHERE DELIVERY_NO='#{doc_no}' OR INVOICE_NO='#{doc_no}'"
        res=@dms.get q
        raise "Picking not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM PT_PKHMST WHERE INVOICE_NO IS NOT NULL AND PICKING_DATE>='#{date_from} 00:00:00' AND PICKING_DATE<='#{date_to} 23:59:59' AND INVOICE_NO LIKE 'PI%'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PT_PKHMST WHERE INVOICE_NO IS NOT NULL AND LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'PI%'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM PT_PKHMST WHERE INVOICE_NO IS NOT NULL AND INVOICE_NO LIKE 'PI%' AND LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def check_import(date_from,date_to)
        puts "Check_import_PD",date_from,date_to
        q="SELECT DELIVERY_NO,INVOICE_NO FROM PT_PKHMST WHERE INVOICE_NO LIKE 'PI%' AND PICKING_DATE>='#{date_from} 00:00:00' AND PICKING_DATE<='#{date_to} 23:59:59' ORDER BY DELIVERY_NO"
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="PI%"
        for row in rows
            invoice=row["DELIVERY_NO"].strip+" "+row["INVOICE_NO"].strip
            dms_docnos.push(invoice)
        end
        pp dms_docnos
        puts "################################################"
        erp_docnos=@erp.get_picking_docnos inv_no,"out",date_from,date_to,@company_id
        pp erp_docnos
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        delivery_no=row["DELIVERY_NO"].strip
        inv_no=row["INVOICE_NO"].strip

        origin=delivery_no+" "+inv_no
        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "UPDATED: Picking already created: #{origin}"
            #@erp.update_picking(res,inv_no,@company_id)
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)

        if not address_id
            address_id=@erp.get_partner_address(partner_id,:required=>true)
        end

        date=row["PICKING_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        invoice_id = @erp.get_invoice_id(inv_no,@company_id)

        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done"=>date,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>"out",
            "address_id"=>address_id,
            "stock_journal_id"=>@erp.get_stock_journal("PARTS_DELIVERED_CUSTOMER_COUNTER_JOURNAL",@company_id), #PART_PI
            "invoice_id"=>invoice_id,
        }

        lines=get_lines(delivery_no)
        pick_vals["move_lines"]=lines.collect {|line_vals| [0,0,line_vals]}

        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created picking #{delivery_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end

    #SPP TODO: this query can be done 1 no need to query for every picking
    def get_lines(delivery_no)
        q="
SELECT *
 FROM PT_PKDMST
 WHERE DELIVERY_NO='#{delivery_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            #pp "line_row",row
            part_no=row["PART_NO"].strip
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            line_no=row["LINE_NO"].to_s
            date=row["ORDER_DATE"].strftime "%Y-%m-%d %H:%M:%S"
            qty=row["DELIVERY_QTY"]
            vals={
                "name"=>delivery_no+" "+line_no,
                "date"=>date,
                "date_expected"=>date,
                "product_id"=>product_id,
                "product_uom"=>@erp.get_uom("PCE",:required=>true),
                "product_qty"=>qty,
                "location_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
                "location_dest_id"=>@erp.get_location("Customers",:required=>true),
                "state"=>"assigned",
                "price_unit"=>row["COST_PRICE"],
            }
            lines<<vals
        end
        lines
    end
end
