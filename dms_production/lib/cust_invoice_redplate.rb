require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoiceRedplate < Importer

    def import_by_no(doc_no)
        puts "Importer_Invoice_Redplate.import_by_no",doc_no
        q="SELECT * FROM ST_RENTAL WHERE SHEET_NO='#{doc_no}' OR BOOKING_NO='#{doc_no}'"
        res=@dms.get q
        raise "Booking invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        puts "Importer_Invoice_Redplate.import_by_date",date_from,date_to
        q="SELECT * FROM ST_RENTAL WHERE RENTAL_DATE>='#{date_from} 00:00:00' AND RENTAL_DATE<='#{date_to} 23:59:59' ORDER BY SHEET_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_Booking_Redplate.import_daily"
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        t0=from_t.strftime "%Y-%m-%d %H:00:00"
        q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_S7",date_from,date_to
        q="SELECT SHEET_NO FROM ST_RENTAL WHERE SHEET_NO IS NOT NULL AND RENTAL_DATE>='#{date_from} 00:00:00' AND RENTAL_DATE<='#{date_to} 23:59:59' ORDER BY SHEET_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["SHEET_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_invoice",date_from,date_to,"S7%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        puts "Importer_Cust_Invoice_Redplate.import_row"
        #pp "row:",row
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        t0=Time.now()
        puts "Creaing Redplate invoice............"
        inv_no=row["SHEET_NO"].strip
        booking_no=row["BOOKING_NO"].strip
        if inv_no.empty?
            @logger.info "SKIPPED : No Redplate invoice no........"
            return
        end

        car_invoice=@dms.get_car_invoice_from_booking booking_no
        car_invoice_no=car_invoice["INVOICE_NO"].strip
        origin=car_invoice_no+ " " + booking_no+" "+inv_no
        res=@erp.execute "account.invoice","search",[["origin","=",origin],["type","=","out_invoice"]]
        if not res.empty?
            @logger.info "SKIPPED : Invoice already created:#{origin}"
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address(partner_id,:type=>"invoice",:required=>true)
        if address_id.nil?
            address_id=@erp.get_partner_address(partner_id,:type=>"default",:required=>true)
        end

        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true
        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,:required=>true
        date_invoice=row["RENTAL_DATE"].strftime("%m/%d/%y")
        prod_name=row["TLP_NO"].strip
        invoice_name=car_invoice_no+" "+prod_name
        vals_={
            "origin" => origin,
            "reference" => inv_no,
            "name"=> invoice_name,
            "journal_id" => @erp.get_journal("CUST_INV_REDPLATE_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_invoice" ,
            "account_id" => @erp.get_account("CUST_INV_REDPLATE_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date" => date_invoice,
            "booking_id" => booking_id,
            "vehicle_id" => vehicle_id,
            "user_id" => user_id,
            "address_invoice_id" => address_id
        }

        prod_id=@erp.get_product "RED_PLATE",:required=>true
        price_unit=row["DEPOSIT_CR"]
        lines_={
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_INV_REDPLATE_DEPOSIT",@parent_company_id,:required=>true),
            "name" => prod_name,
            "product_id" => prod_id,
            "quantity" => 1,
            "price_unit" => price_unit,
            }

        vals_["invoice_line"]=[[0,0,lines_]]

        inv_id=@erp.execute "account.invoice","create",vals_
        @logger.info "CREATED : invoice : #{origin}=>#{inv_id}"

        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]


        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-price_unit).abs>0.25
            msg="wrong total in #{inv_no}: #{amt}/#{price_unit}"
            @logger.error msg
            raise msg
        end 
        #puts "Posting invoice............."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
