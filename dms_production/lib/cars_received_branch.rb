require "dms"
require "erp"
require "importer"
require "pp"
# SUPPLIER_KBN=12 : branch of same company
# SUPPLIER_KBN=02 : other dealer
# DOC_NO=SZ
#
#TODO: merge query : @erp.query_get_supp_inv_car
class Importer_CarsReceivedBranch < Importer
    def query_get(number,date_from=false,date_to=false,last_update=false)
        #LAST_UPDATE_DATE always same as RECEIVING_DATE
        #
        cause= date_from!=false ?  " AND c.RECEIVING_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND c.RECEIVING_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( c.DISTINVOICE_NO='#{number}' OR c.RECEIVING_NO='#{number}' OR c.FRAME_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND c.LAST_UPDATE_DATE>='#{last_update}'": "")

        #Union 2 tables
        # defined query in erp class
        tables = ["ST_PURCHASE","ST_PURCHASEHISTORY"]
        query = @erp.query_get_supp_inv_car(tables,cause)
        return query
        end

    def import_by_no(doc_no)
        q = self.query_get(doc_no)
        res=@dms.get q
        raise "Car received branch/dealer not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q = self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q = self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to,false)
        rows=@dms.query q
        miss=Array.new
        for row in rows
            invoice=row["ORIGIN"]
            inv_id=@erp.get_picking_id invoice,nil,@company_id
            if not inv_id
                miss.push(invoice)
            end
        end
        return miss
    end

    def import_row(row)
        #ignore hd data
        if row["CATEGORY"]=="honda"
            return
        end
        origin=row["ORIGIN"].strip
        res=@erp.execute "stock.picking","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "UPDATED : Picking already created: #{origin}"

            if row["TYPE"]=="dealer"
                @erp.update_picking(res,origin,@company_id)
            end
            return
        end

        date=row["INV_DOC_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        partner_no=row["PARTNER_NO"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)
        if not address_id
            address_id=@erp.get_partner_address(partner_id,:required=>true)
        end


        pick_type="internal"
        stock_journal_id= @erp.get_stock_journal("CARS_RECEIVED_OTHER_BRANCH_JOURNAL",@company_id)
        location_id= @erp.get_location(@branch_code+"-Transfer",:required=>true)

        invoice_id=false
        if row['CATEGORY']=='dealer'
            pick_type="in"
            stock_journal_id= @erp.get_stock_journal("CARS_RECEIVED_GROUP_JOURNAL",@company_id)
            location_id= @erp.get_location("Suppliers",:required=>true) # orig : name might be use XX-Dealers
            #location_id= @erp.get_location(@branch_code+"-Dealers",:required=>true)
            invoice_id=@erp.get_invoice_id(origin,@company_id)
        end

        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done"=>date,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>pick_type,
            "partner_id"=>partner_id,
            "address_id"=>address_id,
            "stock_journal_id"=>stock_journal_id,
            "invoice_id" => invoice_id,
        }
        frame_no=row["FRAME_NO"].strip
        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        prodlot_id=@erp.get_prodlot frame_no
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        line_vals={
            "name"=>frame_no,
            "date_expected"=>date,
            "product_id"=>product_id,
            "product_uom"=>@erp.get_uom("PCE",:required=>true),
            "product_qty"=>1,
            "location_id"=>location_id,
            "location_dest_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
            "state"=>"assigned",
            "prodlot_id"=>prodlot_id,
            "vehicle_id"=>vehicle_id,
            "price_unit"=>row["AMOUNT_UNTAXED"],
        }
        pick_vals["move_lines"]=[[0,0,line_vals]]
        pp "vals",pick_vals
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "CREATED : picking #{origin}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end
end
