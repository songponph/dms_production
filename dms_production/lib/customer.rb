require "dms"
require "erp"
require "pp"
require "importer"

class Importer_Customer < Importer
    def import_by_no(partner_no)
        puts "Importer_Customer.import_by_no",partner_no
        q="SELECT * FROM SM_CUSTOMERBASIC WHERE CUSTOMER_NO='#{partner_no}'"
        res=@dms.get q
        raise "Customer not found: #{partner_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_Customer.import_by_date",date_from,date_to
        q="SELECT * FROM SM_CUSTOMERBASIC WHERE LAST_UPDATE_DATE>='#{date_from} 00:00:00' AND LAST_UPDATE_DATE<='#{date_to} 23:59:59' ORDER BY LAST_UPDATE_DATE"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_Customer.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM SM_CUSTOMERBASIC WHERE LAST_UPDATE_DATE>='#{t0}' ORDER BY LAST_UPDATE_DATE"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM SM_CUSTOMERBASIC WHERE LAST_UPDATE_DATE>='#{t0}' ORDER BY LAST_UPDATE_DATE"
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def check_import(date_from,date_to)
        q="SELECT CUSTOMER_NO FROM SM_CUSTOMERBASIC WHERE LAST_UPDATE_DATE>='#{date_from} 00:00:00' AND LAST_UPDATE_DATE<='#{date_to} 23:59:59' ORDER BY LAST_UPDATE_DATE"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["CUSTOMER_NO"].strip}
        missing=[]
        i=0
        for docno in dms_docnos
            i+=1
            puts "Checking doc #{i}/#{dms_docnos.length}"
            docno=docno.strip.upcase
            partner_id=@erp.get_partner docno,@company_id,@branch_code,"customer",:required=>true
            if not partner_id
                missing.push docno
            end
        end
        return missing
    end

    def import_row(row)
        puts "Importer_Customer.import_row"
        partner_no=row["CUSTOMER_NO"].strip.upcase
        name=row["FIRST_NM"].strip
        if not row["MIDDLE_NM"].empty?
            name+=" "+row["MIDDLE_NM"].strip
        end
        if not row["LAST_NM"].empty?
            name+=" "+row["LAST_NM"].strip
        end
        title_search=row["SIR_NM"].strip
        categ_id=@erp.get_partner_category "Customers",:required=>true
        puts name
        res=@erp.execute "partner.branch.code","search",[["code","=",partner_no],["company_id","=",@company_id]]
        code_id=res.empty? ? nil : res[0]
        partner_id=nil
        if code_id
            puts "found branch code"
            res=@erp.execute "partner.branch.code","read",[code_id],["partner_id"]
            partner_id=res[0]["partner_id"][0]
        else
            puts "branch code not found"
            res=@erp.execute "partner.branch.code","search",[["partner_id.name","=",name]]
            if not res.empty?
                puts "found code in other branch"
                other_code_id=res[0]
                res=@erp.execute "partner.branch.code","read",[other_code_id],["partner_id"]
                partner_id=res[0]["partner_id"][0]
            else
                puts "code not found in other branch"
            end
        end
        if row["SIR_NM"] != "NULL"
            title_id=@erp.get_name_title title_search
        end
        cust_vals={
            "name"=>name, #have to add the title field.
            "customer"=>true,
            "supplier"=>false,
            "category_id"=>[[6,0,[categ_id]]],
        }
        if title_id
            cust_vals["title"]=title_id
        end

        if not partner_id
            puts "creating new partner"
            partner_id=@erp.execute "res.partner","create",cust_vals

            @logger.info "CREATED : Partner: #{partner_no}=>#{partner_id}"
        end

        if not code_id
            puts "creating new branch code"
            code_vals={
                "partner_id"=>partner_id,
                "code"=>partner_no,
                "company_id"=>@company_id,
            }
            @erp.execute "partner.branch.code","create",code_vals
        end

        addr_vals={
            "partner_id"=>partner_id,
            "type"=>"default",
            "name"=>title_search+" "+name,
            "street"=>(row["DETAILS"] or "").strip,
            "street2"=>(row["TOWNCITY"] or "").strip,
            "zip"=>(row["ZIP_CD"] or "").strip,
            "country_id"=>@erp.get_country("TH",:required=>true),
            "city"=>(row["DISTRICT"] or "").strip+" "+(row["PROVINCE"] or "").strip,
            "phone"=>(row["TEL_NO"] or "").strip,
            "mobile"=>(row["MOBILE_NO"] or "").strip,
            "fax"=>(row["FAX_NO"] or "").strip,
        }
        addr_id=@erp.get_partner_address partner_id,:type=>"default"
        if not addr_id
            puts "creating new address"
            addr_id = @erp.execute "res.partner.address","create",addr_vals
            @logger.info "CREATED : Partner:address : #{addr_id}"
        end

        return partner_id
    end
end
