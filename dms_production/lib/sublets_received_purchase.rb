require "dms"
require "erp"
require "importer"
require "pp"

class Importer_SubletReceivedPurchase < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND P.DELIVERY_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND P.DELIVERY_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( P.PO_NO='#{number}' or P.RO_NO='#{number}') ": "")
        cause << (last_update!=false ? " AND P.LAST_UPDATE_DATE>='#{last_update}' ": "")
        list=[]
        for table in ["RR","RT"]
            list<<"
            (
            SELECT P.PO_NO,P.RO_NO,P.DELIVERY_DATE AS DATE,P.SUPPLIER_NO,
            M.COST AS PRICE_UNIT,m.ORDER_DETAIL AS NAME
            FROM #{table}_SUBLETJOB P,#{table}_SUBLETJOB_DETAIL M
            WHERE P.PO_NO LIKE 'RP%' and P.DELIVERY_DATE is not null and P.PO_NO=M.PO_NO
            #{cause}
            )
            "
        end
        sub_query = list.map! { |k| "#{k}" }.join(" UNION ")

        query = "SELECT * from (#{sub_query}) p ORDER BY p.PO_NO "

        return query
    end


    def import_by_no(doc_no)
        q = self.query_get(doc_no)
        res=@dms.get q
        raise "Job order not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q = self.query_get(false,date_from,date_to)

        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q = self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q = self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="RP%"
        for row in rows
            invoice=row["PO_NO"].strip+" "+row["RO_NO"].strip
            dms_docnos.push(invoice)
        end
        erp_docnos=@erp.get_picking_docnos inv_no,"in",date_from,date_to,@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        po_no=row["PO_NO"].strip
        ro_no=row["RO_NO"].strip
        origin=po_no+" "+ro_no

        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "SKIPPED : Picking already created: #{origin}"
            return
        end
        date=row["DATE"].strftime "%Y-%m-%d %H:%M:%S"
        partner_no = row["SUPPLIER_NO"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)
        if not address_id
            address_id=@erp.get_partner_address(partner_id,:required=>true)
        end

        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done"=>date,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"2binvoiced",
            "type"=>"in",
            #"partner_id"=>partner_id,
            "address_id"=>address_id,
            "stock_journal_id"=>@erp.get_stock_journal("SUBLET_RECEIVED_PURCHASE_JOURNAL",@company_id), #SUBLET
        }

        product_id=@erp.get_product "PSJ",:required=>true
        #sublet_detail=@dms.get_sublet_cost po_no,:required=>true
        #cost_price=sublet_detail["COST"]
        #order_detail=sublet_detail["ORDER_DETAIL"]

        line_vals={
            "name"=>row["NAME"],
            "date_expected"=>date,
            "date_planned"=>date,
            "product_id"=>product_id,
            "product_uom"=>@erp.get_uom("PCE",:required=>true),
            "product_qty"=>1,
            "location_id"=>@erp.get_location("Suppliers",:required=>true),
            "location_dest_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
            "state"=>"assigned",
            "price_unit"=>row["PRICE_UNIT"],
        }
        pick_vals["move_lines"]=[[0,0,line_vals]]
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "CREATED :  picking #{po_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        return pick_id
    end
end 
