require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoiceParts < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND I.INVOICE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND I.INVOICE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( I.PROFORMA_INVOICE='#{number}' or I.INVOICE_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND I.LAST_UPDATE_DATE>='#{last_update}' ": "")
        query="

            SELECT I.INVOICE_NO,
                   I.PROFORMA_INVOICE,
                   P.DELIVERY_NO,
                   I.CUSTOMER_TYPE,
                   I.INVOICE_DATE,
                   I.CUSTOMER_NO,
                   I.AMOUNT,
                   AT.TOTAL_CR AS AMOUNT_TOTAL,
                   AT.VAT_CR,
                   I.SIR_NM,
                   I.FIRST_NM,
                   I.LAST_NM,
                   I.DETAILS_NM,
                   I.CITYTOWN_NM,
                   I.ZIP_CD,
                   I.DISTRICT_NM,
                   I.PROVINCE_NM,
                   I.TELEPHONE_NO,
                   I.FAX_NO
            FROM PT_INVOICE I,
                 PT_PKHMST P,
                 AT_UNSETTLE_MAIN AT
            WHERE I.INVOICE_NO=P.INVOICE_NO
                AND AT.INVOICE_NO=I.INVOICE_NO
                AND I.INVOICE_NO NOT LIKE 'PZ%' "+cause+"
            ORDER BY I.INVOICE_DATE,
                     I.INVOICE_NO "

        return query
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        invoice_id = self.import_row(res)
        return invoice_id
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def ximport_realtime(from_t,progress=nil)
        # disaple part import realtime

        # 1 hour back
        t0=(from_t-3600).strftime "%Y-%m-%d %H:%M:00"

        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
        erp_docnos1=@erp.get_invoice_docnos "out_invoice",date_from,date_to,"PI%",@company_id
        erp_docnos2=@erp.get_invoice_docnos "out_cash",date_from,date_to,"PI%",@company_id
        erp_docnos=erp_docnos1 | erp_docnos2
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def update_invoice(invoice_id,reference,date,pmt_method,row)

        res=@erp.execute "account.invoice","read",invoice_id,["type","state","reference","origin","partner_id"]
        partner_id=res[0]["partner_id"][0]
        pm_no=row["INVOICE_NO"].strip
        origin = res[0]["origin"]
        inv_type=res[0]["type"]

        if not (inv_type=="out_cash" and res[0]["state"]=="draft")
            @logger.info "SKIPPED: Invoice already created: #{origin}"
            return false
        end

        if not res[0]["reference"] and not reference.nil?
            @erp.execute "account.invoice","write",invoice_id, {"reference"=>reference,"doc_date"=>date}
            #Compute tax to reset tax invoice no
            @erp.execute "account.invoice","button_compute",invoice_id
        end

        if inv_type=="out_cash"
            self.set_payment_method(invoice_id[0],pmt_method)
        end
        @logger.info "UPDATED: Invoice already created: #{origin}"
        return true
    end

    def import_row(row)
        pm_no=row["PROFORMA_INVOICE"].strip
        inv_no=row["INVOICE_NO"].strip
        #pick_no=row["DELIVERY_NO"].strip

        order_nos=@dms.get_part_delivery_nos inv_no #
        #
        order_no=@dms.get_part_order_no inv_no #

        pick_no = order_nos.map! { |k| "#{k}" }.join(" ")

        origin=pm_no+" "+inv_no+" "+pick_no

        if pm_no.empty? #this should not posible
            @logger.info "SKIPPED : PI not created yet:#{inv_no}"
            return
        end
        date=row["INVOICE_DATE"].strftime("%Y-%m-%d")

        res=@erp.execute "account.invoice","search",[["type","in",["out_invoice","out_cash"]],["reference","=",inv_no],["company_id","=",@company_id]]

        pmt_method = @dms.get_payment_method(pm_no,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id})

        if pmt_method.nil?
            @logger.info "SKIPPED : PI no payment method:#{inv_no}"
            return
        end

        if not res.empty?
            self.update_invoice(res,inv_no,date,pmt_method,row)
            return
        end


        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.create_address_invoice_part row #TODO: this should make generic function not create so many function that import address

        if pmt_method[0]=="credit"
            inv_type="out_invoice"
            journal_code="CUST_INV_PART_JOURNAL_CREDIT"
        else
            inv_type="out_cash"
            journal_code="CUST_INV_PART_JOURNAL_CASH"
        end

        if row["CUSTOMER_TYPE"].match /^610|^615|^620|^622|^623|^625/
            account_id=@erp.get_account("CUST_INV_PART_DEALER_RECEIVABLE",@parent_company_id,:required=>true)
            type="dealer"
        else
            account_id=@erp.get_account("CUST_INV_PART_CUST_RECEIVABLE",@parent_company_id,:required=>true)
            type="customer"
        end

        inv_vals={
            "origin"=>origin,
            "reference"=>inv_no,
            "journal_id"=>@erp.get_journal(journal_code,@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>inv_type,
            "account_id" => account_id,
            "date_invoice" => date,
            "doc_date"=>date,
            "address_invoice_id" => address_id,
            "dms_customer_type" =>row["CUSTOMER_TYPE"],
        }

        #FIXME: To minimize query this should make at the begining
        deposit=@dms.get_part_deposit_invoice_from_order order_no
        if not deposit.nil?
            deposit_inv_no=deposit["INVOICE_NO"].strip
            puts " deposit get ", deposit_inv_no
            deposit_invoice_id=@erp.get_origin_invoice(deposit_inv_no,@company_id,@branch_code,"out_deposit",
                "cust_invoice_part_deposit",:required=>true)

            deposit_amount=deposit["DEPOSIT_AMOUNT"]
            deposit_line_={
            "deposit_id"=>deposit_invoice_id,
            "amount"=> deposit_amount,
            }
            inv_vals["inv_deposit_lines"]=[[0,0,deposit_line_]]
        end

        lines=[]
        for order_no in order_nos
            lines+=get_lines(order_no,type)
        end

        if lines.empty?
            @logger.info "SKIPPED : No lines in invoice"
            return
        end
        inv_vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "CREATED : Invoice: #{inv_no}=>#{inv_id}"

        if inv_type=="out_cash"
            self.set_payment_method(inv_id,pmt_method)
        end


        @erp.execute "account.invoice","button_compute",[inv_id]
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total","amount_tax"]
        amt=res[0]["amount_total"]
        check_total=0.0


        check_total = row["AMOUNT_TOTAL"]

        if row["VAT_CR"] !=res[0]["amount_tax"]
            @erp.execute "account.invoice","set_manual_vat",[inv_id],false,row["AMOUNT"],row["VAT_CR"],@company_id
        end

        if not deposit.nil?
            check_amt = (row["AMOUNT"]-deposit_amount)
            check_amt_tax = check_amt * 0.07
            check_total = check_amt+check_amt_tax
        end

        #if (amt-check_total).abs>0.25 and (amt-check_total_1).abs>0.25
            #msg="wrong total in #{origin}: #{amt}/#{check_total}"
            #@logger.error msg
            #raise msg
        #end
        #
        if (amt-check_total).abs>0.25
            msg="wrong total in #{inv_no}: #{amt}/#{check_total}"
            @logger.error msg
            raise msg
        end

        if inv_type=="out_invoice"
            @erp.exec_workflow "account.invoice","invoice_open",inv_id
        end
        return inv_id
    end

    def get_lines(order_no,type)

        #NOTE: actually variale order_no should rename to pick_no
        #I just keep it the same
        pick_no = order_no
        q="
            SELECT T3.PART_TYPE,T3.PART_NAME_L as NAME, T1.order_line_no as LINE_NO,
            T1.PART_NO,T1.DELIVERY_QTY as PRODUCT_QTY,
            T2.UNIT_PRICE,
            case when (T2.DISCOUNT_RATE!=0)
                then ((T2.UNIT_PRICE*(T1.DELIVERY_QTY))-T1.AMOUNT)/(T1.DELIVERY_QTY)
                else null end  as DISCOUNT
            FROM PT_PKDMST T1 ,PT_RODMST T2,PM_SPMAST T3
            WHERE T1.PART_NO=T2.PART_NO and
                T3.PART_NO=T1.PART_NO and
                T1.ORDER_NO=T2.ORDER_NO and
                T1.ORDER_LINE_NO=T2.LINE_NO and
                T1.delivery_no='#{pick_no}'
            order by T1.ORDER_LINE_NO
            "

        rows=@dms.query(q)
        lines=[]
        for row in rows
            account_id=get_account row,type
            #analytic_id=@erp.get_analytic_account "PT_COUNTER",@company_id,:required=>true
            analytic_id=get_analytic_account(row,type)
            tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true

            uom_id=@erp.get_uom "PCE",:required=>true
            qty=row["PRODUCT_QTY"]
            price=row["UNIT_PRICE"]
            part_no=row["PART_NO"].strip
            part_name=row["NAME"]
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            vals={
                "name"=> part_name,
                "quantity"=>qty,
                "price_unit"=>price,
                "invoice_line_tax_id"=>[[6,0,[tax_id]]],
                "uos_id"=>uom_id,
                "account_id"=>account_id,
                "account_analytic_id"=>analytic_id,
                "product_id"=> product_id,
                "note" => order_no+" "+row["LINE_NO"].to_s,
                "discount" => row["DISCOUNT"],
            }
            lines<<vals
        end
        lines
    end

    #TODO:
    def get_account(row,type)
        part_type=row["PART_TYPE"]
        if type=="customer"
            if part_type=="6"
                code="CUST_INV_PART_INCOME_ACC"
            else
                code="CUST_INV_PART_INCOME_OTHER"
            end
        else
            code="CUST_INV_PART_DEALER_INCOME"
            if part_type=="6"
                code="CUST_INV_PART_DEALER_INCOME_ACC"
            end
        end
        @erp.get_account code,@parent_company_id,:required=>true
    end

    def get_analytic_account(row,type)
        code="PT_DEALER"
        if type=="customer"
            part_type=row["PART_TYPE"]
            part_type_group=case part_type
            when "1","2","3","4" then "GEN"
            when "5" then "OIL"
            when "6" then "ACC"
            else
                raise "Invalid part_type: #{part_type}"
            end
            code="CT_"+part_type_group
        end
        @erp.get_analytic_account code,@company_id,:required=>true
    end
end
