require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustCreditPartDeposit < Importer
    def query_get(number,date_from=false,date_to=false)
        cause= date_from!=false ?  " AND AT.INVOICE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND AT.INVOICE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( AT.NOTE_NO='#{number}' or AT.invoice_no='#{number}' ) ": "")

        query=""" select AT.* from AT_UNSETTLE_MAIN AT
            where  AT.settle_type='008' and AT.settle_kbn='1' and AT.NOTE_KBN='1'
            """ + cause + """
            order by AT.INVOICE_NO
        """
        return query
    end

    def import_by_no(doc_no)
        puts "Importer_CustCreditPartsDeposit.import_by_no",doc_no
        q=self.query_get(doc_no)

        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        puts "Importer_CustCreditPartsDeposit.import_by_date",date_from,date_to
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustCreditPartsDeposit.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d"
        q=self.query_get(false,t0,false)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d"
        #q=self.query_get(false,t0,false)
        #rows=@dms.query q
        #self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_PK",date_from,date_to
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["NOTE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_refund",date_from,date_to,"PK%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        puts "Importer_CustCreditPartsDeposit.import_row"
        note_no=row["NOTE_NO"].strip
        inv_no=row["PRE_INVO_NO"]
        origin=note_no+" "+inv_no
        res=@erp.execute "account.invoice","search",[["reference","=",note_no]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end

        partner_no=row["CUST_CD"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        date=row["INVOICE_DATE"].strftime("%m-%d-%y")
        inv_type="out_refund"
        original_invoice_id=@erp.get_origin_invoice inv_no,@company_id,@branch_code,"out_deposit","cust_invoice_part_deposit",inv_no,:required=>true

        res=@erp.execute "account.invoice","read",[original_invoice_id],["address_invoice_id","user_id"]
        address_id=res[0]["address_invoice_id"][0]
        user_id=res[0]["user_id"][0]

        inv_vals={
            "origin"=>origin,
            "reference"=>note_no,
            "journal_id"=>@erp.get_journal("CUST_INV_PART_DEPOSIT_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>inv_type,
            "account_id"=>@erp.get_account("CUST_INV_PART_DEPOSIT_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice"=>date,
            "doc_date"=>date,
            "orig_invoice_id" => original_invoice_id,
            "address_invoice_id"=>address_id,
            "user_id"=>user_id,
        }

        deposit=row["DEPOSIT_CR"]
        check_amount=deposit + row["VAT_CR"]


        uom_id=@erp.get_uom "PCE",:required=>true
        tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true

        line_1={
            "name"=> "Deposit for "+inv_no,
            "quantity"=>1,
            "price_unit"=>deposit,
            "uos_id"=>uom_id,
            "account_id"=>@erp.get_account("CUST_INV_PART_DEPOSIT",@parent_company_id,:required=>true),
            "invoice_line_tax_id"=>[[6,0,[tax_id]]],
           }
        inv_vals["invoice_line"]=[[0,0,line_1]]

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "invoice created: #{origin}=>#{inv_id}"
        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-check_amount).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{check_amount}"
            @logger.error msg
            raise msg
        end
        puts "Posting invoice............."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
