require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoiceService < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause = date_from!=false ?  " AND AT.INVOICE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND AT.INVOICE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( AT.rela_tran_no='#{number}' or AT.tran_no='#{number}' or AT.invoice_no='#{number}' ) ": "")
        cause << (last_update!=false ? " AND ( AT.LAST_UPDATE_DATE>='#{last_update}' or SV.LAST_UPDATE_DATE>='#{last_update}' ) ": "")

        query=@erp.query_get_service(['RR_INVOICE_H','RT_INVOICE_H'],cause)
        return query
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q = self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q = self.query_get(false,false,false,last_update=t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)

        #t0=from_t.strftime "%Y-%m-%d %H:%M:00"

        # 1 hour back
        t0=(from_t-3600).strftime "%Y-%m-%d %H:%M:00"

        q = self.query_get(false,false,false,last_update=t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        miss=Array.new
        for row in rows
            inv_no=row["INVOICE_NO"].strip
            internal_number=row["RO_NO"].strip
            origin=inv_no+ " "+internal_number
            dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
            erp_docnos=@erp.get_invoice_docnos("" ,date_from,date_to,inv_no,@company_id)
            miss=check_missing dms_docnos,erp_docnos
        end
        return miss
    end

    def update_invoice(invoice_id,reference,date,pmt_method,row)

        res=@erp.execute "account.invoice","read",invoice_id,["type","state","reference","origin","partner_id"]
        partner_id=res[0]["partner_id"][0]
        origin = res[0]["origin"]
        pm_no= origin.split("\s")[0]
        inv_type=res[0]["type"]

        # skip for credit sale
        #
        if not (inv_type=="out_cash" and res[0]["state"]=="draft")
            @logger.info "SKIPPED: Invoice already created: #{origin}"
            return false
        end

        #updat RI reference
        if not res[0]["reference"] and not reference.nil?
            @erp.execute "account.invoice","write",invoice_id, {"reference"=>reference,"doc_date"=>date}

            #compute tax to reset tax invoice no in account.tax.line
            @erp.execute "account.invoice","button_compute",invoice_id
        end

        # try to update wht info
        if inv_type=="out_cash"
            invoice_no = origin.split("\s")[0]
            wht_line = @dms.get_payment_method_wht(invoice_no)
            if not wht_line.nil?
                @erp.execute("account.invoice","action_add_wht_sale",invoice_id)
            end

            self.set_payment_method(invoice_id[0],pmt_method)

        end

        @logger.info "UPDATED: Invoice already created: #{origin}"
        return true
    end

    def import_row(row)
        invoice_id=self.create_invoice(row)
        move_id=self.create_account_move(row)
        return invoice_id
    end


    def create_invoice(row)
        t0=Time.now()
        pm_no=row["INVOICE_NO"].strip
        if pm_no.empty?
            @logger.info "SKIPPED : No RM number, nothing to import"
            return
        end
        internal_number=row["RO_NO"].strip

        doc_date=row["INVOICE_DATE"]
        rm_date =row["INVOICE_DATE"]

        date_invoice=row["REGISTRATION_DATE"]
        ri_date=row["TAX_INVOICE_DATE"]

        if doc_date.empty?
            @logger.info "SKIPPED : Payment not created yet"
            return
        end

        pmt_method = @dms.get_payment_method(pm_no,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id})

        inv_type = false
        inv_no=row["TAX_INVOICE_NO"].strip
        origin=pm_no+" "+internal_number

        type="out_invoice"

        # Return
        # differentiation of Credit Sale
        # > payment method is Credit ( 100% sure)
        # > rm_date!=ri_date
        #
        if ( pmt_method[0]=="credit" ) or (rm_date!=ri_date)
            inv_type="out_invoice"
            journal_code="CUST_INV_SERVICE_JOURNAL_CREDIT"
            ref=pm_no

        # differentiation of Cash Sale
        else # cash sale
            if inv_no.empty? or inv_no.nil?
                #@logger.info "SKIPPED : No RI number for cash sale, nothing to import"
                #return
                inv_no=false
            else
                inv_no=row["TAX_INVOICE_NO"].strip
            end
            inv_type="out_cash"
            ref=inv_no
            journal_code="CUST_INV_SERVICE_JOURNAL_CASH"
        end

        if inv_type==false
            @logger.info "SKIPPED : This RM is not import because no cases match #{pm_no}"
            return
        end

        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            self.update_invoice(res,inv_no,doc_date,pmt_method,row)
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        address_id=@erp.create_address_invoice_service row

        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        comment=""
        for flg in ["GR","PDI","PM","ACC","AM","BP"]
            comment+=" #{flg}" if row["#{flg}_FLG"]==1
        end

        if inv_type=="out_cash"
            date_invoice=row["TAX_INVOICE_DATE"]
            if date_invoice.empty?
                @logger.info "SKIPPED : Payment not created yet "
                return
            end
        end

        vals={
            "type" => inv_type,
            "origin" => origin,
            "reference" => ref,
            "comment" => comment,
            "journal_id" => @erp.get_journal(journal_code,@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "account_id" => @erp.get_account("CUST_INV_SERVICE_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date" => doc_date,
            "vehicle_id" => vehicle_id,
            "address_invoice_id" => address_id,
            "plate_ref" => row["PLATE_REF"].strip,
        }

        lines=[]
        t0_1=Time.now()
        lines+=get_job_lines(pm_no,inv_type,row['SOURCE_TABLE'])
        t0_2=Time.now()
        lines+=get_part_lines(pm_no,inv_type,row['SOURCE_TABLE'])
        t0_3=Time.now()
        lines+=get_other_lines(pm_no,inv_type,row['SOURCE_TABLE'])
        t0_4=Time.now()
        if lines.empty?
            @logger.info "SKIPPED : No lines in invoice=>skipped"
            return
        end
        vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}


        t1=Time.now()
        # create invoice
        inv_id=@erp.execute "account.invoice","create",vals

        # try to update wht info
        if inv_type=="out_cash"

            self.set_payment_method(inv_id,pmt_method)

            # set wht
            wht_line = @dms.get_payment_method_wht(pm_no)
            if not wht_line.nil?
                @erp.execute("account.invoice","action_add_wht_sale",[inv_id])
            end
        end


        t2=Time.now()
        @logger.info "CREATED : invoice : #{pm_no}=>#{inv_id} , #{doc_date} "
        @erp.execute "account.invoice","button_compute",[inv_id]

        t2_1=Time.now()
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total","amount_tax","plate_ref"]
        if row["VAT_CR"] !=res[0]["amount_tax"]
            @erp.execute "account.invoice","set_manual_vat",[inv_id],false,row["G_TOTAL_AMT"],row["VAT_CR"],@company_id
        end

        amt=res[0]["amount_total"]
        if (amt-row["TOTAL_AMT"]).abs>0.25
            msg="wrong total in #{pm_no}: #{amt}/#{row["TOTAL_AMT"]}"
            @logger.error msg
            raise msg
        end
        t2_2=Time.now()

        t3=Time.now()
        if inv_type=="out_invoice"
            @erp.exec_workflow "account.invoice","invoice_open",inv_id
        end
        t4=Time.now()
        #@logger.info "times: total=#{t4-t0} prepare=#{t1-t0} create=#{t2-t1} compute=#{t3-t2} post=#{t4-t3} job_lines=#{t0_2-t0_1} part_lines=#{t0_3-t0_2} other_lines=#{t0_4-t0_3} checking=#{t2_2-t2_1}"
        return inv_id
    end

    def get_job_lines(pm_no,inv_type,table="RR")
        q="
        SELECT
        RJ.RQMT_NO,RJ.JOB_INST_NO,RJ.JOB_INST_SEQ,RJ.JOB_NM,RJ.LABOR_COST,RJ.DISCOUNT_AMT,RJ.QTY,RJ.AMT,RJ.FRT_OWN,
        RJ.SHARED_FLG,RJ.WARR_FLG,RJ.FOC_FLG,
        RJ.JOB_TYPE_CD,
        RI.WORK_TYPE
         FROM #{table}_INVOICE_J RJ
         JOIN #{table}_INVOICE_I RI ON RI.RO_NO = RJ.RO_NO AND RI.INVOICE_SUB_NO = RJ.INVOICE_SUB_NO AND RI.RQMT_NO=RJ.RQMT_NO AND RI.JOB_INST_NO=RJ.JOB_INST_NO
         JOIN #{table}_INVOICE_H RH ON RH.RO_NO = RJ.RO_NO AND RH.INVOICE_SUB_NO = RJ.INVOICE_SUB_NO
         WHERE RH.PRE_INVOCE_NO='#{pm_no}'
        "
        rows=@dms.query(q)
        lines=[]
        for row in rows
            name="#{row['RQMT_NO']}J#{row['JOB_INST_SEQ']} #{row['JOB_NM'].strip}"

            #if name.match(/1J12/)
                #name="1J12" # hot fix for RI1410V1279
            #end
            account_id=self.get_account_job row
            analytic_id=self.get_analytic_job row["WORK_TYPE"]
            note=self.get_line_note row
            if inv_type=="out_invoice"
                tax_id=@erp.get_tax "OUT_VAT7_SUS_EXCL",:required=>true
            else
                tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
            end
            frt=false
            if row["FRT_OWN"]>0.0
                qty=row["FRT_OWN"]
                frt=qty
                price=row["AMT"]/qty
            else
                qty=row["QTY"]
                price=row["LABOR_COST"]
            end
            amt=row["AMT"]
            if row["SHARED_FLG"]==1
                qty = 1.0
                price = amt
            else
                if row["FOC_FLG"]==1 or row["WARR_FLG"]==1
                    discount=(price).to_s
                else
                    discount_amt= (qty*price-amt)/qty
                    if discount_amt>0.001
                        discount=discount_amt.to_s
                    else
                        discount=nil
                    end
                end
            end
            vals={
                "account_id" => account_id,
                "account_analytic_id" => analytic_id,
                "name" => name,
                "company_id" => @company_id,
                "note" => note,
                "quantity" => qty,
                "price_unit" => price,
                "invoice_line_tax_id"=>[[6,0,[tax_id]]],
                "dms_frt"=>frt,
            }
            vals["discount"]=discount if discount
            lines<<vals
        end
        lines
    end

    def get_part_lines(pm_no,inv_type,table="RR")
        q="
            SELECT RP.RQMT_NO,RP.PARTS_SEQ,RP.PARTS_NM,
            RI.WORK_TYPE,PM.PART_TYPE,
            RP.QTY,RP.AMT,RP.DISCOUNT_AMT,rp.RETAIL_PRC as BASE_AMT,
            RP.FOC_FLG,RP.WARR_FLG,
            RP.SERVICE_COST_PRC as PRICE_UNIT ,
            RP.RPT_FLG,
            RP.SHARED_FLG
             FROM #{table}_INVOICE_P RP
             JOIN #{table}_INVOICE_I RI ON RI.RO_NO = RP.RO_NO AND RI.INVOICE_SUB_NO = RP.INVOICE_SUB_NO AND RI.RQMT_NO=RP.RQMT_NO AND RI.JOB_INST_NO=RP.JOB_INST_NO
             JOIN #{table}_INVOICE_H RH ON RH.RO_NO = RP.RO_NO AND RH.INVOICE_SUB_NO = RP.INVOICE_SUB_NO
             JOIN PM_SPMAST PM ON PM.PART_NO = RP.PARTS_NO
             WHERE RH.PRE_INVOCE_NO='#{pm_no}'
            "

        rows=@dms.query(q)
        lines=[]
        for row in rows
            name="#{row['RQMT_NO']}P#{row['PARTS_SEQ']} #{row['PARTS_NM'].strip}"
            account_id=self.get_account_part row
            analytic_id=self.get_analytic_part row["WORK_TYPE"],row["PART_TYPE"]
            tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
            if inv_type=="out_invoice"
                tax_id=@erp.get_tax "OUT_VAT7_SUS_EXCL",:required=>true
            end
            uom_id=@erp.get_uom "PCE",:required=>true
            note=self.get_line_note row

            qty = row["QTY"]
            discount=0.0
            #price = row["PRICE_UNIT"]
            price = row["BASE_AMT"] # for some case not make data conrrect
            # WARR_FLG/SHARED_FLG is false and discount != 0
            # amt > 0 where discount>0

            if row["SHARED_FLG"]==1
                qty = 1.0
                price = row["AMT"]
            elsif row["FOC_FLG"]==1 or row["WARR_FLG"]==1
                #100% discount
                discount=(price).to_s
            else
                discount= row["DISCOUNT_AMT"]
            end
            vals={
                "name"=>name,
                "price_unit"=>price,
                "quantity"=>qty,
                "invoice_line_tax_id"=>[[6,0,[tax_id]]],
                "account_id"=>account_id,
                "account_analytic_id"=>analytic_id,
                "uos_id"=>uom_id,
                "note"=>note,
                "company_id" => @company_id,
            }
            vals["discount"]=discount if discount
            lines<<vals
        end
        lines
    end

    def get_other_lines(pm_no,inv_type,table="RR")
        q="
        SELECT RO.*,RH.*,RI.*
         FROM #{table}_INVOICE_O RO
         JOIN #{table}_INVOICE_H RH ON RH.RO_NO = RO.RO_NO AND RH.INVOICE_SUB_NO = RO.INVOICE_SUB_NO
         JOIN #{table}_INVOICE_I RI ON RI.RO_NO = RO.RO_NO AND RI.INVOICE_SUB_NO = RO.INVOICE_SUB_NO
         AND RI.RQMT_NO=RO.RQMT_NO AND RI.JOB_INST_NO=RO.JOB_INST_NO
         WHERE RH.PRE_INVOCE_NO='#{pm_no}'
        "
        rows=@dms.query(q)
        lines=[]
        for row in rows
            name="#{row['RQMT_NO']}O#{row['OTHERS_COST_SEQ']} #{row['OTHERS_COST_NM'].strip}"
            account_id=self.get_account_other row 
            analytic_id=self.get_analytic_other row["WORK_TYPE"]
            if inv_type=="out_invoice"
                tax_id=@erp.get_tax "OUT_VAT7_SUS_EXCL",:required=>true
            else
                tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
            end
            uom_id=@erp.get_uom "PCE",:required=>true
            note=self.get_line_note row
            amt=row["OTHER_AMT"]
            qty=row["QTY"]
            price=amt/qty
            if row["FOC_FLG"]==1 or row["WARR_FLG"]==1
                discount=amt.to_s
            else
                discount=nil
            end
            name = name.sub("\u001E","") # fix error unicode mixed
            vals={
                "name"=>name,
                "quantity"=>qty,
                "price_unit"=>price,
                "invoice_line_tax_id"=>[[6,0,[tax_id]]],
                "uos_id"=>uom_id,
                "note"=>note,
                "account_id"=>account_id,
                "company_id" => @company_id,
                "account_analytic_id"=>analytic_id,
            }
            vals["discount"]=discount if discount
            lines<<vals
        end
        lines
    end

    def get_line_note(row)
        note=case row["WORK_TYPE"]
        when 1 then "PDI"
        when 2 then "PM"
        when 3 then "GR"
        when 4 then "BP"
        when 5 then "ACC"
        when 6 then "ANN"
        else ""
        end
        for flag in ["RPT","WARR","FOC","SUBLET","SHARED"]
            note+=" "+flag if row[flag+"_FLG"]==1
        end
        note
    end

    def get_account_job(row)
        warr_flg=row["WARR_FLG"]
        if warr_flg==1
            code="CUST_INV_SERVICE_INCOME_JOB_WAR"
        else
            work_type=row["WORK_TYPE"]
            code=case work_type
            when 1,2,3 then "CUST_INV_SERVICE_INCOME_JOB_GR"
            when 4 then "CUST_INV_SERVICE_INCOME_JOB_BP"
            when 5,6 then "CUST_INV_SERVICE_INCOME_JOB_FOC"
            else
                raise "Invalid work_type: #{work_type}"
            end
        end
        @erp.get_account code,@parent_company_id,:required=>true
    end

    def get_account_other(row)
        work_type=row["WORK_TYPE"]
        sublet_flg=row["SUBLET_FLG"]
        if work_type==5
            code="CUST_INV_SERVICE_INCOME_OTHER_ACC"
        elsif work_type==4
            code="CUST_INV_SERVICE_INCOME_OTHER_BP"
            if sublet_flg==1
                code="CUST_INV_SERVICE_INCOME_OTHER_SUBLET"
            end
        else
            if sublet_flg==1
                code="CUST_INV_SERVICE_INCOME_OTHER_SUBLET"
            else
                code="CUST_INV_SERVICE_INCOME_OTHER_GR"
            end
        end
        @erp.get_account code,@parent_company_id,:required=>true
    end

    def get_account_part(row)
        warr_flg=row["WARR_FLG"]
        if warr_flg==1
            code="CUST_INV_SERVICE_INCOME_PART_WAR"
        else
            work_type=row["WORK_TYPE"]
            part_type=row["PART_TYPE"]
            code=case part_type
            when "1","2","3","4"
                case work_type
                when 1,2,3 then "CUST_INV_SERVICE_INCOME_PART_GEN_GR"
                when 4 then "CUST_INV_SERVICE_INCOME_PART_GEN_BP"
                when 5,6 then "CUST_INV_SERVICE_INCOME_PART_GEN_FOC"
                else
                    raise "Invalid work_type: #{work_type}"
                end
            when "5"
                case work_type
                when 1,2,3 then "CUST_INV_SERVICE_INCOME_PART_OIL_GR"
                when 4 then "CUST_INV_SERVICE_INCOME_PART_OIL_BP"
                when 5,6 then "CUST_INV_SERVICE_INCOME_PART_OIL_FOC"
                else
                    raise "Invalid work_type: #{work_type}"
                end
            when "6"
                case work_type
                when 1,2,3 then "CUST_INV_SERVICE_INCOME_PART_ACC_GR"
                when 4 then "CUST_INV_SERVICE_INCOME_PART_ACC_BP"
                when 5,6 then "CUST_INV_SERVICE_INCOME_PART_ACC_FOC"
                else
                    raise "Invalid work_type: #{work_type}"
                end
            else
                raise "Invalid part type: #{part_type}"
            end
        end
        @erp.get_account code,@parent_company_id,:required=>true
    end

    def get_analytic_job(work_type)
        work_type_group=case work_type
        when 1,2,3 then "GR"
        when 4 then "BP"
        when 5,6 then "FOC"
        else
            raise "Invalid work_type: #{work_type}"
        end
        code="SERV_LABOR_"+work_type_group
        @erp.get_analytic_account code,@company_id,:required=>true
    end

    def get_analytic_other(work_type)
        work_type_group=case work_type
        when 1,2,3 then "GR"
        when 4 then "BP"
        when 5,6 then "FOC"
        else
            raise "Invalid work_type: #{work_type}"
        end
        code="SERV_OTHER_"+work_type_group
        @erp.get_analytic_account code,@company_id,:required=>true
    end

    def get_analytic_part(work_type,part_type)
        work_type_group=case work_type
        when 1,2,3 then "GR"
        when 4 then "BP"
        when 5,6 then "FOC"
        else
            raise "Invalid work_type: #{work_type}"
        end
        part_type_group=case part_type
        when "1","2","3","4" then "GEN"
        when "5" then "OIL"
        when "6" then "ACC"
        else
            raise "Invalid part_type: #{part_type}"
        end
        if work_type_group=="FOC"
            code="SERV_PART_"+work_type_group
        else
            code="SERV_PART_"+work_type_group+"_"+part_type_group
        end
        @erp.get_analytic_account code,@company_id,:required=>true
    end

    def create_account_move(row)
        pm_no=row["INVOICE_NO"].strip
        internal_number=row["RO_NO"].strip()
        inv_no=row["TAX_INVOICE_NO"].strip
        if inv_no.nil? or inv_no.empty?
            @logger.info "SKIPPED : No RI# => don't create account move"
            return
        end

        pmt_method = @dms.get_payment_method(pm_no,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id})

        ref=inv_no+" "+pm_no

        doc_date=row["TAX_INVOICE_DATE"]
        date_invoice=row["INVOICE_DATE"]

        if ( pmt_method[0]=='credit')
            @logger.info "CHECKING: Credit sale => create account move NO-#{pm_no} DATE-#{doc_date}"
        else
            #if it is cash sale but import to credit sale it should create also
            origin = pm_no+" "+internal_number
            invoice_id =@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
            if invoice_id.empty?
                @logger.info "SKIPPED : No invoice created ( this shoule not happend )"
                return
            end

            res=@erp.execute "account.invoice","read",invoice_id,["type"]
            if res[0]["type"]=="out_cash"
                @logger.info "SKIPPED : Cash sale => don't create account move"
                return
            end

        end
        res=@erp.execute "account.move","search",[["ref","=",ref],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "SKIPPED : Account move already created #{inv_no}"
            return
        end

        #date=row["CONFIRM_DATE"].strftime "%Y-%m-%d"

        journal_id=@erp.get_journal("CUST_TAX_INV_SERVICE_JOURNAL",@company_id,:required=>true)
        period_id=@erp.get_period(doc_date,@company_id,:required=>true)

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        tax_amt=row["TAX_AMT"]
        base_amt=row["G_TOTAL_AMT"]
        move_vals={
            "ref"=>ref,
            "company_id"=>@company_id,
            "journal_id"=>journal_id,
            "date"=>doc_date,
            "period_id"=>period_id,
        }

        line1_vals={
            "name"=>pm_no,
            "ref"=>pm_no,
            "journal_id"=>journal_id,
            "period_id"=>period_id,
            "account_id"=>@erp.get_account("TAX_OUT_VAT7_SUS_EXCL",@parent_company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "debit"=>tax_amt,
            "date"=>doc_date,
            "date_create"=>date_invoice,# RM DATE
        }
        line2_vals={
            "name"=>inv_no,
            "ref"=>inv_no,
            "journal_id"=>journal_id,
            "period_id"=>period_id,
            "account_id"=>@erp.get_account("TAX_OUT_VAT7_EXCL",@parent_company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "credit"=>tax_amt,
            "date"=>doc_date,
        }
        move_vals["line_id"]=[[0,0,line1_vals],[0,0,line2_vals]]
        res1=@erp.execute "res.partner","read",[partner_id],["name"]
        partner_name=res1[0]["name"]

        #revert suspended tax amount minus
        tax1_vals={
            "tax_id"=>@erp.get_tax("OUT_VAT7_SUS_EXCL",:required=>true),
            "base_amount"=>-base_amt,
            "tax_amount"=>-tax_amt,
            "date"=>date_invoice,# RM DATE
            "partner_id"=>partner_id,
            "partner_name"=> partner_name,
            "account_id"=>@erp.get_account("TAX_OUT_VAT7_SUS_EXCL",@parent_company_id,:required=>true),
            "ref"=>pm_no,
        }
        #substitue by TAX
        tax2_vals={
            "tax_id"=>@erp.get_tax("OUT_VAT7_EXCL",:required=>true),
            "base_amount"=>base_amt,
            "tax_amount"=>tax_amt,
            "date"=>doc_date,
            "partner_id"=>partner_id,
            "partner_name"=> partner_name,
            "account_id"=>@erp.get_account("TAX_OUT_VAT7_EXCL",@parent_company_id,:required=>true),
            "ref"=>inv_no,
        }
        move_vals["vat_lines"]=[[0,0,tax1_vals],[0,0,tax2_vals]]

        move_id=@erp.execute "account.move","create",move_vals
        @erp.execute "account.move","post",[move_id]
        @logger.info "CREATED : account move #{ref}=>#{move_id}"

        #TODO: reconcile tax account
    end
end
