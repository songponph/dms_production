require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoiceCarDealer < Importer

    def import_by_no(doc_no)
        q="SELECT * FROM ST_RORDERINV WHERE INVOICE_NO='#{doc_no}' and INVOICE_NO LIKE 'SZ%' "
        res=@dms.get q
        raise "Customer invoice for car to other dealer not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM ST_RORDERINV WHERE INV_PRINT_DATE>='#{date_from} 00:00:00' AND INV_PRINT_DATE<='#{date_to} 23:59:59' AND INVOICE_NO LIKE 'SZ%' AND SUPPLIER_KBN !=12 ORDER BY INVOICE_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM ST_RORDERINV WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'SZ%' AND SUPPLIER_KBN !=12"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM ST_RORDERINV WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'SZ%' AND SUPPLIER_KBN !=12"
        #rows=@dms.query q
        #self.import_rows rows,progress
        return true
    end

    def check_import(date_from,date_to)
        q="SELECT INVOICE_NO FROM ST_RORDERINV WHERE INVOICE_NO LIKE 'SZ%' AND SUPPLIER_KBN !=12 AND INV_PRINT_DATE>='#{date_from} 00:00:00' AND INV_PRINT_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_invoice",date_from,date_to,"SZ%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        inv_no=row["INVOICE_NO"].strip
        origin=inv_no+" "+row["RCVORDER_NO"].strip
        if origin.empty?
            @logger.info "No invoice no found........"
            return
        end

        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "Invoice already created:#{origin}"
            return
        end

        partner_no=row["CLAIM_CUST_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        address_id=@erp.get_partner_address(partner_id,:type=>"invoice",:required=>true)
        if not address_id
            address_id=@erp.get_partner_address partner_id,:type=>"default",:required=>true
        end

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        res=@erp.execute "product.product","read",[product_id],["name","variants"]
        prod_name=res[0]["name"]
        prod_variants=res[0]["variants"] or ""

        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,@branch_code,:required=>true

        tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true

        date_invoice=row["INV_PRINT_DATE"].strftime("%m/%d/%y")
        analytic_id=@erp.get_analytic_account "SALE_DEALER",@company_id,:required=>true

        vals={
            "origin" => origin,
            "reference" => inv_no,
            "journal_id" => @erp.get_journal("CUST_INVOICE_CAR_DEALER_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_invoice" ,
            "account_id" => @erp.get_account("CUST_INVOICE_CAR_DEALER_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date" => date_invoice,
            "user_id" => user_id,
            "vehicle_id" => vehicle_id,
            "address_invoice_id" => address_id
        }

        price_unit=row["AFTER_DISC_CR"]
        check_amount=price_unit + row["VAT_CR"]
        lines={
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_INVOICE_CAR_DEALER_INCOME",@parent_company_id,:required=>true),
            "name" => prod_name+" "+prod_variants,
            "product_id" => product_id,
            "quantity" => 1,
            "price_unit" => price_unit,
            "invoice_line_tax_id" => [[6,0,[tax_id]]],
            "account_analytic_id" => analytic_id,
            }

        vals["invoice_line"]=[[0,0,lines]]
        inv_id=@erp.execute "account.invoice","create",vals
        @logger.info "invoice created: #{origin}=>#{inv_id}"
        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total","amount_tax"]
        if row["VAT_CR"] !=res[0]["amount_tax"]
            @erp.execute "account.invoice","set_manual_vat",[inv_id],false,row["AFTER_DISC_CR"],row["VAT_CR"],@company_id
        end


        amt=res[0]["amount_total"]
        if (amt-check_amount).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{check_amount}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end

