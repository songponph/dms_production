require "dms"
require "erp"
require "pp"
require "importer"

class Importer_Booking < Importer
    def import_by_no(doc_no)
        puts "Importer_Booking.import_by_no",doc_no
        q="SELECT * FROM ST_BOOKING WHERE BOOKING_NO='#{doc_no}'"
        res=@dms.get q
        raise "Booking not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        puts "Importer_Booking.import_by_date",date_from,date_to
        q="SELECT * FROM ST_BOOKING WHERE BOOKING_DATE>='#{date_from}' AND BOOKING_DATE<='#{date_to}'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_Booking.import_daily"
        @no_update=false

        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM ST_BOOKING WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        @no_update=true

        t0=from_t.strftime "%Y-%m-%d %H:%M:00"

        q="SELECT * FROM ST_BOOKING WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_booking",date_from,date_to
        q="SELECT BOOKING_NO FROM ST_BOOKING WHERE BOOKING_DATE>='#{date_from} 00:00:00' AND BOOKING_DATE<='#{date_to} 23:59:59' ORDER BY BOOKING_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["BOOKING_NO"].strip}
        erp_docnos=@erp.get_booking_docnos date_from,date_to,"SB%"
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        puts "Importer_Booking.import_row : no_update #{@no_update}"

        #pp "row:",row
        booking_no=row["BOOKING_NO"].strip.upcase

        booking_id=@erp.get_booking booking_no

        #TODO: update daily import, not in realtime
        if booking_id and @no_update
            @logger.info "booking already created: #{booking_no}"
            return
        end

        model_no=row["MTOC_CD"]
        partner_no=row["CUSTOMER_NO"].strip.upcase
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,@branch_code,:required=>true
        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        doc_si=@dms.get_doc_SI booking_no

        puts "Booking status:"
        if row["CANCEL_DATE"]
            state="canceled"
        elsif doc_si
            state="done"
        else
            state="open"
        end

        date=row["BOOKING_DATE"]
        booking_date=date.strftime("%m/%d/%y")
        name=row["BOOKING_NO"].strip
        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,:required=>true

        vals_={
            "date" => booking_date,
            "customer_id" => partner_id,
            "name" => name,
            "product_id" => product_id,
            "state" => state,
            "company_id" => @company_id,
            "user_id" => user_id,
        }
        if vehicle_id
            vals_["vehicle_id"] = vehicle_id
        end
        #TODO: update this
        if not row["TRAN_BOOKING_NO"].strip == ""
            tran_booking_no=row["TRAN_BOOKING_NO"].strip.upcase
            tran_booking_id=@erp.get_booking tran_booking_no
            if not tran_booking_id.nil?
                vals_["parent_id"]=tran_booking_id
            else
                puts "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
                imp=DMS_OpenERP.get_importer "booking"
                imp.start_import "branch_code"=>@branch_code,"no_update"=>"-c"
                vals_["parent_id"]=imp.import_by_no tran_booking_no
            end
        end

        if booking_id
            puts "Updating booking...."
            @erp.execute "ac.booking","write",booking_id,vals_
            @logger.info "UPDATED : booking : #{booking_no}"
        else
            puts "Creating new booking....."
            booking_id=@erp.execute "ac.booking","create",vals_
            @logger.info "CREATED : booking: #{booking_no}"
        end
        return booking_id
    end
end
 
