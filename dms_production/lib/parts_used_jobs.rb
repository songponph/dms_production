require "dms"
require "erp"
require "importer"
require "pp"

class Importer_PartsUsedJobs < Importer
    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND p.PICKING_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND p.PICKING_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( p.RO_NO='#{number}' or p.DELIVERY_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND p.LAST_UPDATE_DATE>='#{last_update}' ": "")

        query="
        select rtrim(p.DELIVERY_NO)+' '+p.ro_no as ORIGIN
        ,p.PICKING_DATE as \"DATE\"
        ,rtrim(upper(p.CUSTOMER_NO)) as PARTNER_NO
        ,rtrim(upper(m.PART_NO)) as PRODUCT_ID
        ,m.PART_NO+' '+convert(varchar,m.LINE_NO) as \"NAME\"
        ,m.STOCK_OUT_QTY as PRODUCT_QTY
        ,m.COST_PRICE as PRICE_UNIT
        from
        PT_PKDMST m
        left join  pt_pkhmst p on (p.DELIVERY_NO=m.DELIVERY_NO)
        where p.ro_no is not null and p.RO_NO!=''
        "+cause+
        "
        order by p.PICKING_DATE,p.DELIVERY_NO,m.LINE_NO
        "
        #print  query
        return query
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        rows=@dms.get q
        raise "Picking not found: #{doc_no}" if not res
        self.import_row(rows)
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to,false)
        rows=@dms.query q
        self.import_row rows
    end

    def import_daily(progress=nil)
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        #q=self.query_get(false,false,false,last_update=t0)
        #rows=@dms.query q
        #self.import_row rows
        return
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q=self.query_get(false,date_from=t0)
        #rows=@dms.query q
        #self.import_row rows,progress
    #end 

    def check_import(date_from,date_to)
        puts "Check_import_PD",date_from,date_to
        q="SELECT DELIVERY_NO,RO_NO FROM PT_PKHMST WHERE RO_NO != '' AND PICKING_DATE>='#{date_from} 00:00:00' AND PICKING_DATE<='#{date_to} 23:59:59' ORDER BY DELIVERY_NO"
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="PD%"
        for row in rows
            invoice=row["DELIVERY_NO"].strip+" "+row["RO_NO"].strip
            dms_docnos.push(invoice)
        end
        puts "################################################"
        erp_docnos=@erp.get_picking_docnos inv_no,"internal",date_from,date_to,@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def format_row(rows)
        summary = {}
        rows.each do |rec|
          key= rec.values_at("ORIGIN","DATE","PARTNER_NO")
          summary[key] ||= []
          summary[key] << rec
        end
        return summary
    end

    def import_row(row)
        result=[]
        datas =self.format_row(row)
        for d in datas.keys.sort
            origin, date, partner_no= d

            res=@erp.execute "stock.picking","search",[["origin","=",origin],["company_id","=",@company_id]]
            if not res.empty?
                @logger.info "SKIPPED : Picking already created: #{origin}"
                next
            end
            partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
            address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)
            if not address_id
                address_id=@erp.get_partner_address(partner_id,:required=>true)
            end

            date=date.strftime "%Y-%m-%d %H:%M:%S"

            pick_vals={
                "origin"=>origin,
                "date"=>date,
                "date_done"=>date,
                "move_type"=>"direct",
                "company_id"=>@company_id,
                "invoice_state"=>"none",
                "type"=>"internal",
                "address_id"=>address_id,
                "stock_journal_id"=>@erp.get_stock_journal("PARTS_USED_JOBS_JOURNAL",@company_id),#PART_WIP
            }

            lines=get_lines(datas[d])
            pick_vals["move_lines"]=lines.collect {|line_vals| [0,0,line_vals]}

            pick_id=@erp.execute "stock.picking","create",pick_vals
            @logger.info "CREATED : Picking #{origin}=>#{pick_id} : #{date}"
            @erp.exec_workflow "stock.picking","button_confirm",pick_id
            @erp.exec_workflow "stock.picking","button_done",pick_id
            result<<pick_id
        end
        return result
    end

    def get_lines(rows)
        lines=[]
        for row in rows
            part_no=row["PRODUCT_ID"]

            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            date=row["DATE"].strftime "%Y-%m-%d %H:%M:%S"
            vals={
                "name"=>row["NAME"],
                "date"=>date,
                "date_expected"=>date,
                "product_id"=>product_id,
                "product_uom"=>@erp.get_uom("PCE",:required=>true),
                "product_qty"=>row["PRODUCT_QTY"],
                "location_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
                "location_dest_id"=>@erp.get_location(@branch_code+"-Production",:required=>true),
                "state"=>"assigned",
                "price_unit"=>row["PRICE_UNIT"],
            }
            lines<<vals
        end
        lines
    end
end
