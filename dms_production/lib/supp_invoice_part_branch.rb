require "dms"
require "erp"
require "pp"
require "importer"

class Importer_SuppInvoicePartBranch < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM PT_FLHMST WHERE FLOAT_NO='#{doc_no}' OR INVOICE_NO='#{doc_no}'"
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM PT_FLHMST WHERE SHIPPING_DATE>='#{date_from} 00:00:00' AND SHIPPING_DATE<='#{date_to} 23:59:59' AND INVOICE_NO LIKE 'PZ%' ORDER BY SHIPPING_DATE,FLOAT_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PT_FLHMST WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'PZ%'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q="SELECT INVOICE_NO FROM PT_FLHMST WHERE INVOICE_NO LIKE 'PZ%' AND LIST_DATE>='#{date_from} 00:00:00' AND LIST_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "in_invoice",date_from,date_to,"PF%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM PT_FLHMST WHERE INVOICE_NO LIKE 'PZ%' AND LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def get_payment_method_pz(inv_id,erp_con,options={})

        company_id = options["company_id"]
        parent_company_id = options["parent_company_id"]

        method="cash" # default type for payment method

        vals={}
        vals["method"]="cash"
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total",'date_invoice','reference']
        if not res
            return
        end
        cash_type = 'out'

        vals["value"]={
            "account_id"=>erp_con.get_account("PZ_PMT_CASH",parent_company_id,:required=>true),
            "name" => res[0]['reference'],
            "amount" => res[0]['amount_total'],
            "date" => res[0]['date_invoice'],
            "type" => cash_type,
            "company_id" => company_id,
        }
        res << vals  # append values
        puts "invoice value#{res}"
        return [method,res]
        end

    def import_row(row)
        inv_no=row["INVOICE_NO"].strip
        float_no=row["FLOAT_NO"].strip
        origin=float_no+" "+inv_no

        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end

        partner_no=row["SUPPLIER_CODE"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address partner_id,:required=>true

        if not address_id
            address_id=@erp.get_partner_address partner_id,:required=>true
        end

        date=row["LIST_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        doc_date=row["SHIPPING_DATE"].strftime "%Y-%m-%d %H:%M:%S"

        inv_type = "in_cash"

        inv_vals={
            "origin"=>origin,
            "reference"=>inv_no,
            "journal_id"=>@erp.get_journal("SUPP_INV_PART_BRANCH_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>inv_type,
            "account_id" => @erp.get_account("SUPP_INV_PART_BRANCH_PAYABLE",@parent_company_id,:required=>true),
            "date_invoice" => date,
            "doc_date"=>doc_date,
            "address_invoice_id"=> address_id,
            "check_total"=>row["TOTAL_AMOUNT"]+row["TAX_AMOUNT"],
        }

        lines=get_lines(float_no)
        raise "No lines in invoice" if lines.empty?
        q=0
        lines.each do |line|
           q += line["quantity"]
        end
        if q==0
            @logger.info "Qty is still zero,DMS transaction is not completed yet! => skipped"
            return
        end

        inv_vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}

        pp inv_vals
        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "invoice created: #{inv_no}=>#{inv_id}"

        pmt_method = self.get_payment_method_pz(inv_id,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id,"invoice_type"=>inv_type})

        puts pmt_method

        # Add payment method
        self.set_payment_method(inv_id,pmt_method)

        @erp.execute "account.invoice","button_compute",[inv_id],{},true
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        amt2=row["TOTAL_AMOUNT"]+row["TAX_AMOUNT"]
        if (amt-amt2).abs>0.25
            msg="wrong total in #{inv_no}: #{amt}/#{amt2}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
    end

    def get_lines(float_no)
        q="
SELECT * FROM PT_FLDMST WHERE FLOAT_NO='#{float_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            part_no=row["PART_NO"].strip
            line_no=row["LINE_NO"].to_s
            qty=row["INCOMING_QTY"]
            price=row["INCOMING_COST"]
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            account_id=@erp.get_account "SUPP_INV_PART_BRANCH_EXPENSE",@parent_company_id,:required=>true
            uom_id=@erp.get_uom "PCE",:required=>true
            vals={
                "name"=>float_no+" "+line_no,
                "price_unit"=>price,
                "product_id"=>product_id,
                "quantity"=>qty,
                "account_id"=>account_id,
                "uos_id"=>uom_id,
            }
            lines<<vals
        end
        lines
    end
end
