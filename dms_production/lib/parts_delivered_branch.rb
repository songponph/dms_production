require "dms"
require "erp"
require "importer"
require "pp"

class Importer_PartsDeliveredBranch < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM PT_PKHMST WHERE INVOICE_NO='#{doc_no}'"
        res=@dms.get q
        raise "Job order not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM PT_PKHMST WHERE PICKING_DATE >='#{date_from} 00:00:00' AND PICKING_DATE <='#{date_to} 23:59:59' AND INVOICE_NO LIKE 'PZ%'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PT_PKHMST WHERE INVOICE_NO LIKE 'PZ%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_PD",date_from,date_to
        q="SELECT DELIVERY_NO,INVOICE_NO FROM PT_PKHMST WHERE INVOICE_NO LIKE 'PZ%' AND PICKING_DATE>='#{date_from} 00:00:00' AND PICKING_DATE<='#{date_to} 23:59:59' ORDER BY DELIVERY_NO"
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="PZ%"
        for row in rows
            invoice=row["INVOICE_NO"].strip+" "+row["DELIVERY_NO"].strip
            dms_docnos.push(invoice)
        end
        pp dms_docnos
        puts "################################################"
        erp_docnos=@erp.get_picking_docnos inv_no,"internal",date_from,date_to,@company_id
        pp erp_docnos
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        inv_no=(row["INVOICE_NO"] or "").strip
        dlv_no=row["DELIVERY_NO"].strip
        origin=inv_no+" "+dlv_no

        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]

        if not res.empty?
            @logger.info "UPDATE : Picking already created: #{origin}"
            @erp.update_picking(res,inv_no,@company_id)
            return
        end
        date=row["PICKING_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        invoice=@dms.get_parts_customer inv_no,:required=>true
        inv_date=invoice["INVOICE_DATE"].strftime "%Y-%m-%d %H:%M:%S"

        partner_no=invoice["CUSTOMER_NO"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address partner_id,:type=>"invoice",:required=>true
        invoice_id = @erp.get_invoice_id(inv_no,@company_id)

        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done"=>inv_date,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>"internal",
            "partner_id"=>partner_id,
            "address_id" => address_id,
            "stock_journal_id"=>@erp.get_stock_journal("PARTS_DELIVERED_BRANCH_JOURNAL",@company_id),#PART_PZ_OUT
            "invoice_id"=>invoice_id,
        }

        lines=get_lines(dlv_no,row)
        pick_vals["move_lines"]=lines.collect {|line_vals| [0,0,line_vals]}

        pp "vals",pick_vals
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created picking #{dlv_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end

    def get_lines(dlv_no,job_row)
        q="
SELECT *
 FROM PT_PKDMST 
 WHERE DELIVERY_NO='#{dlv_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            pp "line_row",row
            part_no=row["PART_NO"].strip
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true

            date=row["COMPLETED_DATE"].strftime "%Y-%m-%d %H:%M:%S"
            line_no=row["LINE_NO"].to_s
            qty=row["DELIVERY_QTY"]
            vals={
                "name"=>dlv_no+" "+line_no,
                "date"=>date,
                "date_expected"=>date,
                "product_id"=>product_id,
                "product_uom"=>@erp.get_uom("PCE",:required=>true),
                "product_qty"=>qty,
                "location_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
                "location_dest_id"=>@erp.get_location(@branch_code+"-Transfer",:required=>true),
                "state"=>"assigned",
                "price_unit"=>row["COST_PRICE"],
            }
            lines<<vals
        end
        lines
    end
end
