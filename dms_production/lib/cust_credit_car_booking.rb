require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CreditCarBooking < Importer

    def import_by_no(creditnote_no)
        q="SELECT * FROM ST_BOOKINGVAT WHERE CREDITNOTE_NO='#{creditnote_no}'"
        res=@dms.get q
        raise "Invoice not found: #{creditnote_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        q="SELECT * FROM ST_BOOKINGVAT WHERE CREDITNOTE_DATE>='#{date_from} 00:00:00' AND CREDITNOTE_DATE<='#{date_to} 23:59:59' AND CREDITNOTE_NO LIKE 'S8%' ORDER BY CREDITNOTE_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM ST_BOOKINGVAT WHERE CREDITNOTE_NO LIKE 'S8%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q="SELECT CREDITNOTE_NO FROM ST_BOOKINGVAT WHERE CREDITNOTE_NO LIKE 'S8%' AND CREDITNOTE_DATE>='#{date_from} 00:00:00' AND CREDITNOTE_DATE<='#{date_to} 23:59:59' ORDER BY CREDITNOTE_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["CREDITNOTE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_refund",date_from,date_to,"S8%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:00"
        q="SELECT * FROM ST_BOOKINGVAT WHERE CREDITNOTE_NO LIKE 'S8%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        origin=row["CREDITNOTE_NO"].strip + " " + row["BOOKING_NO"].strip+ " " + row["BOOKINGVAT_NO"].strip

        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created:#{origin}"
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.create_address_credit_booking row

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        res=@erp.execute "product.product","read",[product_id],["name","variants"]
        name=res[0]["name"]
        variant=(res[0]["variants"] or "")
        prod_name=name+" "+variant

        booking_no=row["BOOKING_NO"].strip
        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true

        tax_id=@erp.get_tax "OUT_VAT7_INCL",:required=>true

        date_invoice=row["CREDITNOTE_DATE"].strftime("%m/%d/%y")
        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,:required=>true

        original_invoice=row["BOOKINGVAT_NO"].strip + " " + row["BOOKING_NO"].strip
        original_invoice_id=@erp.get_origin_invoice original_invoice,@company_id,@branch_code,"out_invoice","cust_invoice_car_booking",booking_no,:required=>true

        invoice_name=row["CREDITNOTE_NO"].strip
        vals_={
            "origin" => origin,
            "reference" => invoice_name,
            "comment"=>row["CREDITNOTE_REASON"],
            "journal_id" => @erp.get_journal("CUST_CREDIT_CAR_BOOKING_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_refund",
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_BOOKING_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" =>date_invoice,
            "doc_date" =>date_invoice,
            "booking_id" => booking_id,
            "user_id" => user_id,
            "orig_invoice_id" => original_invoice_id,
            "address_invoice_id" => address_id,
        }

        lines_= {
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_BOOKING_INCOME",@parent_company_id,:required=>true),
            "name" => prod_name,
            "quantity" => 1,
            "price_unit" => row["TOTAL_DEPOSIT_CR"],
            "invoice_line_tax_id" => [[6,0,[tax_id]]],
            "product_id" => product_id
        }

        vals_["invoice_line"]=[[0,0,lines_]]

        inv_id=@erp.execute "account.invoice","create",vals_
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["TOTAL_DEPOSIT_CR"]).abs>0.25
            msg="wrong total in #{bookingvat_no}: #{amt}/#{row["TOTAL_DEPOSIT_CR"]}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
