require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CustCreditCarFull < Importer

    def import_by_no(creditnote_no)
        puts "Importer_CustCreditCarFull.import_by_no",creditnote_no
        q="SELECT * FROM ST_INVOICE WHERE INVOICE_CN_NO='#{creditnote_no}'"
        res=@dms.get q
        raise "Invoice not found: #{creditnote_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        puts "Importer_CustCreditCarFull.import_by_date",date_from,date_to
        q="SELECT * FROM ST_INVOICE WHERE CANCEL_DATE>='#{date_from} 00:00:00' AND CANCEL_DATE<='#{date_to} 23:59:59' AND INVOICE_CN_NO LIKE'S1%' ORDER BY INVOICE_CN_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        t0=from_t.strftime "%Y-%m-%d %H:00:00"
        q="SELECT * FROM ST_INVOICE WHERE CANCEL_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_daily(progress=nil)
        puts "Importer_CustCreditCarFull.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_INVOICE WHERE CANCEL_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        puts "Importer_CustCreditCarFull_Invoice.import_row"
        pp "row:",row
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def check_import(date_from,date_to)
        puts "Check_import_S1",date_from,date_to
        q="SELECT INVOICE_CN_NO FROM ST_INVOICE WHERE INVOICE_CN_NO LIKE 'S1%' AND CANCEL_DATE>='#{date_from} 00:00:00' AND CANCEL_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_CN_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_CN_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_refund",date_from,date_to,"S1%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def create_invoice(row)
        puts "Customer credit note for car full payment....."
        origin=row["INVOICE_CN_NO"].strip + " " + row['INVOICE_NO'].strip + " " + row['BOOKING_NO'].strip

        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created:#{origin}"
            return
        end

        if row["BALANCE_CR"] == 0
            @logger.info "Not created invoice for balance_cr is zero:"
            return
        end

        inv_no=row["INVOICE_NO"].strip
        bill_no=row["BILL_NO"].strip
        partner_no=row["CUSTOMER_NO"].strip.upcase

        full_invoice=@dms.get_cust_invoice_car_full inv_no 
        customer=full_invoice["CUST_CD"].strip
        partner_id=@erp.get_partner customer ,@company_id,@branch_code,:required=>true

        address_row=@dms.get_address_row customer,inv_no
        address_id=@erp.create_address_invoice_car_full address_row

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        booking_no=row["BOOKING_NO"].strip
        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true

        staff=row["STAFF_CD"].strip
        user_id=@erp.get_user staff,:require=>true

        date_invoice=row["CANCEL_DATE"].strftime("%m/%d/%y")

        original_invoice=row['INVOICE_NO'].strip + " " + row['BOOKING_NO'].strip
        original_invoice_id=@erp.get_origin_invoice original_invoice,@company_id,@branch_code,nil,"cust_invoice_car_full",inv_no,:required=>true

        tax_id=@erp.get_tax "OUT_VAT7_INCL",:required=>true

        if not row["FRAME_NO"].empty?
            frame_no=row["FRAME_NO"]
            vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
            car_model=@dms.get_car_model frame_no,:required=>true
            mto_nm=car_model["MTO_NM"]
        end

        invoice_name=row["INVOICE_CN_NO"].strip
        cancel_cd=row["CANCEL_CD"]
        credit_reason=@dms.get_credit_note_reason cancel_cd,:required=>true
        inv_val={
            "origin" => origin,
            "reference" => invoice_name,
            "comment" => credit_reason,
            "journal_id" => @erp.get_journal("CUST_CREDIT_CAR_FULL_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_refund",
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_FULL_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" =>date_invoice,
            "doc_date" =>date_invoice,
            "booking_id" => booking_id,
            "orig_invoice_id" => original_invoice_id,
            "user_id" => user_id,
            "address_invoice_id" => address_id,
            "vehicle_id" => vehicle_id,
        }

        lines=[]

        down_pay=row["DOWN_PAY_CR"]

        res=@dms.get_car_invoice_from_booking booking_no,:required=>true
        partner_no=res["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        res=@erp.execute "res.partner","read",[partner_id],["name"]
        partner_name=res[0]["name"]

        if down_pay > 0.0
            line_1={
                "account_id" => @erp.get_account("CUST_CREDIT_CAR_FULL_DEDUCT",@parent_company_id,:required=>true),
                "name" => partner_name,
                "quantity" => 1,
                "price_unit" => -(down_pay),
                "product_id" => product_id,
            }
            lines<<line_1
        end
        car_analytic=@dms.get_car_product model_no
        inport_flg=car_analytic["INPORT_FLG"]

        analytic_account_id = @erp.get_analytic_from_code "SALE_", mto_nm,@company_id,inport_flg

        res=@erp.execute "product.product","read",[product_id],["name","variants"]

        lname= res[0]["name"] +" "+ res[0]["variants"]

        total_cr=row["TOTAL_CR"]
        line_2= {
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_FULL_INCOME",@parent_company_id,:required=>true),
            "name" => lname,
            "quantity" => 1,
            "price_unit" => total_cr,
            "invoice_line_tax_id" => [[6,0,[tax_id]]],
            "product_id" => product_id,
            "account_analytice_id" => analytic_account_id,
        }

        lines<<line_2
        raise "no lines in invoice" if lines.empty?
        inv_val["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}

        pp "vals:",inv_val
        pp "line1:",line_1
        pp "line2:",line_2

        if down_pay > 0.0
            check_amt = total_cr - down_pay
        else
            check_amt = total_cr
        end

        inv_id=@erp.execute "account.invoice","create",inv_val
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-check_amt).abs>0.25
            msg="wrong total in #{invoice_name}: #{amt}/#{check_amt}"
            @logger.error msg
            raise msg
        end
        puts "Posting invoice............."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        #@erp.execute "ac.booking","write",[booking_id],{"vehicle_id"=>vehicle_id}
        return inv_id
    end
end
