require "dms"
require "erp"
require "importer"

class Importer_CustInvoiceCarDown < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM ST_INVOICE WHERE BILL_NO='#{doc_no}' OR BOOKING_NO='#{doc_no}'"
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM ST_INVOICE WHERE BILL_PRINT_DATE>='#{date_from} 00:00:00' AND BILL_PRINT_DATE<='#{date_to} 23:59:59' ORDER BY BILL_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:00:00"
        q="SELECT * FROM ST_INVOICE WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        t0=from_t.strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM ST_INVOICE WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q="SELECT BILL_NO FROM ST_INVOICE WHERE BILL_PRINT_DATE>='#{date_from} 00:00:00' AND BILL_PRINT_DATE<='#{date_to} 23:59:59' ORDER BY BILL_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["BILL_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_invoice",date_from,date_to,"S3%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end 

    def import_row(row)
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        t0=Time.now()
        bill_no=row["BILL_NO"].strip
        if bill_no.empty?
            @logger.info "No document S3 no. to import."
            return
        end

        origin=row["BILL_NO"].strip+ " " +row["BOOKING_NO"].strip+ " "+row["INVOICE_NO"].strip

        res=@erp.execute "account.invoice","search",[["origin","=","#{origin}"],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end


        if row["DOWN_PAY_CR"]==0
            @logger.info "Downpaycr is zero,no need to import..."
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address(partner_id,:type=>"invoice",:required=>true)
        if address_id.nil?
            address_id=@erp.get_partner_address(partner_id,:type=>"default",:required=>true)
        end

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        res=@erp.execute "product.product","read",[product_id],["name","variants"]
        name=res[0]["name"]
        variant=(res[0]["variants"] or "")
        prod_name=name+" "+variant

        booking_no=row["BOOKING_NO"].strip
        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true

        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,:required=>true

        origin=row["BILL_NO"].strip+ " " +row["BOOKING_NO"].strip+ " "+row["INVOICE_NO"].strip

        datetime=row["BILL_PRINT_DATE"]
        date_invoice=datetime.strftime("%m/%d/%y")

        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        vals_={
            "origin" => origin,
            "reference" => row["BILL_NO"].strip,
            "journal_id" => @erp.get_journal("CUST_INV_CAR_DOWN_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_invoice",
            "account_id" => @erp.get_account("CUST_INV_CAR_DOWN_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date" => date_invoice,
            "booking_id" => booking_id,
            "user_id" => user_id ,
            "vehicle_id" => vehicle_id,
            "address_invoice_id" => address_id
        }


        lines_={
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_INV_CAR_DOWN_INCOME",@parent_company_id,:required=>true),
            "name" => prod_name,
            "quantity" => 1,
            "product_id" => product_id,
            "price_unit" => row["DOWN_PAY_CR"],
        }

        vals_["invoice_line"]=[[0,0,lines_]]
        #cash_vals_={
                #"account_id"=>@erp.get_account("CUST_PMT_CASH",@parent_company_id,:required=>true),
                #"name" => bill_no,
                #"amount" => row["DOWN_PAY_CR"],
                #"type" => "in",
            #}
        #vals_["cash_moves"]=[[0,0,cash_vals_]]

        t1=Time.now()

        inv_id=@erp.execute "account.invoice","create",vals_
        t2=Time.now()
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        @erp.execute "account.invoice","button_compute",[inv_id]

        t2_2=Time.now()
        t3=Time.now()

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["DOWN_PAY_CR"]).abs>0.25
            msg="wrong total in #{bill_no}: #{amt}/#{row["DOWN_PAY_CR"]}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        t4=Time.now()
        @logger.info "times:total=#{t4-t0} prepare=#{t1-t0} create=#{t2-t1} compute_tax=#{t2_2-t2} invoice_posting=#{t3-t2_2} checking=#{t4-t3}"
        #@erp.execute "ac.booking","write",[booking_id],{"vehicle_id"=>vehicle_id}
        return inv_id
    end
end 
