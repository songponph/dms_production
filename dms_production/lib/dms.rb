
require "../jar/sqljdbc4.jar"
require "logger"

def resultset_to_hash(resultset)
  meta = resultset.meta_data
  rows = []
  while resultset.next
    row = {}
    (1..meta.column_count).each do |i|
      name = meta.column_name i
      row[name]  =  case meta.column_type(i)
                    when -6, -5, 5, 4
                      # TINYINT, BIGINT, INTEGER
                      resultset.get_int(i).to_i
                    when 41
                      # Date
                      resultset.get_date(i)
                    when 92
                      # Time
                      resultset.get_time(i).to_i
                    when 93
                      # Timestamp
                      #resultset.get_timestamp(i)
                      val=resultset.get_timestamp(i)
                      val=Time.at(0)+val.getTime()/1000 if val
                      val
                    when 2, 3, 6
                      # NUMERIC, DECIMAL, FLOAT
                      case meta.scale(i)
                      when 0
                        resultset.get_long(i).to_i
                      else
                        resultset.get_string(i).to_f
                      end
                    when 1, -15, -9, 12
                      # CHAR, NCHAR, NVARCHAR, VARCHAR
                      resultset.get_string(i).to_s
                    else
                      resultset.get_string(i).to_s
                    end
    end
    rows << row
  end
  rows
end

class DMSConnection
    def connect(url)
        puts "DMS.connect #{url}"
        Java::com.microsoft.sqlserver.jdbc.SQLServerDriver
        @url=url
        reconnect
        @cache={}
        @logger=DMS_OpenERP.get_logger
    end

    def reconnect()
        @db_con=java.sql.DriverManager.get_connection(@url)
    end

    def query(q)
        #puts "DMS.query",q
        rs=nil
        for i in [3,5,10,20]
            begin

                st=@db_con.create_statement
                st.setQueryTimeout 60
                rs=st.execute_query q

                break
            rescue
                puts "Query error, trying again: #{q}"
                sleep i         #wait

                # if execute error try to connect again
                reconnect
            end
        end

        if rs.nil?
            raise "Can't execute query: #{q}"
        end

        rows=resultset_to_hash rs
        rs.close()
        rows
    end

    def get(q)
        res=self.query q
        return nil if res.empty?
        res[0]
    end

    def get_cache(key)
        val=@cache[key]
        val
    end

    def set_cache(key,val)
        @cache[key]=val
    end

    def get_payment_method_wht(tran_no,options={})
        res=self.get_cache(["payment_wht",tran_no])
        return res if res

        q="
        SELECT *
        FROM AT_UNSETTLE_SETTL
        WHERE TRAN_NO = '#{tran_no}'
        and SET_METHOD_CD in ('76')
        "
        row=self.get(q)
        if not row
            return nil
        end
        res={}

        res["amount"] = row["SETTL_CR"]
        if res["amount"] == 0.0
            res["amount"] = row["SETTL_PLAN_CR"]
        end

        self.set_cache(["payment_wht",tran_no],res)

        return res
    end

    def get_payment_method(tran_no,erp_con,options={})
        # disable cache for temporary
        #res=self.get_cache(["payment_method",tran_no])
        #return res if res

        company_id = options["company_id"]
        parent_company_id = options["parent_company_id"]

        q="
        SELECT
            SET_METHOD_CD,
            SETTL_PLAN_DATE as DATE,
            SETTL_PLAN_CR as AMOUNT,
            CREDIT_CD as NUMBER,
            FINANCE_CD as BANK
        FROM AT_UNSETTLE_SETTL
        WHERE TRAN_NO = '#{tran_no}'
        and SET_METHOD_CD not in ('76','69')
        "
        rows=self.query(q)

        if not rows
            raise "Payment method not found: #{tran_no}" if options[:required]
            return nil
        end

        #partner_id = options[:partner_id] #

        method="cash" # default type for payment method

        res=[]
        for row in rows
            vals={}

            amount = row["AMOUNT"]
            date = row["DATE"].strftime "%Y-%m-%d"

            case row["SET_METHOD_CD"]
                #10 : cash
                #20 : cheque
                #30 : bank transfer
                #76 : WHT
                #45 : coupon almost use by Paysave
                #46 : QR Code
                when "10","20","30","45","46"
                    vals["method"]="cash"

                    vals["value"]={
                        "account_id"=>erp_con.get_account("CUST_PMT_CASH",parent_company_id,:required=>true),
                        "name" => tran_no,
                        "amount" => amount,
                        "date" => date,
                        "type" => "in",
                        "company_id" => company_id,
                    }

                when "40"

                    vals["method"]="creditcard"

                    bank = row["BANK"].strip
                    if bank =="STC"
                        bank = "TBANK"
                    end

                    bank_id=erp_con.get_bank(bank,:required=>true)

                    vals["value"]={
                        "name" => tran_no,
                        "account_id"=>erp_con.get_account("CUST_PMT_CREDIT_CARD_#{bank}",parent_company_id,:required=>true),
                        "amount" => amount,
                        "cheque_date" => date,
                        "number" => row["NUMBER"].strip,
                        "type" => "in",
                        "subtype" => "credit",
                        "bank_id" => bank_id,
                        "company_id" => company_id,
                        #"partner_id" => partner_id , # TODO:
                    }


                when "45","62","64","63","36","71","67","78","70","74"
                    method = "credit"

                else # TODO: add cheque pmt method
                    raise "Invalid payment method: #{row['SET_METHOD_CD']}"
                end

            res << vals  # append values

            end

            # disable cache for temporary
            #self.set_cache(["payment_method",tran_no],res)
            return [method,res]
        end

    def get_sublet_car_frame_no(ro_no,options={})
        puts "DMS.get_sublet-car_frame for #{ro_no}"
        q="SELECT * FROM RR_REPAIRORDER WHERE RO_NO='#{ro_no}'"
        row=self.get(q)
        if not row
            raise "frame number cannot be found for this ro numver: #{invoice_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_sublet_cost(po_no,options={})
        puts "DMS.get_sublet_cost #{po_no}"
        q="SELECT * FROM RR_SUBLETJOB_DETAIL WHERE PO_NO='#{po_no}'"
        row=self.get(q)
        if not row
            raise "Cost cannot be found for this po_no: #{po_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_cust_invoice_car_full(invoice_no,options={})
        puts "DMS.get_car_full_invoice #{invoice_no}"
        q="
SELECT *
FROM AT_UNSETTLE_MAIN 
WHERE INVOICE_NO='#{invoice_no}'
"
        row=self.get(q)
        if not row
            throw "Parts proforma invoice not found:#{invoice_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_bank_credit(tran_no,options={})
        puts "DMS.get_bank_credit#{tran_no}"
        q="
SELECT *
FROM AT_UNSETTLE_SETTL
WHERE TRAN_NO = '#{tran_no}'
"
        row=self.get(q)
        if not row
            throw "Payment method not found: #{tran_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_part_delivery_nos(inv_no,options={})
        puts "DMS.get_part_delivery_nos: #{inv_no}"
        q="
SELECT DISTINCT pd.DELIVERY_NO
FROM PT_PKDMST pd,PT_PKHMST ph
WHERE pd.DELIVERY_NO=ph.DELIVERY_NO AND ph.INVOICE_NO='#{inv_no}'
"
        rows=self.query(q)
        if rows.empty?
            throw "Order numbers not found for parts: #{inv_no}" if options[:required]
            return []
        end
        order_nos=rows.collect {|r| r["DELIVERY_NO"].strip}
        return order_nos
    end

    def get_part_order_nos(inv_no,options={})
        puts "DMS.get_part_order_nos: #{inv_no}"
        q="
SELECT DISTINCT pd.ORDER_NO
FROM PT_PKDMST pd,PT_PKHMST ph
WHERE pd.DELIVERY_NO=ph.DELIVERY_NO AND ph.INVOICE_NO='#{inv_no}'
"
        rows=self.query(q)
        if rows.empty?
            throw "Order numbers not found for parts: #{inv_no}" if options[:required]
            return []
        end
        order_nos=rows.collect {|r| r["ORDER_NO"].strip}
        return order_nos
    end

    def get_part_order_no(inv_no,options={})
        puts "DMS.get_order_no: #{inv_no}"
        q="
        SELECT top 1 pd.ORDER_NO
        FROM PT_PKDMST pd,PT_PKHMST ph
        WHERE pd.DELIVERY_NO=ph.DELIVERY_NO AND ph.INVOICE_NO='#{inv_no}'
        "
        row=self.get(q)
        if row.empty?
            throw "No order found for this invoice: #{order_no}" if options[:required]
            return []
        end
        order_no=row["ORDER_NO"].strip
        return order_no
    end

    def get_part_deposit_invoice_from_order(order_no,options={})
        puts "DMS.get_part_deposit_from_order"
        q="
SELECT INVOICE_NO,DEPOSIT_AMOUNT
FROM PT_ROHMST
WHERE ORDER_NO='#{order_no}' AND INVOICE_NO LIKE 'PI%'
"
        row=self.get(q)
        if row.nil?
            throw "No deposit for this order:#{order_no}" if options[:required]
            return nil
        end
        return row
    end


    def get_part_amount_from_order(order_no,options={})
        puts "DMS.get_part_amount_from_order: #{order_no}"
        q="
SELECT *
FROM PT_ROHMST
WHERE ORDER_NO='#{order_no}'
"
        rows=self.get(q)
        if rows.empty?
            throw "No Total amount for this order: #{order_no}" if options[:required]
            return []
        end
        return rows
    end

    def get_frame_model(frame_no,options={})
        puts "DMS.get_frame_model #{frame_no}"
        q="SELECT * FROM SM_VEHICLEINFO WHERE FRAME_NO='#{frame_no}'"
        row=self.get(q)
        if not row
            raise "Frame number not found: #{frame_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_credit_note_reason(cancel_cd,options={})
        puts "DMS.get_credit_note_reason #{cancel_cd}"
        q="SELECT * FROM CM_CODE WHERE SYSTEM_CD='S' AND CODE_TYPE=52 AND COMMON_CD='#{cancel_cd}'"
        row=self.get(q)
        if not row
            raise "Credit note reason not found for the cancel code: #{cancel_cd}" if options[:required]
            return nil
        end 
        if not row["CD_NM_LOCAL"].empty?
            credit_reason=row["CD_NM_LOCAL"].strip
        else
            credit_reason=row["CD_NM_ENGLISH"].strip
        end
        return credit_reason
    end

    def get_part_name(part_no,options={})
        #puts "DMS.get_part_name: #{part_no}"
        q="
SELECT *
FROM PM_SPMAST
WHERE PART_NO='#{part_no}'
"
        row=self.get(q)
        if not row
            throw "Part name not found for this part number #{part_no}"
            return nil
        end
        if not row["PART_NAME_L"].empty?
            name = row["PART_NAME_L"].strip
        else
            name = row["PART_NAME_E"].strip
        end
        return name
    end

    def get_part_no_branch(invoice_no,options={})
        #puts "DMS.get part no #{invoice_no}"
        q="SELECT * FROM PT_PKHMST WHERE INVOICE_NO='#{invoice_no}'"
        row=self.get(q)
        if not row
            raise "Part no cannot be found for this invoice in stock picking: #{invoice_no}" if options[:required]
            return nil
        end
        delivery_no=row["DELIVERY_NO"].strip
        q1="SELECT * FROM PT_PKDMST WHERE DELIVERY_NO='#{delivery_no}'"
        row1=self.get(q1)
        if not row1
            raise "Part no cannot be found for this invoice in stock picking: #{invoice_no}" if options[:required]
            return nil
        end
        part_no=row1["PART_NO"].strip
        return part_no
    end

    def get_ult_partner_no(doc_no,options={})
        #puts "DMS.get_customer #{doc_no}"
        q="
SELECT *
FROM RU_ULTIMATE_CARE 
WHERE INVOICE_NO='#{doc_no}'
"
        row=self.get(q)
        if not row
            throw "Customer not found for ultimate care: #{doc_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_parts_customer(invoice_no,options={})
        #puts "DMS.get_customer #{invoice_no}"
        q="
SELECT *
FROM  PT_INVOICE  
WHERE INVOICE_NO='#{invoice_no}'
"
        row=self.get(q)
        if not row
            throw "Customer not found for parts: #{invoice_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_model(model_no,options={})
        #puts "DMS.get_model #{model_no}"
        q="SELECT * FROM SM_VEHICLEINFO WHERE MTOC_CD='#{model_no}'"
        row=self.get(q)
        if not row
            raise "Model number not found: #{model_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_car_invoice(invoice_no,options={})
        #puts "DMS.get_car_invoice_no #{invoice_no}"
        q="
SELECT *
FROM ST_INVOICE
WHERE INVOICE_NO = '#{invoice_no}'
"
        row=self.get(q)
        if not row
            @logger.info "Car invoice not created yet: #{invoice_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_car_delivery_customer(booking_no,options={})
        #puts "DMS.get_car_delivery_booking: #{booking_no}"
        q="
SELECT *
FROM ST_DELIVERY
WHERE BOOKING_NO='#{booking_no}'
"
        row=self.get(q)
        if not row
            throw "Car delivery for this booking not found: #{booking_no}" if options[:requied]
            return nil
        end
        return row
    end

    def get_car_invoice_from_booking(booking_no,options={})
        #puts "DMS.get_car_invoice_no #{booking_no}"
        q="
SELECT *
FROM ST_INVOICE
WHERE BOOKING_NO = '#{booking_no}'
"
        row=self.get(q)
        if not row
            throw "Car invoice not found: #{booking_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_address_row(partner_no,inv_no)
        #puts "Create address"
        q="
SELECT *
FROM SM_SUPPLIER
WHERE supplier_no='#{partner_no}'
"
        row=self.get(q)
        if not row
            q1="SELECT * FROM ST_INVOICE WHERE INVOICE_NO='#{inv_no}'"
            row=self.get(q1)
            if not row
               raise "Address cannot be found for this partner:#{partner_no}" if options[:required]
               return nil
            end
        end
        return row
    end

    def get_proforma_invoice(invoice_no,options={})
        #puts "DMS.get_proforma_invoice #{invoice_no}"
        q="
SELECT *
FROM  PT_INVOICE
WHERE INVOICE_NO = '#{invoice_no}'
"
        row=self.get(q)
        if not row
            throw "Parts proforma invoice not found:#{invoice_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_car_invoice_from_booking(booking_no,options={})
        #puts "DMS.get_invoice_from_booking: #{booking_no}"
        q="
SELECT *
FROM ST_INVOICE 
WHERE BOOKING_NO = '#{booking_no}'
"
        row=self.get(q)
        if not row
            throw "Invoice not found for booking_no:#{booking_no}" if options[:required]
            return nil
        end
        return row
    end

    def get_doc_SI(booking_no,options={})
        #puts "DMS.get_SI_doc #{}"
        q="
SELECT *
FROM ST_INVOICE
WHERE BOOKING_NO = '#{booking_no}'
"
        row=self.get(q)
        if not row
            throw "Doc no SI not found: #{booking_no}" if options[:required]
            return nil
        end
        return row["INVOICE_NO"]
    end

    def get_car_name(mtoc_cd,options={})
        #puts "DMS.get_car_name #{mtoc_cd}"
        q="
SELECT *
FROM SM_MTOC
WHERE MTOC_CD = '#{mtoc_cd}'
"
        row=self.get(q)
        if not row
            throw "Car name not found for this no:#{mtoc_cd}" if options[:required]
            return nil
        end
        return row["MTO_NM"]
    end

    def get_car_product(mtoc_cd,options={})
        #puts "DMS.get_car_name #{mtoc_cd}"
        q="
SELECT *
FROM SM_MTOC
WHERE MTOC_CD = '#{mtoc_cd}'
"
        row=self.get(q)
        if not row
            throw "Car not found for this no:#{mtoc_cd}" if options[:required]
            return nil
        end
        return row
    end

    def get_service_pm(invoice_no ,options={})
        #puts "DMS.get_service_proforma #{invoice_no}"
        q="
SELECT *
FROM RR_INVOICE_H
WHERE INVOCE_NO='#{invoice_no}'
"
        row=self.get(q)
        if not row
            q1="
            SELECT *
            FROM RT_INVOICE_H
            WHERE INVOCE_NO='#{invoice_no}'
            "
            row=self.get(q1)
            if not row
                throw "Proforma invoice for service not found; #{invoice_no}" if options[:required]
                return nil
            end
        end
        return row
    end

    def get_car_model(frame_no,options={})
        #puts "DMS.get_car_model #{frame_no}"
        q="
SELECT *
FROM SM_VEHICLEINFO
WHERE FRAME_NO = '#{frame_no}'
"
        row=self.get(q)
        if not row
            throw "Car model cannot be found: #{frame_no}" if options[:required]
            return nil
        end
        return row
    end
end

module DMS
    @@dms_urls={
        #"YR"=>"jdbc:sqlserver://192.168.80.241;databaseName=DMS10142;user=dms;password=rama3",
        #"SP"=>"jdbc:sqlserver://202.100.80.241;databaseName=SP10109;user=dms;password=rama3",
        #"BM"=>"jdbc:sqlserver://192.168.80.241;databaseName=BM10124;user=dms;password=rama3",
        #"PHC"=>"jdbc:sqlserver://192.168.80.241;databaseName=PHC14113;user=dms;password=rama3",
        #"R3N"=>"jdbc:sqlserver://192.168.80.241;databaseName=DMS10138;user=dms;password=rama3",
        #"DHA"=>"jdbc:sqlserver://192.168.80.241;databaseName=DHA20111;user=dms;password=rama3",
        #"SW"=>"jdbc:sqlserver://192.168.80.241;databaseName=DMS20114;user=dms;password=rama3",

        # Inactive dms
        #"R3U"=>"jdbc:sqlserver://192.168.40.12;databaseName=DMS20116;user=almacom;password=7|98OnJS",

        "YR"=>"jdbc:sqlserver://192.168.80.12;databaseName=DMS10164;user=almacom;password=7|98OnJS",
        "SP"=>"jdbc:sqlserver://202.100.10.12;databaseName=DMS10160;user=almacom;password=7|98OnJS",
        "BM"=>"jdbc:sqlserver://192.168.30.12;databaseName=DMS10161;user=almacom;password=7|98OnJS",
        "PHC"=>"jdbc:sqlserver://192.168.20.12;databaseName=DMS14128;user=almacom;password=7|98OnJS",
        "R3N"=>"jdbc:sqlserver://192.168.50.12;databaseName=DMS10162;user=almacom;password=7|98OnJS",
        "DHA"=>"jdbc:sqlserver://192.168.10.12;databaseName=DMS10165;user=almacom;password=7|98OnJS",
        "SW"=>"jdbc:sqlserver://192.168.70.12;databaseName=DMS10163;user=almacom;password=7|98OnJS",
    }
    @@dms_connections={}

    def self.get_connection branch_code
        puts "DMS.get_connection",branch_code
        con=@@dms_connections[branch_code]
        return con if con
        con=DMSConnection.new
        con.connect @@dms_urls[branch_code]
        @@dms_connections[branch_code]=con
        return con
    end
end
