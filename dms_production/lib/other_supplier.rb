require "dms"
require "erp"
require "pp"
require "importer"

class Importer_OtherSupplier < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM SM_SUPPLIER WHERE SUPPLIER_NO='#{doc_no}'"
        res=@dms.get q
        raise "Supplier not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM SM_SUPPLIER WHERE LAST_UPDATE_DATE>='#{date_from}' AND LAST_UPDATE_DATE<='#{date_to}'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM SM_SUPPLIER WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM SM_SUPPLIER WHERE LAST_UPDATE_DATE>='#{t0}' ORDER BY SUPPLIER_NO"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        pp row
        partner_no=row["SUPPLIER_NO"].strip.upcase
        name=row["NAME_NM"].strip

        res=@erp.execute "partner.branch.code","search",[["code","=",partner_no],["company_id","=",@company_id]]
        code_id=res[0]
        if code_id
            @logger.info "found branch code: #{partner_no}"
            res=@erp.execute "partner.branch.code","read",[code_id],["partner_id"]
            partner_id=res[0]["partner_id"][0]
        else
            @logger.info "branch code not found: #{partner_no}"
            res=@erp.execute "res.partner","search",[["name","=",name]]
            if not res.empty?
                @logger.info "Found supplier with same name: #{name} => don't create new partner"
                partner_id=res[0]
            else
                @logger.info "Supplier with same name not found: #{name} => creating new partner"
                categ_id=@erp.get_partner_category "Other Suppliers",:required=>true
                supp_vals={
                    "name"=>name,
                    "customer"=>false,
                    "supplier"=>true,
                    "category_id"=>[[6,0,[categ_id]]],
                }
                partner_id=@erp.execute "res.partner","create",supp_vals
            end
        end

        addr_vals={
            "partner_id"=>partner_id,
            "type"=>"default",
            "name"=>name,
            "function"=>(row["SIR_NM"] or "").strip,
            "street"=>(row["DETAILS"] or "").strip,
            "street2"=>(row["TOWNCITY"] or "").strip,
            "zip"=>(row["ZIP_CD"] or "").strip,
            "country_id"=>@erp.get_country("TH",:required=>true),
            "city"=>(row["DISTRICT"] or "").strip+" "+(row["PROVINCE"] or "").strip,
            "phone"=>(row["TEL_NO"] or "").strip,
            "mobile"=>(row["MOBILE_NO"] or "").strip,
            "fax"=>(row["FAX_NO"] or "").strip,
        }
        addr_id=@erp.get_partner_address partner_id,:type=>"default"
        if not addr_id
            puts "creating new address"
            @erp.execute "res.partner.address","create",addr_vals
        end

        if not code_id
            puts "creating new branch code"
            code_vals={
                "partner_id"=>partner_id,
                "code"=>partner_no,
                "company_id"=>@company_id,
            }
            @erp.execute "partner.branch.code","create",code_vals
        end

        @logger.info "Supplier imported: #{partner_no}=>#{partner_id}"
        partner_id
    end
end
