# -*- encoding: utf-8 -*-
require "dms"
require "erp"
require "pp"
require "importer"

class Importer_Partner< Importer

    def query(number,date_from=false,date_to=false,last_update=false)

        cause= date_from!=false ?  " AND REGISTRATION_DATE >='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND REGISTRATION_DATE <='#{date_to} 23:59:59' ": "")
        cause << (last_update!=false ? " AND LAST_UPDATE_DATE>='#{last_update}' ": "")

        cause_customer = (number!=false ?  " AND ( CUSTOMER_NO='#{number}' ) ": "")
        cause_supplier = (number!=false ?  " AND ( SUPPLIER_NO='#{number}' ) ": "")
        cause_supplier_part = (number!=false ?  " AND ( SUPPLIER_CODE='#{number}' ) ": "")

        customer="
        SELECT upper(customer_no) AS REF,
            sir_nm AS TITLE,
            FIRST_NM AS NAME1,
            MIDDLE_NM AS NAME2 ,
            LAST_NM AS NAME3,
            ENGSIR_NM AS ETITLE,
            ENGFIRST_NM AS ENAME1,
            ENGMIDDLE_NM AS ENAME2 ,
            ENGLAST_NM AS ENAME3 ,
            'default' AS ADDR_TYPE,
            '' AS \"FUNCTION\",
            DETAILS AS STREET,
            TOWNCITY AS STREET2,
            DISTRICT AS CITY,
            PROVINCE AS STATE_ID ,
            ZIP_CD AS ZIP,
            TEL_NO AS PHONE,
            MOBILE_NO AS MOBILE,
            OTHER_NO AS PHONE2,
            FAX_NO AS FAX ,
            EMAIL AS EMAIL ,
            'Customers' AS CATEGORY,
            cast(CUSTOMERID as varchar(20)) AS TIN,
            --'' AS TIN,
            1 AS CUSTOMER,
            0 AS SUPPLIER,
            LAST_UPDATE_DATE AS WRITE_DATE
        FROM SM_CUSTOMERBASIC
        WHERE FIRST_NM is not null and ACTIVE_FLG=1
            #{cause_customer} #{cause}
        "

        customer_delivery="
        SELECT top 10 upper(customer_no) AS REF,
            sir_nm AS TITLE,
            FIRST_NM AS NAME1,
            MIDDLE_NM AS NAME2 ,
            LAST_NM AS NAME3,
            '' AS ETITLE,
            '' AS ENAME1,
            '' AS ENAME2 ,
            '' AS ENAME3,
            'delivery' AS ADDR_TYPE,
            '' AS \"FUNCTION\",
            DETAILS AS STREET,
            TOWNCITY AS STREET2,
            DISTRICT AS CITY,
            PROVINCE AS STATE_ID ,
            ZIP_CD AS ZIP,
            TEL_NO AS PHONE,
            '' AS MOBILE,
            '' AS PHONE2,
            FAX_NO AS FAX ,
            '' AS EMAIL ,
            'Customers' AS CATEGORY,
            '' AS TIN ,
            1 AS CUSTOMER,
            0 AS SUPPLIER,
            LAST_UPDATE_DATE AS WRITE_DATE
        FROM SM_CUSTOMERMAIL
        WHERE FIRST_NM is not null
            #{cause_customer} #{cause}
        --ORDER BY LAST_UPDATE_DATE DESC"

        customer_invoicing="
        SELECT top 10 upper(customer_no) AS REF,
            sir_nm AS TITLE,
            FIRST_NM AS NAME1,
            MIDDLE_NM AS NAME2 ,
            LAST_NM AS NAME3 ,
            '' AS ETITLE ,
            '' AS ENAME1,
            '' AS ENAME2 ,
            '' AS ENAME3 ,
            'invoice' AS ADDR_TYPE ,
            '' AS \"FUNCTION\",
            DETAILS AS STREET,
            TOWNCITY AS STREET2,
            DISTRICT AS CITY,
            PROVINCE AS STATE_ID ,
            ZIP_CD AS ZIP,
            TEL_NO AS PHONE,
            '' AS MOBILE,
            '' AS PHONE2,
            FAX_NO AS FAX ,
            '' AS EMAIL ,
            'Customers' AS CATEGORY,
            '' AS TIN ,
            1 AS CUSTOMER,
            0 AS SUPPLIER,
            LAST_UPDATE_DATE AS WRITE_DATE
        FROM SM_CUSTOMERCLAIM
        WHERE FIRST_NM is not null
            #{cause_customer} #{cause}
        --ORDER BY LAST_UPDATE_DATE DESC"

        supplier_part="
        SELECT UPPER(SUPPLIER_CODE) AS REF,
            '' AS TITLE,
            SUPPLIER_NAME_LOCAL1 AS NAME1,
            '' AS NAME2,
            '' AS NAME3,
            '' AS ETITLE,
            '' AS ENAME1,
            '' AS ENAME2 ,
            '' AS ENAME3,
            'default' AS ADDR_TYPE ,
            '' AS \"FUNCTION\",
            SUPPLIER_ADDRESS1 AS STREET,
            SUPPLIER_ADDRESS2 AS STREET2,
            SUPPLIER_ADDRESS3 AS CITY,
            SUPPLIER_ADDRESS4 AS STATE_ID,
            ZIP_CODE AS ZIP,
            TEL_NO1 AS PHONE,
            TEL_NO3 AS MOBILE,
            TEL_NO2 AS PHONE2,
            FAX_NO AS FAX,
            '' AS EMAIL,
         CASE WHEN SUPPLIER_TYPE='1' THEN 'HONDA-Parts' ELSE 'Parts' END AS CATEGORY,
            '' AS TIN,
            0 AS CUSTOMER,
            1 AS SUPPLIER,
            LAST_UPDATE_DATE AS WRITE_DATE
        FROM PM_VENDOR
        WHERE active_flag=1 
            #{cause_supplier_part} #{cause}
        "

        supplier="
        SELECT rtrim (upper (s.SUPPLIER_NO)) AS REF,
              '' AS TITLE,
              s.NAME_NM AS NAME1,
              '' AS NAME2,
              '' AS NAME3,
              '' AS ETITLE,
              '' AS ENAME1,
              '' AS ENAME2,
              '' AS ENAME3,
              'default' AS ADDR_TYPE,
              '' AS \"FUNCTION\",
              s.DETAILS AS STREET,
              s.TOWNCITY AS STREET2,
              s.DISTRICT AS CITY,
              s.PROVINCE AS STATE_ID,
              s.ZIP_CD AS ZIP,
              s.TEL_NO AS PHONE,
              '' AS PHONE2,
              s.FAX_NO AS FAX,
              '' AS EMAIL,
              CASE
                 WHEN s.SUPPLIER_KBN IN ('01', '21') THEN 'HONDA'
                 WHEN s.SUPPLIER_KBN = '02' THEN 'Dealers'
                 WHEN s.SUPPLIER_KBN = '04' THEN 'Service'
                 WHEN s.SUPPLIER_KBN = '06' THEN 'Insurance'
                 WHEN s.SUPPLIER_KBN = '07' THEN 'Finance'
                 WHEN s.SUPPLIER_KBN = '08' THEN 'Bank'
                 WHEN s.SUPPLIER_KBN = '12' THEN 'Branch'
                 ELSE ''
              END
                 AS CATEGORY,
              cast(s.TAXID_NO as varchar(20)) AS TIN,
              --'' AS TIN,
              0 AS CUSTOMER,
              1 AS SUPPLIER,
              s.REGISTRATION_DATE,
              s.LAST_UPDATE_DATE AS WRITE_DATE
         FROM SM_SUPPLIER s
        WHERE s.ACTIVE_FLG = 1
         #{cause_supplier} #{cause}
        "
        rows=[]
        sqls=[customer,customer_delivery,customer_invoicing,supplier,supplier_part]
        for sql in sqls
            rows+=@dms.query(sql)
        end

        #sub_query = list.map! { |k| "#{k}" }.join(" \n UNION ")
        #query = " SELECT * from (#{sub_query}) p ORDER BY p.REF"
        return rows
    end

    def import_by_no(partner_no)
        res=self.query(partner_no)
        raise "Partner not found: #{partner_no}" if not res
        partner_id = self.import_rows(res)
        #FIXME :  what happends when import by no get more than 1 partner
        return partner_id

    end

    def import_by_date(date_from,date_to)
        rows=self.query(false,date_from,date_to)
        partners = self.import_rows rows
        return partners
    end

    def import_daily(progress=nil)
        #t0=(Time.now-3600*24*1).strftime "%Y-%m-%d %H:%M:%S"
        #q=self.query_get(false,false,false,t0)
        #rows=@dms.query q
        #self.import_rows rows,progress
        return
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def format_row(rows)
        summary = {}
        rows.each do |rec|
          key= rec.values_at("REF","CATEGORY")
          summary[key] ||= []
          summary[key] << rec
        end
        return summary
    end

    def import_rows(rows)

        datas =self.format_row(rows)
        partners=[]
        for d in datas.keys.sort
            ref, category= d

            ref=ref.strip

            category_id=@erp.get_partner_category category,:required=>true

            partner_id=@erp.get_partner ref,@company_id

            row = datas[d][0]
            name=self.format_name(row)

            title_id=@erp.get_name_title row["TITLE"]
            vals={
                "name"=>name,
                "ref"=>ref,
                "lang"=>"th_TH",
                "last_dms_update"=> row["WRITE_DATE"].strftime("%Y-%m-%d %H:%M:%S"),
                "dms_name"=>name,
                "dms_title"=>row["TITLE"],
                "dms_category"=>category,
                "dms_ref"=>ref,
                "company_id"=>@company_id,
                "dms_company_id"=>@company_id,
                "customer"=>true,
                "supplier"=>true,
                "title"=> title_id ? title_id  : false
            }
            no_update=false

            if not partner_id

                vals["category_id"]=[[6,0,[category_id]]]

                partner_id=@erp.execute "res.partner","create",vals
                @logger.info "CREATED : Partner: #{ref}=>#{partner_id}"
            else
                #next #TEMP fix

                res=@erp.execute "res.partner","read",[partner_id],["no_update"]
                if res[0]["no_update"]
                    no_update=true
                    next #<<<
                end
                #vals.delete("name")
                #vals.delete("title")
                @logger.info "UPDATED: Partner: #{ref}=>#{partner_id}"
                @erp.execute "res.partner","write",[partner_id],vals
            end
            partners << partner_id

            self.import_address(partner_id,datas[d],no_update)
        end
        return partners.length==1 ? partners[0] : partners
    end

    def format_name(row,show_title=false)
        name=""
        if show_title
            name << ( row["TITLE"] ? row["TITLE"].strip  : "" )
        end

        name+=row["NAME1"].strip
        if not row["NAME2"].empty?
            name+=" "+row["NAME2"].strip
        end
        if not row["NAME3"].empty?
            name+=" "+row["NAME3"].strip
        end
        return name
    end

    def format_phone(row)
        phone=""
        if not row["PHONE"]
            phone+=row["PHONE"].strip
        end
        if not row["PHONE2"]
            phone+=","+row["PHONE2"].strip
        end
        return phone
    end

    def import_address(partner_id,rows,no_update)
        addrs=[]
        for row in rows

            street = row["STREET"].strip.gsub("\u001E","")

            # hot fix for RI1410V1210
            #if street.match(/111/)
                #street = "111/33 หมู่ที่ 11 ซ.ธนสิทธิ์ ถ.เทพารักษ์"
            #end

            phone=self.format_phone(row)
            name=self.format_name(row,show_title=true)
            addr_vals={
                "partner_id"=>partner_id,
                "type"=>row["ADDR_TYPE"],
                "name"=>name,
                "street"=>street,
                "street2"=>(row["STREET2"] or "").strip,
                "zip"=>(row["ZIP"] or "").strip,
                "country_id"=>@erp.get_country("TH",:required=>true),
                "city"=>(row["CITY"] or "").strip+" "+(row["STATE_ID"] or "").strip,
                "phone"=>phone,
                "mobile"=>(row["MOBILE"] or "").strip,
                "fax"=>(row["FAX"] or "").strip,
            }
            #XXX: now to get partner address
            addr_id=@erp.get_partner_address partner_id,:type=>row["ADDR_TYPE"]
            if not addr_id
                addr_id=@erp.execute "res.partner.address","create",addr_vals
                @logger.info "CREATED : Partner:address : #{addr_id}"
            else
                #next #temfix

                if no_update
                    next #<<<
                end
                @logger.info "UPDATED: Partner:address : #{addr_id}"
                addr_id=@erp.execute "res.partner.address","write",[addr_id],addr_vals
            end

            addrs<<addr_id

        end
        return addrs
    end

end
