require "dms"
require "erp"
require "importer"

class Importer_JobProduct < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM RM_JOBDENOTE WHERE JOB_DENOTE_CD='#{doc_no}'"
        res=@dms.get q
        raise "Part not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM RM_JOBDENOTE WHERE LAST_UPDATE_DATE>='#{date_from}' AND LAST_UPDATE_DATE<='#{date_to}'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM RM_JOBDENOTE WHERE LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM RM_JOBDENOTE WHERE LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def import_row(row)
        job_no=row["JOB_DENOTE_CD"].strip
        product_id=@erp.get_product(job_no)
        if product_id
            @logger.info "Product #{job_no} already created"
            return
        end
        vals={
            "default_code"=>job_no,
            "name"=>row["JOB_DENOTE_ENG_NM"],
            "type"=>"service",
            "categ_id"=>@erp.get_product_category("Jobs",:required=>true),
        }
        @erp.execute "product.product","create",vals
        @logger.info "Created product #{job_no}"
    end
end
