require "dms"
require "erp"
require "pp"
require "importer"

class Importer_SuppInvoiceCar < Importer
    def query_get(number,date_from=false,date_to=false,last_update=false)
        #LAST_UPDATE_DATE always same as RECEIVING_DATE
        #
        cause= date_from!=false ?  " AND c.RECEIVING_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND c.RECEIVING_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( c.DISTINVOICE_NO='#{number}' OR c.RECEIVING_NO='#{number}' OR c.FRAME_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND c.LAST_UPDATE_DATE>='#{last_update}'": "")

        #Union 2 tables
        # defined query in erp class
        tables = ["ST_PURCHASE","ST_PURCHASEHISTORY"]
        query = @erp.query_get_supp_inv_car(tables,cause)
        return query
    end

    def import_by_no(doc_no)
        q = self.query_get(doc_no)
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q = self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        miss=Array.new
        for row in rows
            inv_no=row["INVOICE_NO"].strip
            dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
            erp_docnos=@erp.get_invoice_reference "in_invoice",date_from,date_to,inv_no,@company_id
            miss=check_missing dms_docnos,erp_docnos
        end
        return miss
    end

    def import_row(row)
        inv_no=row["INVOICE_NO"].strip
        pick_no=row["PICK_NO"].strip
        origin=row["ORIGIN"].strip

        if row["CATEGORY"]=="branch"
            return
        end

        res=@erp.execute "account.invoice","search",[["type","=","in_invoice"],["origin","=",origin],["company_id","=",@company_id]]

        if not res.empty?
            @logger.info "SKIPPED : Invoice already created: #{origin}"
            return
        end

        partner_no=row["PARTNER_NO"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        address_id=@erp.get_partner_address partner_id,:type=>"invoice",:required=>true
        if not address_id
            address_id=@erp.get_partner_address partner_id,:type=>"default",:required=>true
        end

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        res=@erp.execute "product.product","read",[product_id],["name","variants"]
        prod_name=res[0]["name"]
        prod_variants=res[0]["variants"]
        date_invoice=row["DATE_INVOICE"].strftime "%Y-%m-%d"
        doc_date=row["INV_DOC_DATE"].strftime "%Y-%m-%d"

        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        journal_id=@erp.get_journal("SUPP_INV_CAR_JOURNAL",@company_id,:required=>true)

        inv_vals={
            "origin"=>origin,
            "reference"=>inv_no,
            "journal_id"=>journal_id,
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>"in_invoice",
            "account_id" => @erp.get_account("SUPP_INV_CAR_PAYABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date" => doc_date,
            "vehicle_id" => vehicle_id,
            "address_invoice_id" => address_id,
            "check_total"=>row["AMOUNT_UNTAXED"]+row["AMOUNT_TAX"],
            "manual_vat"=>1,
        }

        tax_id=@erp.get_tax "IN_VAT7_EXCL",:required=>true
        line_vals={
            "uos_id"=>@erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("SUPP_INV_CAR_EXPENSE",@parent_company_id,:required=>true),
            "name"=>prod_name+" "+prod_variants,
            "product_id"=>product_id,
            "quantity"=>1,
            "price_unit"=>row["AMOUNT_UNTAXED"],
            "invoice_line_tax_id"=>[[6,0,[tax_id]]],
        }
        inv_vals["invoice_line"]=[[0,0,line_vals]]

        vat_vals={
            "tax_id"=>@erp.get_tax("IN_VAT7_EXCL",:required=>true),
            "name"=>"IN_VAT7_EXCL",
            "base_amount"=>row["AMOUNT_UNTAXED"],
            "tax_amount"=>row["AMOUNT_TAX"],
            "ref"=>inv_no,
            "date"=>date_invoice,
            "partner_id"=>partner_id,
            "partner_name"=>@erp.get_partner_name(partner_id),
            "account_id"=>@erp.get_account("TAX_IN_VAT7_EXCL",@parent_company_id,:required=>true),
            "company_id"=>@company_id,
        }
        inv_vals["vat_lines"]=[[0,0,vat_vals]]

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "CREATED : Invoice : #{inv_no}=>#{inv_id}"
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
