require "dms"
require "erp"
require "importer"
require "pp"
#CONtain of supplier shipment
#doc no is varies but not start with PZ

class Importer_PartsReceivedSupplier < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM PT_FLHMST WHERE INVOICE_NO NOT LIKE 'PZ%' AND  FLOAT_NO='#{doc_no}'"
        res=@dms.get q
        raise "Job order not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM PT_FLHMST WHERE INVOICE_NO NOT LIKE 'PZ%'
                AND INCOMING_DATE >='#{date_from} 00:00:00'
                AND INCOMING_DATE <='#{date_to} 23:59:59' ORDER BY FLOAT_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q="SELECT * FROM PT_FLHMST WHERE INVOICE_NO NOT LIKE 'PZ%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM PT_FLHMST WHERE INVOICE_NO NOT LIKE 'PZ%' AND LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def check_import(date_from,date_to)
        puts "Check_import_parts_received_supplier",date_from,date_to
        q="SELECT INVOICE_NO,FLOAT_NO FROM PT_FLHMST WHERE INVOICE_NO NOT LIKE 'PZ%' AND INCOMING_DATE>='#{date_from} 00:00:00' AND INCOMING_DATE<='#{date_to} 23:59:59' ORDER BY FLOAT_NO"
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="PF%"
        for row in rows
            invoice=row["FLOAT_NO"].strip+" "+row["INVOICE_NO"].strip
            dms_docnos.push(invoice)
        end
        pp dms_docnos
        puts "################################################"
        erp_docnos=@erp.get_picking_docnos inv_no,"in",date_from,date_to,@company_id
        pp erp_docnos
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        float_no=(row["FLOAT_NO"] or "").strip
        inv_no=row["INVOICE_NO"].strip
        origin=float_no + " " + inv_no
        #if some of this will be skipped because it already import from "parts_delivered_branch"
        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "UPDATED : Picking already created: #{origin}"
            @erp.update_picking(res,origin,@company_id)
            return
        end 
        partner_no= row["SUPPLIER_CODE"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)

        if not address_id
            address_id=@erp.get_partner_address(partner_id,:required=>true)
        end

        if not row["INCOMING_DATE"].nil?
            date=row["INCOMING_DATE"].strftime "%Y-%m-%d %H:%M:%S"
            date_done=row["INCOMING_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        else
            date=row["SHIPPING_DATE"].strftime "%Y-%m-%d %H:%M:%S"
            date_done=row["SHIPPING_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        end

        invoice_id = @erp.get_invoice_id(inv_no,@company_id)

        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done" => date_done,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=> "in",
            "address_id"=>address_id,
            "stock_journal_id"=>@erp.get_stock_journal("PARTS_RECEIVED_SUPPLIER_JOURNAL",@company_id), #PART_IN
            "invoice_id"=>invoice_id,
        }

        lines=[]
        lines=get_lines(float_no,row,date)
        pick_vals["move_lines"]=lines.collect {|line_vals| [0,0,line_vals]}

        #pp "vals",pick_vals
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created picking #{float_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end

    def get_lines(float_no,job_row,date)
        q="
SELECT *
 FROM PT_FLDMST
 WHERE FLOAT_NO='#{float_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            part_no=row["PART_NO"].strip
            product_id=@erp.get_product part_no,@branch_code,"part_product" ,:required=>true

            line_no=row["LINE_NO"].to_s
            qty=row["INCOMING_QTY"]
            vals={
                "name"=>float_no+" "+line_no,
                "date"=>date,
                "date_expected"=>date,
                "product_id"=>product_id,
                "product_uom"=>@erp.get_uom("PCE",:required=>true),
                "product_qty"=>qty,
                "location_id"=> @erp.get_location("Suppliers",:required=>true),
                "location_dest_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
                "state"=>"assigned",
                "price_unit"=>row["INCOMING_COST"],
            }
            lines<<vals
        end
        lines
    end
end
