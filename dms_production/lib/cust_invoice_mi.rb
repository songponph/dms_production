require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoiceMI < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND INVOICE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND INVOICE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( INVOICE_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND LAST_UPDATE_DATE>='#{last_update}' ": "")
        query="
            SELECT  INVOICE_DATE,
                    INVOICE_NO,
                    INVOICE_TOTAL,
                    '00000000' as DEALER_CD_HATC,
                    FIRST_NM,
                    SIR_NM,
                    DETAILS_NM,
                    CITYTOWN_NM,
                    ZIP_CD,
                    DISTRICT_NM,
                    TELEPHONE_NO,
                    FAX_NO,
                    PROVINCE_NM
            FROM    RT_PM_INV_H
            WHERE   INVOICE_NO like 'MI%'
            " +cause+ " ORDER BY INVOICE_DATE,INVOICE_NO"

        return query
    end

    def import_by_no(doc_no)
        puts "Importer_CustInvoicePreventiveMaintenance.import_by_no",doc_no
        q=self.query_get(doc_no)
        res=@dms.get q
        raise "Customer invoice for Preventive Maintenance not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        puts "Importer_CustInvoicePreventiveMaintenance.import_by_date",date_from,date_to
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustInvoicePreventiveMaintenance.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d"
        q=self.query_get(false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d"
        #q=self.query_get(false,t0)
        #rows=@dms.query q
        #self.import_rows rows,progress
        return
    end

    def check_import(date_from,date_to)
        puts "Check_import_FI",date_from,date_to
        q="
            SELECT  INVOICE_NO
            FROM    RT_PM_INV_H
            WHERE   INVOICE_NO like 'MI%'
            AND     INVOICE_DATE>='#{date_from} 00:00:00'
            AND     INVOICE_DATE<='#{date_to} 23:59:59'
            ORDER BY INVOICE_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_invoice",date_from,date_to,"FI%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        puts "Importer_CustInvoicePreventiveMaintenance.import_row"
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        t0=Time.now()
        origin=row["INVOICE_NO"].strip
        if origin.empty?
            @logger.info "SKIPPED : No invoice no found........"
            return
        end

        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "SKIPPED : already created:#{origin}"
            return
        end

        partner_no=row["DEALER_CD_HATC"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.create_address_invoice_claim_free_service row

        date_invoice=row["INVOICE_DATE"].strftime("%m/%d/%y")

        inv_vals={
            "origin" => origin,
            "reference" => origin,
            "journal_id" => @erp.get_journal("CUST_INV_PM_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_invoice",
            "account_id" => @erp.get_account("CUST_INV_PM_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date"=>date_invoice,
            "address_invoice_id" => address_id,
        }

        lines=[]
        lines+=get_lines(origin)
        inv_vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "invoice created: #{origin}=>#{inv_id}"
        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["INVOICE_TOTAL"]).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{row['INVOICE_TOTAL']}"
            @logger.error msg
            raise msg
        end
        puts "Posting invoice............."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end

    def get_lines(inv_no) 
        q="
        SELECT  PART_AMOUNT_APPROVE,
                APPROVE_AMOUNT,
                FSC_ITEM,
                FSC_NAME,
                CAR_NM,
                YEAR
        FROM    RT_FSC_INV_SUM_PLUS
        WHERE   INVOICE_NO='#{inv_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows

            # Get Income Account
            account_id=false
            # Service Income
            if not row["PART_AMOUNT_APPROVE"].nil?
                account_id=@erp.get_account "CUST_INV_PM_INCOME_PARTS",@parent_company_id,:required=>true
                amt=row["PART_AMOUNT_APPROVE"]
                tax_id=@erp.get_tax "OUT_VAT7_EXCL",@parent_company_id
                uom_id=@erp.get_uom "PCE",:required=>true

                qty=row["FSC_ITEM"]
                price=amt/qty

                name=(row["FSC_NAME"].to_s).strip+" "+row["CAR_NM"].strip+" "+ row["YEAR"].to_s
                vals={
                    "name"=> name,
                    "quantity"=>qty,
                    "price_unit"=>price,
                    "invoice_line_tax_id"=>[[6,0,[tax_id]]],
                    "uos_id"=>uom_id,
                    "account_id"=>account_id,
                }
                lines<<vals
            end
            # Parts Income
            if not row["APPROVE_AMOUNT"].nil?
                account_id=@erp.get_account "CUST_INV_PM_INCOME_SERVICE",@parent_company_id,:required=>true
                amt=row["APPROVE_AMOUNT"]

                tax_id=@erp.get_tax "OUT_VAT7_EXCL",@parent_company_id
                uom_id=@erp.get_uom "PCE",:required=>true

                qty=row["FSC_ITEM"]
                price=amt/qty

                name=(row["FSC_NAME"].to_s).strip+" "+row["CAR_NM"].strip+" "+ row["YEAR"].to_s
                vals={
                    "name"=> name,
                    "quantity"=>qty,
                    "price_unit"=>price,
                    "invoice_line_tax_id"=>[[6,0,[tax_id]]],
                    "uos_id"=>uom_id,
                    "account_id"=>account_id,
                }
                lines<<vals
            end
        end
        return lines
    end
end
