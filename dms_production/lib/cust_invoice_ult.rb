require "dms"
require "erp"
require "pp"
require "importer"
#TODO: copy improved function from RM
class Importer_CustInvoiceUlt < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND i.PRE_INVOICE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND i.PRE_INVOICE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( i.PRE_INVOICE_NO='#{number}' or i.INVOICE_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND i.LAST_UPDATE_DATE>='#{last_update}' ": "")

        #TODO : import as Cash and be able to import as credit
        query="
            SELECT i.INVOICE_NO,
                   i.UC_NO,
                   i.PRE_INVOICE_NO,
                   rtrim(rtrim(i.PRE_INVOICE_NO)+' '+rtrim(i.UC_NO)) AS ORIGIN,
                   i.CUSTOMER_NO,
                   i.INVOICE_DATE,
                   i.PRE_INVOICE_DATE,
                   i.USER_ID,
                   i.FRAME_NO,
                   i.CAR_NM,
                   i.MODELYEAR,
                   i.REG_NO,
                   i.MILEAGE,
                   i.CHARGE_AMOUNT,
                   i.INVOICE_DETAILS,
                   i.GOODS_AMOUNT,
                   i.VAT_AMOUNT,
                   COALESCE(i.SIR_NM_RECEIPT,'')+rtrim(i.FIRST_NM+COALESCE(i.MIDDLE_NM_RECEIPT,''))+' '+i.LAST_NM AS NAME,
                   i.ADDRESS_NO_RECEIPT+' '+i.VILLAGE_RECEIPT+' '+i.MOO_RECEIPT+ ' '+i.SOI_RECEIPT AS STREET,
                   i.TOWNCITY_RECEIPT AS STREET2,
                   i.DISTRICT_RECEIPT+' '+i.PROVINCE_RECEIPT AS CITY,
                   i.TEL_NO_RECEIPT AS PHONE,
                   i.MOBILE_NO_RECEIPT AS MOBILE,
                   i.ZIP_CD_RECEIPT AS ZIP,
                   i.DISCOUNT_AMOUNT AS DISCOUNT,
                   i.DISCOUNT_BALANCE,
                   u.SECTION_CD
            FROM RU_ULTIMATE_CARE i,
                 CM_STAFF u
            WHERE i.PRE_INVOICE_NO IS NOT NULL
              AND i.PRE_INVOICE_NO!=''
              AND i.INVOICE_NO IS NOT NULL
              AND i.USER_ID=u.USER_ID
        "+cause+"
        ORDER BY i.PRE_INVOICE_DATE,i.PRE_INVOICE_NO "

        return query
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        # 1 hour back
        t0=(from_t-3600).strftime "%Y-%m-%d %H:%M:00"

        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["ORIGIN"].strip}

        #check all invoice with that origin
        erp_docnos=@erp.get_invoice_docnos "",date_from,date_to,"UM%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end


    def import_row(row)
        invoice_id=self.create_invoice(row)
        move_id=self.create_account_move(row)
        return invoice_id
    end

    def create_invoice(row)
        pp(row)
        pm_no=row["PRE_INVOICE_NO"].strip

        inv_no=row["INVOICE_NO"].strip
        internal_number=row["UC_NO"].strip
        origin=row["ORIGIN"]

        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "SKIPPED : Invoice already created: #{pm_no}"
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.create_address_invoice row

        date_invoice=row["PRE_INVOICE_DATE"].strftime("%Y-%m-%d")
        doc_date= (not row["INVOICE_DATE"].nil?) ? row["INVOICE_DATE"].strftime("%Y-%m-%d") : date_invoice

        reference=pm_no

        pmt_method = @dms.get_payment_method(pm_no,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id})

        if pmt_method.nil?
            @logger.info "SKIPPED : UI no payment method:#{inv_no}"
            return
        end

        tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
        #TODO: if date_invoice is not empty but doc_date is empty import to credit
        if ( pmt_method[0]=="credit" ) or (date_invoice!=doc_date)
            doc_date=date_invoice
            inv_type="out_invoice"
            journal_code="CUST_INV_ULTCARE_JOURNAL_CREDIT"
            tax_id=@erp.get_tax "OUT_VAT7_SUS_EXCL",:required=>true
        else

            if inv_no.empty?
                @logger.info "SKIPPED : No UI# => don't Import cash sale"
                return
            end

            reference=inv_no
            inv_type="out_cash"
            journal_code="CUST_INV_ULTCARE_JOURNAL_CASH"
        end

        frame_no=row["FRAME_NO"].strip
        product_id=@erp.get_product "ULTC",:required=>true
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        staff_cd=row["USER_ID"].strip
        user_id=@erp.get_user staff_cd,@branch_code,:required=>true

        comment=""
        if row["CAR_NM"] != nil
            comment+=" "+row["CAR_NM"].strip
        end
        if row["MODELYEAR"] != nil
            comment+=" "+row["MODELYEAR"].to_s
        end
        if row["REG_NO"] != nil
            comment+= " "+row["REG_NO"].to_s
        end

        if row["MILEAGE"]!= nil
            comment+= " "+row["MILEAGE"].to_s
        end
        comment+= " "+row["UC_NO"]

        inv_vals={
            "origin"=>origin,
            "reference"=>reference,
            "comment"=> comment,
            "journal_id"=>@erp.get_journal(journal_code,@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>inv_type,
            "account_id" => @erp.get_account("CUST_INV_ULTCARE_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date"=> date_invoice,
            "user_id" => user_id,
            "address_invoice_id" => address_id,
            "vehicle_id" => vehicle_id,
        }

        amount_to_pay=row["CHARGE_AMOUNT"]


        name=row["INVOICE_DETAILS"].strip
        analytic_id=@erp.get_analytic_account "PT_COUNTER",@company_id,:required=>true
        income_code = "CUST_INV_ULTCARE_INCOME_SV"
        if row["SECTION_CD"]=="SL"
            income_code = "CUST_INV_ULTCARE_INCOME"
        end

        # income service/sale
        account_id = @erp.get_account(income_code,@parent_company_id,:required=>true)

        line_vals={
            "uos_id"=>@erp.get_uom("PCE",:required=>true),
            "account_id" => account_id,
            "name"=>name,
            "quantity"=>1,
            "price_unit"=>row["GOODS_AMOUNT"],
            "discount"=>row["DISCOUNT"],
            "product_id" => product_id,
            "invoice_line_tax_id"=>[[6,0,[tax_id]]],
            "analytic_id"=>analytic_id,
        }

        inv_vals["invoice_line"]=[[0,0,line_vals]]

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "CREATED : invoice : #{inv_no}=>#{inv_id}"

        #TODO: write get payment method not to copy for many transform
        pmt_method = @dms.get_payment_method(pm_no,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id})

        if pmt_method.nil?
            @logger.info "SKIPPED : UI no payment method:#{inv_no}"
            return
        end

        if inv_type=="out_cash"
            self.set_payment_method(inv_id,pmt_method)
        end


        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        check_total=row["CHARGE_AMOUNT"]
        if (amt-check_total).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{check_total}"
            @logger.error msg
            raise msg
        end
        if inv_type=="out_invoice"
            @erp.exec_workflow "account.invoice","invoice_open",inv_id
        end
        return inv_id
    end

    def create_account_move(row)
        pm_no=row["PRE_INVOICE_NO"].strip
        inv_no=(row["INVOICE_NO"] or "").strip
        internal_number=row["UC_NO"].strip

        if inv_no.empty?
            @logger.info "SKIPPED : No UI# => don't create account move"
            return
        end
        ref=inv_no+" "+pm_no
        #ref=inv_no+" "+pm_no+" "+internal_number
        date_invoice=row["PRE_INVOICE_DATE"].strftime("%Y-%m-%d")
        doc_date= (not row["INVOICE_DATE"].nil?) ? row["INVOICE_DATE"].strftime("%Y-%m-%d") : ''
        origin=row["ORIGIN"]
        if doc_date < "2012-08-01"
            @logger.info "SKIPPED : No UI# => don't create account move"
            return
        end

        pmt_method = @dms.get_payment_method(pm_no,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id})

        if pmt_method.nil?
            @logger.info "SKIPPED : UI no payment method:#{inv_no}"
            return
        end

        if pmt_method[0]=='credit'
            @logger.info "CHECKING: Credit sale => create account move NO-#{pm_no} DATE-#{doc_date}"
        else
            #if it is cash sale but import to credit sale it should create also
            origin = pm_no+" "+internal_number
            invoice_id =@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
            if invoice_id.empty?
                @logger.info "SKIPPED : No invoice created ( this shoule not happend )"
                return
            end

            res=@erp.execute "account.invoice","read",invoice_id,["type"]
            if res[0]["type"]=="out_cash"
                @logger.info "SKIPPED : UM : Cash sale => don't create account move"
                return
            end
        end

        res=@erp.execute "account.move","search",[["company_id","=",@company_id],'|',["ref","=",ref],["ref","=",ref+internal_number]]

        if not res.empty?
            @logger.info "SKIPPED : Account move already created #{inv_no}"
            return
        end
        journal_id=@erp.get_journal("CUST_TAX_INV_SERVICE_JOURNAL",@company_id,:required=>true)
        period_id=@erp.get_period(doc_date,@company_id,:required=>true)
        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        tax_amt=row["VAT_AMOUNT"]
        base_amt=row["GOODS_AMOUNT"]

        if (not row["DISCOUNT_BALANCE"].nil?) and row["DISCOUNT_BALANCE"]!=0.0
            base_amt=row["DISCOUNT_BALANCE"]
        end

        move_vals={
            "ref"=>ref,
            "company_id"=>@company_id,
            "journal_id"=>journal_id,
            "date"=>doc_date,
            "period_id"=>period_id,
        }

        line1_vals={
            "name"=>pm_no,
            "ref"=>pm_no,
            "journal_id"=>journal_id,
            "period_id"=>period_id,
            "account_id"=>@erp.get_account("TAX_OUT_VAT7_SUS_EXCL",@parent_company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "debit"=>tax_amt,
            "date"=>doc_date,
        }
        line2_vals={
            "name"=>inv_no,
            "ref"=>inv_no,
            "journal_id"=>journal_id,
            "period_id"=>period_id,
            "account_id"=>@erp.get_account("TAX_OUT_VAT7_EXCL",@parent_company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "credit"=>tax_amt,
            "date"=>doc_date,
        }
        move_vals["line_id"]=[[0,0,line1_vals],[0,0,line2_vals]]
        res1=@erp.execute "res.partner","read",[partner_id],["name"]
        partner_name=res1[0]["name"]

        tax1_vals={
            "tax_id"=>@erp.get_tax("OUT_VAT7_SUS_EXCL",:required=>true),
            "base_amount"=>-base_amt,
            "tax_amount"=>-tax_amt,
            "date"=>date_invoice,
            "partner_id"=>partner_id,
            "partner_name"=> partner_name,
            "account_id"=>@erp.get_account("TAX_OUT_VAT7_SUS_EXCL",@parent_company_id,:required=>true),
            "ref"=>pm_no,
        }
        tax2_vals={
            "tax_id"=>@erp.get_tax("OUT_VAT7_EXCL",:required=>true),
            "base_amount"=>base_amt,
            "tax_amount"=>tax_amt,
            "date"=>doc_date,
            "partner_id"=>partner_id,
            "partner_name"=> partner_name,
            "account_id"=>@erp.get_account("TAX_OUT_VAT7_EXCL",@parent_company_id,:required=>true),
            "ref"=>inv_no,
        }
        move_vals["vat_lines"]=[[0,0,tax1_vals],[0,0,tax2_vals]]

        move_id=@erp.execute "account.move","create",move_vals
        @erp.execute "account.move","post",[move_id]
        @logger.info "CREATED : Account move #{ref}=>#{move_id}"
        return move_id
    end
end
