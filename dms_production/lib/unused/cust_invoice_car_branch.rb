require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoiceCarBranch < Importer

    def import_by_no(doc_no)
        q="SELECT * FROM ST_RORDERINV WHERE INVOICE_NO='#{doc_no}'"
        res=@dms.get q
        raise "Customer invoice to branch not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM ST_RORDERINV WHERE INV_PRINT_DATE>='#{date_from} 00:00:00' AND INV_PRINT_DATE<='#{date_to} 23:59:59' AND INVOICE_NO LIKE 'SF%' AND SUPPLIER_KBN = 12"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_RORDERINV WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'SF%' AND SUPPLIER_KBN = 12"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_RORDERINV WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'SF%' AND SUPPLIER_KBN = 12"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        pp "row:",row
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        t0=Time.now()
        origin=row["INVOICE_NO"].strip
        if origin.empty?
            @logger.info "No invoice found........"
            return
        end

        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end

        partner_no=row["CLAIM_CUST_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,"customer",:required=>true
        address_id=@erp.get_partner_address(partner_id,:type=>"invoice",:required=>true)

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,:required=>true

        analytic_id=@erp.get_analytic_account "SALE_DEALER",@company_id,:required=>true

        date_invoice=row["INV_PRINT_DATE"].strftime("%m/%d/%y")

        vals_={
            "origin" => origin,
            "reference" => origin,
            "journal_id" => @erp.get_journal("CUST_INV_CAR_BRANCH_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_invoice",
            "account_id" => @erp.get_account("CUST_INV_CAR_BRANCH_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "user_id" => user_id,
            "address_invoice_id" => address_id,
            "account_analytic_id" => analytic_id,
            "vehicle_id" => vehicle_id,
        }

        model_no=row["MTOC_CD"].strip 
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        res=@erp.execute "product.product","read",[product_id],["name"]
        prod_name=res[0]["name"]
        price_unit=row["AFTER_DISC_CR"]
        check_amount=row["AFTER_DISC_CR"]+row["VAT_CR"]
        lines_={
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_INV_CAR_BRANCH_INCOME",@parent_company_id,:required=>true),
            "name" => prod_name,
            "product_id" => product_id,
            "quantity" => 1,
            "price_unit" => price_unit,
            "account_analytic_id" => analytic_id,
            }

        vals_["invoice_line"]=[[0,0,lines_]]


        pp "vals:",vals_
        pp "lines:",lines_
        t1=Time.now()

        inv_id=@erp.execute "account.invoice","create",vals_
        t2=Time.now()
    
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]

        t2_2=Time.now()
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-check_amount).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{check_amount}"
            @logger.error msg
            raise msg
        end
        puts "Posting invoice............."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        t4=Time.now()
        return inv_id
    end
end
