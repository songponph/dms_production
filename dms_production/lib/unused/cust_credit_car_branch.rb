require "dms"
require "erp"
require "importer"
require "pp"
#supplier_kbn=12 branch

class Importer_CreditCarBranch < Importer

    def import_by_no(creditnote_no)
        puts "Importer_CustCreditCarBranch.import_by_no",creditnote_no
        q="SELECT * FROM ST_RORDERINV WHERE INVOICE_CN_NO='#{creditnote_no}'"
        res=@dms.get q
        raise "Invoice not found: #{creditnote_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        puts "Importer_CustCreditCarBranch.import_by_date",date_from,date_to
        q="SELECT * FROM ST_RORDERINV  WHERE INV_CN_PRINT_DATE>='#{date_from}' AND INV_CN_PRINT_DATE<='#{date_to}' AND INVOICE_CN_NO LIKE 'S0%' AND INVOICE_NO LIKE 'SF%'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustCreditCarBranch.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_RORDERINV WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_CN_NO LIKE 'S0%' AND INVOICE_NO LIKE 'SF%'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_daily(progress=nil)
        t0=(Time.now-60*10).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_RORDERINV WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_CN_NO LIKE 'S0%' AND INVOICE_NO LIKE 'SF%'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        puts "Importer_CustCreditCarBranch.import_row"
        pp "row:",row
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        puts "Customer credit car branch....."
        origin=row["INVOICE_CN_NO"].strip

        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created:#{origin}"
            return
        end

        partner_no=row["CLAIM_partner_no"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,"customer",:required=>true

        address_id=@erp.get_partner_address(partner_id,:type=>"invoice",:required=>true)

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        date_invoice=row["INV_CN_PRINT_DATE"].strftime("%m/%d/%y")

        original_invoice=row["INVOICE_NO"].strip
        #original_invoice_id=@erp.get_origin_invoice original_invoice,@company_id,original_invoice,@branch_code,"cust_invoice_car_dealer",:required=>true
        original_invoice_id=@erp.get_origin_invoice original_invoice,@company_id

        if not row["FRAME_NO"].empty?
            frame_no=row["FRAME_NO"]
            vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        end
        vals_={
            "origin" => origin,
            "reference" => origin,
            "journal_id" => @erp.get_journal("CUST_CREDIT_CAR_BRANCH_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_refund",
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_BRANCH_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "vehicle_id" => vehicle_id,
            "orig_invoice_id" => original_invoice_id,
            "address_invoice_id" => address_id,
        }

        car_name=@dms.get_car_name row["MTOC_CD"],:required=>true

        lines_= {
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_BRANCH_INCOME",@parent_company_id,:required=>true),
            "name" => car_name,
            "quantity" => 1,
            "price_unit" => row["TOTAL_CR"],
            "product_id" => product_id
        }

        vals_["invoice_line"]=[[0,0,lines_]]

        inv_id=@erp.execute "account.invoice","create",vals_
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["TOTAL_CR"]).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{row["TOTAL_CR"]}"
            @logger.error msg
            raise msg
        end
        puts "Posting invoice............."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
