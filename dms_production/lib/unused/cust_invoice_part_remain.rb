require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoicePartRemain < Importer
    def import_by_no(doc_no)
        puts "Importer_CustInvoicePartsDeposit.import_by_no",doc_no
        q="SELECT * FROM PT_ROHMST WHERE INVOICE_NO='#{doc_no}'"
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_CustInvoicePartsDeposit.import_by_date",date_from,date_to
        q="SELECT * FROM PT_ROHMST WHERE ORDER_DATE>='#{date_from} 00:00:00' AND ORDER_DATE<='#{date_to} 23:59:59' AND INVOICE_NO LIKE 'PI%' ORDER BY INVOICE_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustInvoicePartsDeposit.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PT_ROHMST WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'PI%'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PT_ROHMST WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'PI%'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_cust_invoice_part_deposit",date_from,date_to
        q="SELECT INVOICE_NO FROM PT_ROHMST WHERE INVOICE_NO LIKE 'PI%' AND ORDER_DATE>='#{date_from} 00:00:00' AND ORDER_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_invoice",date_from,date_to,"PI%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        puts "Importer_CustInvoicePartsDeposit.import_row"
        pp "row:",row
        inv_no=row["INVOICE_NO"].strip
        order_no=row["ORDER_NO"].strip
        origin=inv_no+" "+order_no
        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,"customer",:required=>true
        address_id=@erp.get_partner_address partner_id,:type=>"default",:required=>true
        date=row["ORDER_DATE"].strftime("%m-%d-%y")

        inv_vals={
            "origin"=>origin,
            "reference"=>inv_no,
            "journal_id"=>@erp.get_journal("CUST_INV_PART_DEPOSIT_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>"out_cash",
            "account_id"=>@erp.get_account("CUST_INV_PART_DEPOSIT_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice"=>date,
            "address_invoice_id"=>address_id,
        }
       
        lines=[]
        deposit=row["DEPOSIT_AMOUNT"]
        check_amount=row["TOTAL_AMOUNT"]-(deposit + row["DEPOSIT_TAX_AMOUNT"])
 
        uom_id=@erp.get_uom "PCE",:required=>true
        tax_id=@erp.get_tax "OUT_VAT7_EXCL",@parent_company_id
        deposit_invoice=@erp.get_origin_invoice inv_no,@company_id,:required=>true
        deposit_line_={
            "deposit_id"=>deposit_invoice,
            "amount"=> deposit,
        }
        inv_vals["inv_deposit_lines"]=[[0,0,deposit_line_]] 
        #line_1={
            #"name"=> "DEPOST AMOUNT",
            #"quantity"=>1,
            #"price_unit"=>-(deposit),
            #"uos_id"=>uom_id,
            #"account_id"=>@erp.get_account("CUST_INV_PART_DEPOSIT",@parent_company_id,:required=>true),
            #"invoice_line_tax_id"=>[[6,0,[tax_id]]],
           #}  
        lines+=get_lines(order_no,row)
        inv_vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}
        pp "vals:",inv_vals

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "invoice created: #{origin}=>#{inv_id}"
        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]
        puts "Posting invoice............."

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-check_amount).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{check_amount}"
            @logger.error msg
            raise msg
        end
        return inv_id
    end 
        
    def get_lines(order_no,job_row)
        q="
SELECT *
FROM PT_RODMST 
WHERE ORDER_NO='#{order_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            pp row
            tax_id=@erp.get_tax "OUT_VAT7_EXCL",@parent_company_id
            uom_id=@erp.get_uom "PCE",:required=>true
            deposit=job_row["DEPOSIT_AMOUNT"]
            amt=row["AMOUNT"]
            qty=row["ORDER_QTY"]
            price=amt/qty
            part_no=row["PART_NO"].strip
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true

            part_name=@dms.get_part_name part_no,:required=>true
            line_2={
                "name"=> part_name,
                "quantity"=>qty,
                "price_unit"=>price,
                "invoice_line_tax_id"=>[[6,0,[tax_id]]],
                "uos_id"=>uom_id,
                "account_id"=>@erp.get_account("CUST_INV_PART_DEPOSIT_INCOME",@parent_company_id,:required=>true)
            }
            lines<<line_2
        end
        return lines
    end
end    
