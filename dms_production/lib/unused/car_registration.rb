require "dms"
require "erp"
require "importer"
require "pp"

class Importer_CarRegistration < Importer
    def import_by_no(doc_no)
        puts "Importer_CarRegistration.import_by_no",doc_no
        q="SELECT * FROM ST_REGISTRATION WHERE REGSERIAL_NO='#{doc_no}'"
        res=@dms.get q
        raise "Car registration not found in dms: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_CarRegistration.import_by_date",date_from,date_to
        q="SELECT * FROM ST_REGISTRATION WHERE LAST_UPDATE_DATE>='#{date_from}' AND LAST_UPDATE_DATE<='#{date_to}'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CarRegistration.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_REGISTRATION WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        puts "Importer_CarRegistration.import_row"
        reg_serial=row["REGSERIAL_NO"].strip
        frame_no=row["FRAME_NO"].strip
        mto_cd=row["MTO_CD"].strip
        prodlot_id=@erp.get_prodlot frame_no,:required=>true
        product_id=@erp.get_product mto_cd,:required=>true

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,"customer",:required=>true
        address_id=@erp.get_partner_address(partner_id,:required=>true)

        vals = {
            "product_id"=> product_id,
            "customer_id"=> partner_id,
            "lot_id"=> prodlot_id,
            "name"=> reg_serial,
            }
        puts "vals..........."
        pp vals

        res=@erp.execute "ac.registration","search",[["name","=",reg_serial]]
        if not res.empty?
            owner_id=res[0]
            @erp.execute "ac.registration","write",owner_id,vals
            @logger.info "Car registration updated: #{reg_serial}"
        else
            @erp.execute "ac.registration","create",vals
            @logger.info "Car registration created: #{reg_serial}"
        end
    end
end
