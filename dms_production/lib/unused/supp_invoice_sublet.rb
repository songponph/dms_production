require "dms"
require "erp"
require "pp"
require "importer"

class Importer_SuppInvoiceSublet < Importer
    def import_by_no(doc_no)
        puts "Importer_SuppInvoiceSublet.import_by_no",doc_no
        q="SELECT * FROM RR_SUBLETJOB WHERE PO_NO='#{doc_no}' OR RO_NO='#{doc_no}'"
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_SuppInvoiceSublet.import_by_date",date_from,date_to
        q="SELECT * FROM RR_SUBLETJOB WHERE RECEIVE_DATE>='#{date_from} 00:00:00' AND RECEIVE_DATE <='#{date_to} 23:59:59' ORDER BY PO_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def check_import(date_from,date_to)
        puts "Check_import_supp_invoice_sublet",date_from,date_to
        q="SELECT PO_NO FROM RR_SUBLETJOB WHERE RECEIVE_DATE>='#{date_from} 00:00:00' AND RECEIVE_DATE<='#{date_to} 23:59:59' ORDER BY PO_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["PO_NO"].strip}
        pp dms_docnos
        puts "##############################################################"
        erp_docnos=@erp.get_invoice_docnos "in_invoice",date_from,date_to,"RP%",@company_id
        pp erp_docnos
        miss=check_missing dms_docnos,erp_docnos
        pp miss
        return miss
    end

    def import_daily(progress=nil)
        puts "Importer_SuppInvoiceSublet.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM RR_SUBLETJOB WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM RR_SUBLETJOB WHERE LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def import_row(row)
        puts "Importer_SuppInvoiceSublet.import_row"
        pp "row:",row
        po_no=row["PO_NO"].strip
        ro_no=row["RO_NO"].strip
        origin=po_no+" "+ro_no
        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created: #{origin}"
            return
        end

        partner_no=row["SUPPLIER_NO"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,"other_supplier",:required=>true
        address_id=@erp.get_partner_address partner_id,:type=>"invoice",:required=>true

        date=row["RECEIVE_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        sublet_frame=@dms.get_sublet_car_frame_no ro_no,:required=>true
        frame_no=sublet_frame["FRAME_NO"].strip
        model_nm=sublet_frame["MODEL_NM"].strip
        year=sublet_frame["YEAR"].to_s
        grade=sublet_frame["GRADE"].strip
        comment=year+" "+model_nm+" "+grade
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        inv_vals={
            "origin"=>origin,
            "reference"=>po_no,
            "comment"=>comment,
            "journal_id"=>@erp.get_journal("SUPP_INV_SUBLET_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>"in_invoice",
            "account_id"=>@erp.get_account("SUPP_INV_SUBLET_PAYABLE",@parent_company_id,:required=>true),
            "date_invoice"=>date,
            "vehicle_id"=> vehicle_id,
            "address_invoice_id" => address_id,
        }

        lines=get_lines(po_no)
        raise "No lines in invoice" if lines.empty?
        inv_vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "invoice created: #{origin}=>#{inv_id}"
        puts "Computing invoice..."
        set_total="set_total"
        @erp.execute "account.invoice","button_compute",[inv_id],{},true
        puts "Posting invoice..."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
    end

    def get_lines(po_no)
        q="
SELECT * FROM RR_SUBLETJOB_DETAIL WHERE PO_NO='#{po_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            seq_no=row["SEQ_NO"].to_s
            order_detail=row["ORDER_DETAIL"].strip
            price=row["COST"]
            account_id=@erp.get_account "SUPP_INV_SUBLET_EXPENSE",@parent_company_id,:required=>true
            uom_id=@erp.get_uom "PCE",:required=>true
            product_id=@erp.get_product "PSJ",:required=>true
            vals={
                "name"=>seq_no+" "+order_detail,
                "price_unit"=>price,
                "quantity"=>1,
                "account_id"=>account_id,
                "uos_id"=>uom_id,
                "product_id"=>product_id,
            }
            lines<<vals
            pp lines
        end
        lines
    end
end
