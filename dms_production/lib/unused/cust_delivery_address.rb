require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustDeliveryAddress < Importer
    def import_by_no(partner_no)
        puts "Importer_CustDeliveryAddress.import_by_no",partner_no
        q="SELECT * FROM SM_CUSTOMERMAIL WHERE CUSTOMER_NO='#{partner_no}'"
        res=@dms.get q
        raise "Customer not found: #{partner_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        puts "Importer_CustDeliveryAddress.import_by_date",date_from,date_to
        q="SELECT * FROM SM_CUSTOMERMAIL WHERE LAST_UPDATE_DATE>='#{date_from} 00:00:00' AND LAST_UPDATE_DATE<='#{date_to} 23:59:59'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustDeliveryAddress.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM SM_CUSTOMERMAIL WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        puts "Importer_CustDeliveryAddress.import_row"
        pp row
        partner_no=row["CUSTOMER_NO"].strip.upcase

        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        name=row["FIRST_NM"].strip+" "+row["LAST_NM"].strip
        addr_vals={
            "partner_id"=>partner_id,
            "name"=>name,
            "function"=>(row["SIR_NM"] or "").strip,
            "street"=>(row["DETAILS"] or "").strip,
            "street2"=>(row["TOWNCITY"] or "").strip,
            "zip"=>(row["ZIP_CD"] or "").strip,
            "country_id"=>@erp.get_country("TH",:required=>true),
            "city"=>(row["DISTRICT"] or "").strip+" "+(row["PROVINCE"] or "").strip,
            "phone"=>(row["TEL_NO"] or "").strip,
            "mobile"=>(row["CONT_MOBILE_NO"] or "").strip,
            "fax"=>(row["FAX_NO"] or "").strip,
            "type"=>"delivery",
        }
        res=@erp.execute "res.partner.address","search",[["partner_id","=",partner_id],["type","=","delivery"]]
        if res.empty?
            puts "creating new address"
            pp addr_vals
            addr_id=@erp.execute "res.partner.address","create",addr_vals
            @logger.info "CREATED : Partner delivery address: #{partner_no}=>#{addr_id}"
        end
    end
end
