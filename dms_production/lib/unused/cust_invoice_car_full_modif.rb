require "dms"
require "erp"
require "pp"
require "importer"

class Importer_InvoiceCarFull < Importer
    def import_by_no(invoice_no)
        puts "Importer_Invoice_Carfull.import_by_no",invoice_no
        q="SELECT * FROM ST_INVOICE WHERE INVOICE_NO='#{invoice_no}'"
        res=@dms.get q
        raise "Booking invoice not found: #{invoice_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_Invoice_Carfull.import_by_date",date_from,date_to
        q="SELECT * FROM ST_INVOICE WHERE INVOICE_NO LIKE 'SI%' AND TRAN_DATE>='#{date_from} 00:00:00' AND TRAN_DATE<='#{date_to} 23:59:59'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_Invoice_Carfull.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_INVOICE WHERE INVOICE_NO LIKE 'SI%' AND LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row) 
        puts "Importer_Invoice_Carfull.import_row"
        pp "row:",row
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        t0=Time.now()
        puts "Creating car full invoice...."
        invoice=row["INVOICE_NO"].strip
        if invoice[5,1] == '/'
            raise "Skip making invoice for this number...#{invoice}.."
        end

        booking_no=row["BOOKING_NO"].strip
        bill_no=row["BILL_NO"].strip
        down_pay=row["DOWN_PAY_CR"]
        total_cr=row["TOTAL_CR"]

        if not booking_no
            raise "Booking not found"
        else
            origin = invoice + " " + booking_no
        end

        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true


        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created:#{origin}"
            return
        end

        datetime=row["INV_PRINT_DATE"]
        date_invoice=datetime.strftime("%m/%d/%y")

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,"customer",:required=>true
        address_id=@erp.get_partner_address(partner_id,:required=>true)

        frame_no=row["FRAME_NO"]
        car_model=@dms.get_car_model frame_no
        if not car_model
            @logger.info "Product could not be found......"
            return
        end
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        model_no=car_model["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        res=@erp.execute "product.product","read",[product_id],["name"]
        model_name=res[0]["name"]
        color_name=car_model["COLOR_NM"].strip

        user_code=car_inv["STAFF_CD"].strip
        user_id=@erp.get_user user_code,:required=>true

        inv_val={
            "origin" => origin,
            "reference" => invoice,
            "journal_id" => @erp.get_journal("CUST_INV_CAR_FULL_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_invoice",
            "account_id" => @erp.get_account("CUST_INV_CAR_FULL_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "address_invoice_id" => address_id,
            "booking_id"=>booking_id,
            "user_id" => user_id,
            "vehicle_id" => vehicle_id,
        }
        lines=[]

        if bill_no!=nil
            inv_down=bill_no+" "+booking_no+" "+invoice
            deposit_invoice=@erp.get_origin_invoice inv_down,:required=>true
            deposit_line_={
                "deposit_id"=>deposit_invoice,
                "amount"=> down_pay,
            }

            inv_val["inv_deposit_lines"]=[[0,0,deposit_line_]]

        end

        if model_name[0,4]== "CITY"
            analytic_search = "CITY"
        elsif model_name[0,4] == "CR-V"
            analytic_search = "CRV"
        elsif model_name[0,4] == "JAZZ"
            analytic_search = "JAZZ"
        elsif model_name[0,4] == "BRIO"
            analytic_search = "BRIO"
        elsif model_name[0,5] == "CIVIC"
            analytic_search = "CIVIC"
        elsif model_name[0,6] == "ACCORD"
            analytic_search = "ACCORD"
        elsif model_name[0,5] == "FREED"
            analytic_search = "FREED"
        else
            raise "Not supported model name #{model_name}"  
        end
        analytic_name="SALE_" + analytic_search 
        analytic_account_id=@erp.get_analytic_account analytic_name,@company_id,:required=>true

        tax_id= @erp.get_tax("OUT_VAT7_INCL",@parent_company_id,:required=>true)

        line_2 = {
            "account_id" => @erp.get_account("CUST_INV_CAR_FULL_INCOME",@parent_company_id,:required=>true),
            "name" => model_name+" "+color_name,
            "product_id" => product_id, 
            "quantity" => 1,
            "account_analytic_id" => analytic_account_id,
            "invoice_line_tax_id" => [[6,0,[tax_id]]],
            "price_unit" => total_cr,
        }

        if bill_no != nil
            check_amount = total_cr - down_pay
        else
            check_amount = total_cr
        end

        lines<<line_2
        raise "no lines in invoice" if lines.empty?
        inv_val["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}

        inv_id=@erp.execute "account.invoice","create",inv_val
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]

        @logger.info "tax computed:"

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-check_amount).abs>0.25
            msg="wrong total in #{bill_no}: #{amt}/#{check_amount}"
            @logger.error msg
            raise msg
        end
        puts "Posting invoice............."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        @erp.execute "ac.booking","write",[booking_id],{"vehicle_id"=>vehicle_id}
        return inv_id
    end

end 
