require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoicePaysave < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND i.CONFIRM_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND i.CONFIRM_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( i.PACKAGE_NO='#{number}' or i.INVOICE_NO='#{number}' or i.FRAME_NO='#{number}') ": "")
        cause << (last_update!=false ? " AND i.CONFIRM_DATE>='#{last_update}' ": "")

        #TODO : import as Cash and be able to import as credit
        query="
            SELECT i.INVOICE_NO,
                   i.PACKAGE_NO as PRE_INVOICE_NO,
                   rtrim(rtrim(i.INVOICE_NO)+' '+rtrim(i.PACKAGE_NO)) AS ORIGIN,
                   i.CUSTOMER_NO,
                   i.INVOICE_DATE,
                   i.CONFIRM_DATE as PRE_INVOICE_DATE,
                   i.USER_ID,
                   i.FRAME_NO,
                   i.CAR_NM,
                   i.MODEL_YEAR,
                   i.REG_NO,
                   i.PACKAGE_NAME as INVOICE_DETAILS,
                   COALESCE(i.SIR_NM,'')+rtrim(i.FIRST_NM+COALESCE(i.MIDDLE_NM,''))+' '+i.LAST_NM AS NAME,
                   i.ADDRESS_NO+' '+i.VILLAGE+' '+i.MOO+ ' '+i.SOI + ' ' + i.ROAD  AS STREET,
                   i.TOWNCITY AS STREET2,
                   i.DISTRICT +' '+i.PROVINCE AS CITY,
                   i.TEL_NO AS PHONE,
                   i.MOBILE_NO AS MOBILE,
                   i.ZIP_CD AS ZIP,
                   i.GOODS_AMOUNT as AMOUNT_UNTAXED,
                   i.VAT_AMOUNT as AMOUNT_TAX,
                   i.CHARGE_AMOUNT as AMOUNT_TOTAL,
                   u.SECTION_CD
            FROM RT_PACKAGE i,
                 CM_STAFF u
            WHERE i.INVOICE_NO IS NOT NULL
              AND i.INVOICE_NO!=''
              AND i.USER_ID=u.USER_ID
        "+cause+"
        ORDER BY i.PACKAGE_NO"

        return query
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["ORIGIN"].strip}

        #check all invoice with that origin
        erp_docnos=@erp.get_invoice_docnos "",date_from,date_to,"KM%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end


    def import_row(row)
        invoice_id=self.create_invoice(row)
        #move_id=self.create_account_move(row)
        return invoice_id
    end

    def create_invoice(row)
        pp(row)
        pm_no=row["PRE_INVOICE_NO"].strip

        inv_no=row["INVOICE_NO"].strip
        origin=row["ORIGIN"].strip

        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "SKIPPED : Invoice already created: #{pm_no}"
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.create_address_invoice row

        date_invoice=row["PRE_INVOICE_DATE"].strftime("%Y-%m-%d")
        doc_date= (not row["INVOICE_DATE"].nil?) ? row["INVOICE_DATE"].strftime("%Y-%m-%d") : date_invoice

        reference=pm_no

        pmt_method = @dms.get_payment_method(pm_no,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id})

        tax_id=@erp.get_tax "OUT_VAT7_EXCL",:required=>true
        # NOW: KI never be credit

        if inv_no.empty?
            @logger.info "SKIPPED : No KI# => don't Import cash sale"
            return
        end

        reference=inv_no
        inv_type="out_cash"
        journal_code="CUST_INV_PAYSAVE_JOURNAL_CASH"

        frame_no=row["FRAME_NO"].strip

        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        staff_cd=row["USER_ID"].strip
        user_id=@erp.get_user staff_cd,@branch_code,:required=>true

        comment=""
        if row["CAR_NM"] != nil
            comment+=" "+row["CAR_NM"].strip
        end
        if row["MODELYEAR"] != nil
            comment+=" "+row["MODELYEAR"].to_s
        end
        if row["REG_NO"] != nil
            comment+= " "+row["REG_NO"].to_s
        end

        inv_vals={
            "origin"=>origin,
            "reference"=>reference,
            "comment"=> comment,
            "journal_id"=>@erp.get_journal(journal_code,@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>inv_type,
            "account_id" => @erp.get_account("CUST_INV_PAYSAVE_RECEIVABLE",@parent_company_id,:required=>true), #XXX add maping
            "date_invoice" => date_invoice,
            "doc_date"=> date_invoice,
            "user_id" => user_id,
            "address_invoice_id" => address_id,
            "vehicle_id" => vehicle_id,
        }

        amount_to_pay=row["AMOUNT_TOTAL"]

        #TODO: write get payment method not to copy for many transform

        if pmt_method.nil?
            @logger.info "SKIPPED : KI payment method missing"
            return
        end

        name=row["INVOICE_DETAILS"].strip
        analytic_id=@erp.get_analytic_account "PT_COUNTER",@company_id,:required=>true

        # income service/sale

        line_vals={
            "uos_id"=>@erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_INV_PAYSAVE_INCOME",@parent_company_id,:required=>true), #XXX: map this
            "name"=>name,
            "quantity"=>1,
            "price_unit"=>row["AMOUNT_UNTAXED"],
            "discount"=>0,
            "product_id" => false,
            "invoice_line_tax_id"=>[[6,0,[tax_id]]],
            "analytic_id"=>analytic_id,
        }

        inv_vals["invoice_line"]=[[0,0,line_vals]]

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "CREATED : Paysave invoice : #{inv_no}=>#{inv_id}"

        if inv_type=="out_cash"
            self.set_payment_method(inv_id,pmt_method)
        end

        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total","amount_tax"]

        # set manual vat
        if row["AMOUNT_TAX"] !=res[0]["amount_tax"]
            @erp.execute "account.invoice","set_manual_vat",[inv_id],false,row["AMOUNT_UNTAXED"],row["AMOUNT_TAX"],@company_id
        end

        amt=res[0]["amount_total"]
        check_total=row["AMOUNT_TOTAL"]
        if (amt-check_total).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{check_total}"
            @logger.error msg
            raise msg
        end
        #Credit sale not support yet
        #if inv_type=="out_invoice"
            #@erp.exec_workflow "account.invoice","invoice_open",inv_id
        #end
        return inv_id
    end
end
