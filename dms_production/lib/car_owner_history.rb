require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CarOwnerHistory < Importer

    def query_get(number,date_from=false,date_to=false)
        cause= date_from!=false ?  " AND LAST_UPDATE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND LAST_UPDATE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( CUSTOMER_NO='#{number}' or FRAME_NO='#{number}' ) ": "")

        #FIXME: check active_flg
        query="
            SELECT upper (CUSTOMER_NO) AS CUSTOMER_NO, START_DATE, FRAME_NO
              FROM ST_OWNERHISTORY
             WHERE     ACTIVE_FLG = 1 "+cause+"
             ORDER BY START_DATE"
        return query
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        rows=@dms.query q
        raise "Owner history found: #{doc_no}" if rows.empty?
        self.import_rows rows
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d"
        q=self.query_get(false,date_from=t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        partner_no=row["CUSTOMER_NO"].strip.upcase
        t0=Time.now
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        t1=Time.now
        start_date=row["START_DATE"].strftime "%Y-%m-%d"
        start_date="1900-01-01" if start_date<"1900-01-01"

        res=@erp.execute "ac.owner.history","search",[["vehicle_id","=",vehicle_id],["end_date","=",false]]
        if res.empty?
            @logger.info "Last owner not found => record new owner"
            vals={
                "vehicle_id"=>vehicle_id,
                "partner_id"=>partner_id,
                "start_date"=>start_date,
            }
            new_id=@erp.execute "ac.owner.history","create",vals
            @logger.info "New owner recorded: #{new_id}"
            return new_id
        end
        @logger.info "Last owner found"
        last_id=res[0]
        res=@erp.execute "ac.owner.history","read",[last_id],["partner_id","start_date","end_date"]
        last_vals=res[0]
        last_partner_id=last_vals["partner_id"][0]
        if partner_id==last_partner_id
            @logger.info "New owner is same as last owner (#{frame_no}) => don't record new owner"
            return
        end
        @logger.info "Trying to merge customers..."
        res=@erp.try_merge_partner partner_id,last_partner_id
        if res
            @logger.info "Customers merged (#{partner_id}->#{last_partner_id}) => don't record new owner"
            return
        end
        @logger.info "Merge failed, try to add new owner"
        if start_date<=last_vals["start_date"]
            @logger.info "New owner start date is older than previous owner start date (#{frame_no}) => don't record new owner"
            return
        end
        vals={
            "vehicle_id"=>vehicle_id,
            "partner_id"=>partner_id,
            "start_date"=>start_date,
        }
        new_id=@erp.execute "ac.owner.history","create",vals
        @erp.execute "ac.owner.history","write",[last_id],{"end_date"=>start_date}
        @logger.info "New owner recorded: #{new_id}, last owner updated: #{last_id}"
        new_id
    end
end
