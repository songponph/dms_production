require "dms"
require "erp"
require "pp"
require "importer"

class Importer_SuppInvoiceParts < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND INCOMING_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND INCOMING_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( FLOAT_NO='#{number}' or INVOICE_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND LAST_UPDATE_DATE>='#{last_update}' ": "")
        query="
            SELECT INVOICE_NO,FLOAT_NO,INCOMING_DATE AS DOC_DATE,LIST_DATE AS DATE_INVOICE,
            SUPPLIER_CODE as PARTNER_NO,
                TOTAL_AMOUNT,TAX_AMOUNT
            from PT_FLHMST
            where INCOMING_DATE is not null and INVOICE_NO not like 'PZ%'
            "+cause+"
            ORDER BY INCOMING_DATE,INVOICE_NO
        "
        return query
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q=self.query_get(false,t0)
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "in_invoice",date_from,date_to,"PF%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end 

    def import_row(row)
        inv_no=row["INVOICE_NO"].strip
        float_no=row["FLOAT_NO"].strip
        origin=float_no+" "+inv_no

        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "SKIPPED : Invoice already created: #{origin}"
            return
        end

        partner_no=row["PARTNER_NO"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        address_id=@erp.get_partner_address partner_id,:type=>"invoice",:required=>true

        if not address_id
            address_id=@erp.get_partner_address partner_id,:required=>true
        end

        date=row["DATE_INVOICE"].strftime "%Y-%m-%d %H:%M:%S"
        doc_date=row["DOC_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        inv_vals={
            "origin"=>origin,
            "reference"=>inv_no,
            "journal_id"=>@erp.get_journal("SUPP_INV_PARTS_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>"in_invoice",
            "account_id"=>@erp.get_account("SUPP_INV_PARTS_PAYABLE",@parent_company_id,:required=>true),
            "date_invoice"=>date,
            "doc_date"=>doc_date,
            "address_invoice_id"=>address_id,
            "check_total"=>row["TOTAL_AMOUNT"]+row["TAX_AMOUNT"],
        }

        lines=get_lines(float_no,row["TAX_AMOUNT"]==0)
        raise "No lines in invoice" if lines.empty?
        q=0
        lines.each do |line|
           q += line["quantity"]
        end
        if q==0
            @logger.info "SKIPPED : Qty is still zero,DMS transaction is not completed yet! => skipped"
            return
        end
        inv_vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}
        pp(inv_vals)

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "CREATED : invoice : #{inv_no}=>#{inv_id}"
        @erp.execute "account.invoice","button_compute",[inv_id],{},true
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        amt2=row["TOTAL_AMOUNT"]+row["TAX_AMOUNT"]
        if (amt-amt2).abs>0.25
            msg="wrong total in #{inv_no}: #{amt}/#{amt2}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
    end

    def get_lines(float_no,no_vat)
        q="
        SELECT
            upper(PART_NO) as PART_NO,LINE_NO,INCOMING_QTY,INCOMING_COST
        FROM PT_FLDMST WHERE INCOMING_QTY!=0.0 and FLOAT_NO='#{float_no}'
        "
        rows=@dms.query(q)
        lines=[]
        for row in rows
            part_no=row["PART_NO"].strip
            line_no=row["LINE_NO"].to_s
            qty=row["INCOMING_QTY"]
            #qty=0.0
            price=row["INCOMING_COST"]
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            account_id=@erp.get_account "SUPP_INV_PARTS_EXPENSE",@parent_company_id,:required=>true
            uom_id=@erp.get_uom "PCE",:required=>true
            vals={
                "name"=>float_no+" "+line_no,
                "price_unit"=>price,
                "product_id"=>product_id,
                "quantity"=>qty,
                "account_id"=>account_id,
                "uos_id"=>uom_id,
            }
            if not no_vat
                tax_id=@erp.get_tax "IN_VAT7_EXCL",:required=>true
                vals["invoice_line_tax_id"]=[[6,0,[tax_id]]]
            end
            lines<<vals
        end
        lines
    end
end
