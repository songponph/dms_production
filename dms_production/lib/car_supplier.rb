require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CarSupplier < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM SM_MAKER WHERE MAKER_CD='#{doc_no}' OR MAKER_NM='#{doc_no}'"
        res=@dms.get q
        raise "Supplier not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM SM_MAKER WHERE LAST_UPDATE_DATE>='#{date_from}' AND LAST_UPDATE_DATE<='#{date_to}'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM SM_MAKER WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM SM_MAKER WHERE LAST_UPDATE_DATE>='#{t0}' ORDER BY MAKER_CD"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        q="SELECT MAKER_CD FROM SM_MAKER WHERE LAST_UPDATE_DATE>='#{date_from} 00:00:00' AND LAST_UPDATE_DATE<='#{date_to} 23:59:59' ORDER BY LAST_UPDATE_DATE"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["MAKER_CD"].strip}
        missing=[]
        for docno in dms_docnos
            docno=docno.strip.upcase
            partner_id=@erp.get_partner docno,@company_id,@branch_code,:required=>true
            if not partner_id
                missing.push docno
            end
        end
        return missing
    end

    def import_row(row)
        pp row
        partner_no=row["MAKER_CD"].strip.upcase
        name=row["MAKER_NM"].strip

        res=@erp.execute "partner.branch.code","search",[["code","=",partner_no],["company_id","=",@company_id]]
        code_id=res[0]
        if code_id
            @logger.info "found branch code: #{partner_no}"
            res=@erp.execute "partner.branch.code","read",[code_id],["partner_id"]
            partner_id=res[0]["partner_id"][0]
        else
            @logger.info "branch code not found: #{partner_no}"
            res=@erp.execute "res.partner","search",[["name","=",name],["supplier","=",1]]
            if not res.empty?
                @logger.info "Found supplier with same name: #{name} => don't create new partner"
                partner_id=res[0]
            else
                @logger.info "Supplier with same name not found: #{name} => creating new partner"
                categ_id=@erp.get_partner_category "Car Suppliers",:required=>true
                supp_vals={
                    "name"=>name,
                    "customer"=>false,
                    "supplier"=>true,
                    "category_id"=>[[6,0,[categ_id]]],
                }
                partner_id=@erp.execute "res.partner","create",supp_vals
            end
        end

        addr_vals={
            "partner_id"=>partner_id,
            "type"=>"default",
            "name"=>name,
        }
        addr_id=@erp.get_partner_address partner_id,:type=>"default"
        if not addr_id
            puts "creating new address"
            addr_id = @erp.execute "res.partner.address","create",addr_vals
        end

        if not code_id
            puts "creating new branch code"
            code_vals={
                "partner_id"=>partner_id,
                "code"=>partner_no,
                "company_id"=>@company_id,
            }
            @erp.execute "partner.branch.code","create",code_vals
        end

        @logger.info "Supplier imported: #{partner_no}=>#{partner_id}"
        partner_id
    end
end
