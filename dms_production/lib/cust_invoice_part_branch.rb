require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoicePartBranch < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND I.INVOICE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND I.INVOICE_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( I.PROFORMA_INVOICE='#{number}' or I.INVOICE_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND I.LAST_UPDATE_DATE>='#{last_update}' ": "")
        query="
            SELECT I.INVOICE_NO,
                   I.PROFORMA_INVOICE,
                   P.DELIVERY_NO,
                   I.CUSTOMER_TYPE,
                   I.INVOICE_DATE,
                   I.CUSTOMER_NO,
                   I.AMOUNT,
                   I.SIR_NM,
                   I.FIRST_NM,
                   I.LAST_NM,
                   I.DETAILS_NM,
                   I.CITYTOWN_NM,
                   I.ZIP_CD,
                   I.DISTRICT_NM,
                   I.PROVINCE_NM,
                   I.TELEPHONE_NO,
                   I.FAX_NO
            FROM PT_INVOICE I,
                 PT_PKHMST P
            WHERE I.INVOICE_NO=P.INVOICE_NO
                AND I.INVOICE_NO LIKE 'PZ%' "+cause+"
            ORDER BY I.INVOICE_DATE,
                     I.INVOICE_NO "
        return query
    end

    def import_by_no(doc_no)
        puts "Importer_CustInvoicePartsBranch.import_by_no",doc_no
        q=self.query_get(doc_no)
        res=@dms.get(q)
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        puts "Importer_CustInvoicePartsBranch.import_by_date",date_from,date_to
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustInvoicePartsBranch.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM PT_INVOICE WHERE INVOICE_NO LIKE 'PZ%' AND LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
        return
    end

    def check_import(date_from,date_to)
        puts "Check_import_cust_invoice_part_branch",date_from,date_to
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        inv_no="PZ%"
        doc_nos=Array.new
        for row in rows
            doc_nos.push(row["INVOICE_NO"].strip)
        end
        miss=@erp.check_invoice_import_missing doc_nos,date_from,date_to,inv_no,@company_id
        return miss
    end

    def check_import(date_from,date_to)
        puts "Check_import_cust_invoice_part_branch",date_from,date_to
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_invoice",date_from,date_to,"PZ%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def get_payment_method_pz(inv_id,erp_con,options={})

        company_id = options["company_id"]
        parent_company_id = options["parent_company_id"]

        method="cash" # default type for payment method

        vals={}
        vals["method"]="cash"
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total",'date_invoice','reference']
        if not res
            return
        end
        cash_type = 'in'
        if options['invoice_type'] == "in_cash"
            cash_type = 'out'
        end
        vals["value"]={
            "account_id"=>erp_con.get_account("PZ_PMT_CASH",parent_company_id,:required=>true),
            "name" => res[0]['reference'],
            "amount" => res[0]['amount_total'],
            "date" => res[0]['date_invoice'],
            "type" => cash_type,
            "company_id" => company_id,
        }
        res << vals  # append values
        puts "invoice value#{res}"
        return [method,res]
        end

    def import_row(row)
        puts "Importer_CustInvoicePartsBranch.import_row"
        pp "row:",row
        pm_no=row["PROFORMA_INVOICE"].strip
        inv_no=row["INVOICE_NO"].strip
        puts "making list of pickings"
        order_nos=@dms.get_part_delivery_nos inv_no #
        pp order_nos
        #
        order_no=@dms.get_part_order_no inv_no #
        pick_no = order_nos.map! { |k| "#{k}" }.join(" ")
        origin=pm_no+" "+inv_no+" "+pick_no

        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "Invoice already created: #{pm_no}"
            return res[0]
        end

        partner_no=row["CUSTOMER_NO"].strip
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address partner_id,:required=>true

        date=row["INVOICE_DATE"].strftime("%Y-%m-%d")

        inv_type = "out_cash"

        inv_vals={
            "origin"=>origin,
            "reference"=>inv_no,
            "journal_id"=>@erp.get_journal("CUST_INV_PART_BRANCH_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>inv_type,
            "account_id" => @erp.get_account("CUST_INV_PART_BRANCH_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date,
            "doc_date" => date,
            "address_invoice_id" => address_id,
        }

        lines=[]
        for order_no in order_nos
            lines+=get_lines(order_no)
        end
        raise "No lines in invoice" if lines.empty?
        inv_vals["invoice_line"]=lines.collect {|vals_| [0,0,vals_]}

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "invoice created: #{inv_no}=>#{inv_id}"

        pmt_method = self.get_payment_method_pz(inv_id,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id,"invoice_type"=>inv_type})

        puts pmt_method

        # Add payment method
        self.set_payment_method(inv_id,pmt_method)

        puts "Computing invoice..."
        @erp.execute "account.invoice","button_compute",[inv_id]
        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["AMOUNT"]).abs>0.25
            msg="wrong total in #{inv_no}: #{amt}/#{row['AMOUNT']}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end

    def get_lines(order_no)

        #same Query as cust_invoice_part
        #TODO: make shared function
        pick_no = order_no
        q="
            SELECT T3.PART_TYPE,
                   T3.PART_NAME_L AS NAME,
                   T1.order_line_no AS LINE_NO,
                   T1.PART_NO,
                   T1.DELIVERY_QTY AS PRODUCT_QTY,
                   round((T2.AMOUNT) / (T1.DELIVERY_QTY), 2) AS UNIT_PRICE,
                   CASE
                       WHEN (T2.DISCOUNT_RATE!=0) THEN ((T2.UNIT_PRICE*(T1.DELIVERY_QTY))-T1.AMOUNT)/(T1.DELIVERY_QTY)
                       ELSE NULL
                   END AS DISCOUNT
            FROM PT_PKDMST T1 ,
                 PT_RODMST T2,
                 PM_SPMAST T3
            WHERE T1.PART_NO=T2.PART_NO
                AND T3.PART_NO=T1.PART_NO
                AND T1.ORDER_NO=T2.ORDER_NO
                AND T1.ORDER_LINE_NO=T2.LINE_NO
                AND T1.delivery_no='#{pick_no}'
            ORDER BY T1.ORDER_LINE_NO
        "
        rows=@dms.query(q)
        lines=[]
        for row in rows
            account_id=@erp.get_account "CUST_INV_PART_BRANCH_INCOME",@parent_company_id,:required=>true
            analytic_id=@erp.get_analytic_account "PT_DEALER",@company_id,:required=>true
            uom_id=@erp.get_uom "PCE",:required=>true
            qty=row["PRODUCT_QTY"]
            price=row["UNIT_PRICE"]
            part_no=row["PART_NO"].strip
            part_no=row["PART_NO"].strip
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true

            part_name=@dms.get_part_name part_no,:required=>true
            vals={
                "name"=> part_name,
                "quantity"=>qty,
                "price_unit"=>price,
                "uos_id"=>uom_id,
                "account_id"=>account_id,
                "product_id"=>product_id,
                "account_analytice_id"=>analytic_id,
                "note"=>order_no
            }
            lines<<vals
        end
        lines
   end
end
