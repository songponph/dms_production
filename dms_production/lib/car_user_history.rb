require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CarUserHistory < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM ST_USERHISTORY WHERE CUSTOMER_NO='#{doc_no}' OR FRAME_NO='#{doc_no}' ORDER BY START_DATE"
        rows=@dms.query q
        raise "User history found: #{doc_no}" if rows.empty?
        self.import_rows rows
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM ST_USERHISTORY WHERE LAST_UPDATE_DATE>='#{date_from} 00:00:00' AND LAST_UPDATE_DATE<='#{date_to} 23:59:59' ORDER BY START_DATE"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_USERHISTORY WHERE LAST_UPDATE_DATE>='#{t0}' ORDER BY START_DATE"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_row(row)
        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        start_date=row["START_DATE"].strftime "%Y-%m-%d"
        start_date="1900-01-01" if start_date<"1900-01-01"

        # find last user of the car in ERP
        res=@erp.execute "ac.usage.history","search",[["vehicle_id","=",vehicle_id],["end_date","=",false]]
        if res.empty? # no user found => create new user history
            vals={
                "vehicle_id"=>vehicle_id,
                "partner_id"=>partner_id,
                "start_date"=>start_date,
            }
            new_id=@erp.execute "ac.usage.history","create",vals
            return new_id
        end
        # last user found 
        last_id=res[0]
        res=@erp.execute "ac.usage.history","read",[last_id],["partner_id","start_date","end_date"]
        last_vals=res[0]
        last_partner_id=last_vals["partner_id"][0]
        if partner_id==last_partner_id
            @logger.info "New user is same as last user => don't import"
            return
        end
        # try to merge users
        res=@erp.try_merge_partner partner_id,last_partner_id
        if res
            @logger.info "Customers merged (#{partner_id}->#{last_partner_id}) => don't record new user"
            return
        end
        @logger.info "Merge failed, try to add new user"
        if start_date<=last_vals["start_date"]
            @logger.info "New user start date is older than previous user start date => don't record new user"
            return
        end
        vals={
            "vehicle_id"=>vehicle_id,
            "partner_id"=>partner_id,
            "start_date"=>start_date,
        }
        new_id=@erp.execute "ac.usage.history","create",vals
        @erp.execute "ac.usage.history","write",[last_id],{"end_date"=>start_date}
        @logger.info "New user recorded: #{new_id}, last user updated: #{last_id}"
    end
end
