require "dms"
require "erp"
require "pp"
require "importer"

class Importer_CustInvoicePartDeposit < Importer
    def import_by_no(doc_no)
        puts "Importer_CustInvoicePartsDeposit.import_by_no",doc_no
        q="SELECT * FROM PT_ROHMST WHERE INVOICE_NO='#{doc_no}'"
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        puts "Importer_CustInvoicePartsDeposit.import_by_date",date_from,date_to
        q="SELECT * FROM PT_ROHMST WHERE ORDER_DATE>='#{date_from}' AND ORDER_DATE<='#{date_to}' AND INVOICE_NO LIKE 'PI%' ORDER BY INVOICE_NO"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        puts "Importer_CustInvoicePartsDeposit.import_daily"
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PT_ROHMST WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'PI%'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM PT_ROHMST WHERE LAST_UPDATE_DATE>='#{t0}' AND INVOICE_NO LIKE 'PI%'"
        #rows=@dms.query q
        #self.import_rows rows,progress
        return
    end

    def check_import(date_from,date_to)
        puts "Check_import_cust_invoice_part_deposit",date_from,date_to
        q="SELECT INVOICE_NO FROM PT_ROHMST WHERE INVOICE_NO LIKE 'PI%' AND ORDER_DATE>='#{date_from} 00:00:00' AND ORDER_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_deposit",date_from,date_to,"PI%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end 

    def import_row(row)
        puts "Importer_CustInvoicePartsDeposit.import_row"
        pp "row:",row
        inv_no=row["INVOICE_NO"].strip
        origin=inv_no
        res=@erp.execute "account.invoice","search",[["origin","=",origin],["company_id","=",@company_id]]
        if not res.empty?
            @logger.info "SKIPPED : Invoice already created: #{origin}"
            return
        end

        partner_no=row["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.create_address_invoice_part row
        date=row["ORDER_DATE"].strftime("%m-%d-%y")
        inv_type="out_deposit"
        inv_vals={
            "origin"=>origin,
            "reference"=>inv_no,
            "journal_id"=>@erp.get_journal("CUST_INV_PART_DEPOSIT_JOURNAL",@company_id,:required=>true),
            "partner_id"=>partner_id,
            "company_id"=>@company_id,
            "type"=>inv_type,
            "account_id"=>@erp.get_account("CUST_INV_PART_DEPOSIT_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice"=>date,
            "doc_date"=>date,
            "address_invoice_id"=>address_id,
        }


        deposit=row["DEPOSIT_AMOUNT"]
        check_amount=deposit + row["DEPOSIT_TAX_AMOUNT"]
        order_no=row["ORDER_NO"].strip

        pmt_method = @dms.get_payment_method(order_no.strip,@erp,{"company_id"=>@company_id,"parent_company_id"=>@parent_company_id})

        uom_id=@erp.get_uom "PCE",:required=>true
        tax_id=@erp.get_tax "OUT_VAT7_EXCL",@parent_company_id
        line_1={
            "name"=> "Deposit for "+order_no,
            "quantity"=>1,
            "price_unit"=>deposit,
            "uos_id"=>uom_id,
            "account_id"=>@erp.get_account("CUST_INV_PART_DEPOSIT",@parent_company_id,:required=>true),
            "invoice_line_tax_id"=>[[6,0,[tax_id]]],
           }
        inv_vals["invoice_line"]=[[0,0,line_1]]

        inv_id=@erp.execute "account.invoice","create",inv_vals
        @logger.info "CREATED : invoice created: #{origin}=>#{inv_id}"

        self.set_payment_method(inv_id,pmt_method)

        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-check_amount).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{check_amount}"
            @logger.error msg
            raise msg
        end
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
