require "dms"
require "erp"
require "importer"
#require "customer"
require "partner"
require "partner_tax_info"
require "sale_user"
#require "cust_invoice_address"
#require "cust_delivery_address"
#require "car_owner_history"
#require "car_user_history"
#require "car_registration"
require "car_product"
require "job_product"
require "part_product"
require "vehicle"
require "booking"
require "cust_invoice_service"
require "parts_used_jobs"
require "parts_delivered_customer_service"
require "parts_returned_jobs"
require "cars_received_supplier"
require "cars_received_branch"
require "cars_delivered_customer"
require "cars_delivered_branch"
require "cars_delivered_dealer"
require "cars_returned_customer"
require "cars_returned_supplier"
require "cars_returned_branch"
require "cars_returned_dealer"
require "red_plate"
require "red_plate_stock"
require "supp_invoice_car"
require "supp_credit_car"
require "cust_invoice_parts"
require "cust_credit_parts"
require "cust_invoice_ult"
require "cust_credit_ult"
require "cust_invoice_paysave"
require "cust_credit_paysave"
require "cust_invoice_car_booking"
require "cust_credit_car_booking"
require "cust_invoice_car_down"
require "cust_credit_car_down"
require "cust_invoice_car_dealer"
require "cust_credit_car_dealer"
require "cust_invoice_car_full"
require "cust_credit_car_full"
#require "cust_invoice_car_branch"
#require "cust_credit_car_branch"
require "cust_invoice_claim_free_service"
require "cust_invoice_pm"
require "cust_credit_pm"
require "cust_invoice_mi"
require "cust_credit_mi"
require "cust_credit_service"
require "cust_credit_part_deposit"
require "cust_invoice_redplate"
require "supp_invoice_parts"
require "supp_invoice_part_branch"
#require "supp_invoice_sublet"
require "supp_invoice_redplate"
require "allocation"
require "cust_invoice_part_branch"
require "cust_invoice_part_deposit"
#require "cust_invoice_part_remain"
require "cust_credit_parts_branch"
require "parts_received_suppliers"
require "parts_received_other_branch"
require "parts_delivered_customer_counter"
require "parts_delivered_branch"
require "parts_returned_branch"
require "parts_returned_customer_service"
require "parts_returned_supplier"
require "part_loss"
require "part_excess"
#require "dealer"
#require "part_supplier"
#require "car_supplier"
#require "other_supplier"
require "sublets_received_purchase"

module DMS_OpenERP
    @@all_branches=[
        "R3N",
        #"R3U",
        "YR",
        "BM",
        "SP",
        "DHA",
        "SW",
        "PHC",
    ]

    @@all_importer_classes=[
        #["customer",Importer_Customer],
        ["partner",Importer_Partner],
        ["partner_tax_info",Importer_Partner_Tax_Info],
        #["cust_invoice_address",Importer_CustInvoiceAddress],
        #["cust_delivery_address",Importer_CustDeliveryAddress],
        ["vehicle",Importer_Vehicle],
        #["car_owner_history",Importer_CarOwnerHistory],
        #["car_user_history",Importer_CarUserHistory],
        #["car_registration",Importer_CarRegistration],
        #["dealer",Importer_Dealer],
        #["car_supplier",Importer_CarSupplier],
        #["part_supplier",Importer_PartSupplier],
        #["other_supplier",Importer_OtherSupplier],
        ["sale_user",Importer_SaleUser],
        ["car_product",Importer_CarProduct],
        ["part_product",Importer_PartProduct],
        ["job_product",Importer_JobProduct],
        ["booking",Importer_Booking],
        #update booking
        ["allocation",Importer_Allocation],
        # customer invoice
        ["cust_invoice_car_booking",Importer_CustInvoiceCarBooking],
        ["cust_credit_car_booking",Importer_CreditCarBooking],

        ["cust_invoice_car_down",Importer_CustInvoiceCarDown],
        ["cust_credit_car_down",Importer_CustCreditCarDown],

        ["cust_invoice_redplate",Importer_CustInvoiceRedplate],

        ["cust_invoice_car_full",Importer_InvoiceCarFull],
        ["cust_credit_car_full",Importer_CustCreditCarFull], 

        ["cust_invoice_service",Importer_CustInvoiceService],
        ["cust_invoice_claim_free_service",Importer_CustInvoiceClaimFreeService],
        ["cust_invoice_pm",Importer_CustInvoicePreventiveMaintenance],
        ["cust_credit_pm",Importer_CustCreditNotesPreventiveMaintenance],
        ["cust_invoice_mi",Importer_CustInvoiceMI],
        ["cust_credit_mi",Importer_CustCreditNotesMI],
        ["cust_invoice_parts",Importer_CustInvoiceParts],
        ["cust_invoice_part_branch",Importer_CustInvoicePartBranch],
        ["cust_invoice_part_deposit",Importer_CustInvoicePartDeposit],
        #["cust_invoice_part_remain",Importer_CustInvoicePartRemain],
        ["cust_invoice_ult",Importer_CustInvoiceUlt],
        ["cust_invoice_paysave",Importer_CustInvoicePaysave],
        ["cust_credit_paysave",Importer_CustCreditPaysave],
        #["cust_invoice_car_branch",Importer_CustInvoiceCarBranch],
        ["cust_invoice_car_dealer",Importer_CustInvoiceCarDealer],
        # customer credit note
        ["cust_credit_parts",Importer_CustCreditParts],
        ["cust_credit_parts_branch",Importer_CustCreditPartsBranch],
        ["cust_credit_ult", Importer_CustCreditUltimate],
        ["cust_credit_part_deposit",Importer_CustCreditPartDeposit], 
        ["cust_credit_service",Importer_CustCreditService],
        #["cust_credit_car_branch",Importer_CreditCarBranch],#entry make by stock
        ["cust_credit_car_dealer",Importer_CreditCarDealer],
        # supplier invoice
        ["supp_invoice_car",Importer_SuppInvoiceCar],
        ["supp_invoice_parts",Importer_SuppInvoiceParts],
        ["supp_invoice_part_branch",Importer_SuppInvoicePartBranch],
        #["supp_invoice_sublet",Importer_SuppInvoiceSublet],
        ["supp_invoice_redplate",Importer_SuppInvoiceRedplate],
        # Supplier Credit note
        ["supp_credit_car",Importer_SuppCreditCar],
        # stock of cars
        ["cars_received_supplier",Importer_CarsReceivedSupplier],
        ["cars_delivered_customer",Importer_CarsDeliveredCustomer],
        ["cars_delivered_branch",Importer_CarsDeliveredBranch],
        ["cars_delivered_dealer",Importer_CarsDeliveredDealers],
        ["cars_returned_dealer",Importer_CarsReturnedDealer],
        ["cars_returned_customer",Importer_CarsReturnedCustomer],
        ["cars_returned_supplier",Importer_CarsReturnedSupplier],
        ["cars_received_branch",Importer_CarsReceivedBranch],
        ["cars_returned_branch",Importer_CarsReturnedBranch],
        # stock of parts
        ["parts_used_jobs",Importer_PartsUsedJobs],
        ["parts_delivered_customer_service",Importer_PartsDeliveredCustomerService],
        ["parts_delivered_branch", Importer_PartsDeliveredBranch],
        ["parts_delivered_customer_counter",Importer_PartsDeliveredCustomerCounter],
        ["parts_returned_jobs",Importer_PartsReturnedJobs],
        ["parts_returned_branch",Importer_PartsReturnedBranch],
        ["parts_returned_customer_service",Importer_PartsReturnedCustomerService],
        ["parts_returned_supplier",Importer_PartsReturnedSupplier],
        ["parts_received_other_branch",Importer_PartsReceivedOtherBranch],
        ["parts_received_suppliers",Importer_PartsReceivedSupplier],
        ["part_loss",Importer_PartsLoss],
        ["part_excess",Importer_PartsExcess],
        # other
        ["sublets_received_purchase",Importer_SubletReceivedPurchase],
        ["red_plate",Importer_RedPlate],
        ["red_plate_stock",Importer_RedPlateStock],
    ]

    @@logger=nil

    def self.get_all_branches
        @@all_branches
    end

    def self.get_all_doc_types
        @@all_importer_classes.collect {|x| x[0]}
    end

    def self.get_importer(doc_type_)
        importer_cls_=nil
        for doc_type,importer_cls in @@all_importer_classes
            if doc_type==doc_type_
                importer_cls_=importer_cls
                break
            end
        end
        raise "Invalid document type: #{doc_type_}" if not importer_cls_
        importer_cls_.new
    end

    def self.get_logger
        @@logger=Logger.new("/tmp/dms_openerp.log") if not @@logger
        @@logger
    end

    def self.record_missing(doc_type,doc_no)
        open("/tmp/missing_#{doc_type}.log","a") do |f|
            f.puts doc_no
        end
    end
end
