require "dms"
require "erp"
require "pp"
require "importer"

class Importer_SuppInvoiceRedplate < Importer

    def import_by_no(doc_no)
        q="SELECT * FROM ST_RENTAL WHERE SHEET_NO='#{doc_no}' OR BOOKING_NO='#{doc_no}'"
        res=@dms.get q
        raise "Booking invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        q="SELECT * FROM ST_RENTAL WHERE RENTAL_DATE>='#{date_from} 00:00:00' AND RENTAL_DATE<='#{date_to} 23:59:59' ORDER BY SHEET_NO,RENTAL_DATE"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{t0}'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q="SELECT * FROM ST_RENTAL WHERE LAST_UPDATE_DATE>='#{t0}'"
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def check_import(date_from,date_to)
        q="SELECT SHEET_NO FROM ST_RENTAL WHERE RENTAL_DATE>='#{date_from} 00:00:00' AND RENTAL_DATE<='#{date_to} 23:59:59' ORDER BY SHEET_NO"
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["SHEET_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "in_invoice",date_from,date_to,"S7%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        t0=Time.now()
        inv_no=row["SHEET_NO"].strip
        booking_no=row["BOOKING_NO"].strip
        if inv_no.empty?
            @logger.info "No Redplate invoice no........"
            return
        end

        car_inv=@dms.get_car_invoice_from_booking booking_no
        origin=car_inv["INVOICE_NO"].strip+" "+booking_no+" "+inv_no
        res=@erp.execute "account.invoice","search",[["origin","=",origin],["type","=","in_invoice"]]
        if not res.empty?
            @logger.info "SKIPPED : Invoice already created:#{origin}"
            return
        end

        partner_id=@erp.get_partner "supp_tpl",@company_id,nil,:nocompany=>true
        address_id=@erp.get_partner_address(partner_id,:required=>true)

        booking_id=@erp.get_booking booking_no,@branch_code,:required=>true
        frame_no=row["FRAME_NO"].strip
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        car_invoice=@dms.get_car_invoice_from_booking booking_no
        car_invoice_no=car_invoice["INVOICE_NO"].strip
        prod_name=row["TLP_NO"].strip
        invoice_name=car_invoice_no+" "+prod_name
        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,@branch_code
        date_invoice=row["RENTAL_DATE"].strftime("%m/%d/%y")

        vals_={
            "origin" => origin,
            "reference" => inv_no,
            "name"=>invoice_name,
            "journal_id" => @erp.get_journal("SUPP_INV_REDPLATE_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "in_invoice",
            "account_id" => @erp.get_account("SUPP_INV_REDPLATE_PAYABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "booking_id" => booking_id,
            "vehicle_id" => vehicle_id,
            "user_id"=>user_id,
            "address_invoice_id" => address_id
        }

        product_id=@erp.get_product "RED_PLATE",:required=>true
        price_unit=row["DEPOSIT_CR"]
        lines_={
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("SUPP_INV_REDPLATE_EXPENSE",@parent_company_id,:required=>true),
            "name" => invoice_name,
            "product_id" => product_id,
            "quantity" => 1,
            "price_unit" => price_unit,
            }

        vals_["invoice_line"]=[[0,0,lines_]]

        inv_id=@erp.execute "account.invoice","create",vals_
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        @erp.execute "account.invoice","button_compute",[inv_id],{},true

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-price_unit).abs>0.25
            msg="wrong total in #{inv_no}: #{amt}/#{price_unit}"
            @logger.error msg
            raise msg
        end
        #NOTE:novalidate:@erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
