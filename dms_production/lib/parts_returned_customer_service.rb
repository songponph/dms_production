require "dms"
require "erp"
require "importer"
require "pp"

class Importer_PartsReturnedCustomerService < Importer
    def import_by_no(doc_no)
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_NO ='#{doc_no}'"
        res=@dms.get q
        raise "Job order not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE IO_DATE >='#{date_from} 00:00:00' AND IO_DATE <='#{date_to} 23:59:59' AND IO_NO LIKE 'PT%'"
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q="SELECT * FROM PT_RETURNINVOICE_H WHERE LAST_UPDATE_DATE>='#{t0}' AND IO_NO LIKE 'PT%'"
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_parts_returned_customer_service",date_from,date_to
        q="SELECT INVOICE_NO,IO_NO FROM PT_RETURNINVOICE_H WHERE IO_NO LIKE 'PT%' AND IO_DATE>='#{date_from} 00:00:00' AND IO_DATE<='#{date_to} 23:59:59' ORDER BY INVOICE_NO"
        rows=@dms.query q
        dms_docnos=Array.new
        inv_no="PT%"
        for row in rows
            invoice=row["IO_NO"].strip+" "+row["INVOICE_NO"].strip
            dms_docnos.push(invoice)
        end
        pp dms_docnos
        puts "################################################"
        erp_docnos=@erp.get_picking_docnos inv_no,"in",date_from,date_to,@company_id
        pp erp_docnos
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end 

    def import_row(row)
        io_no=row["IO_NO"].strip
        inv_no=(row["INVOICE_NO"] or "").strip
        origin=io_no+" "+inv_no

        res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        if not res.empty?
            @logger.info "Picking already created: #{origin}"
            return
        end
        date_done=row["DELIVERED_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        date=row["IO_DATE"].strftime "%Y-%m-%d %H:%M:%S"
        customer=@dms.get_parts_customer(inv_no)
        partner_no=customer["CUSTOMER_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address partner_id,:type=>"invoice",:required=>true 

        pick_vals={
            "origin"=>origin,
            "date"=>date,
            "date_done"=>date_done,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>"in",
            "address_id"=>address_id,
            "stock_journal_id"=>@erp.get_stock_journal("PARTS_RETURNED_CUSTOMER_SERVICE_JOURNAL",@company_id), #PART_PT
        }

        lines=get_lines(io_no,row)
        pick_vals["move_lines"]=lines.collect {|line_vals| [0,0,line_vals]}

        pp "vals",pick_vals
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "Created picking #{io_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end

    def get_lines(io_no,job_row)
        q="
SELECT *
 FROM PT_RETURNINVOICE_D
 WHERE IO_NO='#{io_no}'
"
        rows=@dms.query(q)
        lines=[]
        for row in rows
            pp "line_row",row
            part_no=row["PART_NO"].strip
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            date=job_row["IO_DATE"].strftime "%Y-%m-%d %H:%M:%S"
            line_no=row["LINE_NO"].to_s
            qty=row["RETURN_INPUT_QTY"]
            vals={
                "name"=>io_no+" "+line_no,
                "date"=>date,
                "date_expected"=>date,
                "product_id"=>product_id,
                "product_uom"=>@erp.get_uom("PCE",:required=>true),
                "product_qty"=>qty,
                "location_id"=>@erp.get_location("Customers",:required=>true),
                "location_dest_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
                "state"=>"assigned",
                "price_unit"=>row["COST_PRICE"],
            }
            lines<<vals
        end
        lines
    end
end
