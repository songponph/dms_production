require "dms"
require "erp"
require "importer"
require "pp"
#in_return
#SUPPLIER_KBN : 02 Dealer
#               12 Branch
#               01 Honda

#TODO:merge query : query_get_supp_inv_car
class Importer_CarsReturnedSupplier < Importer

    def query(number,date_from=false,date_to=false,last_update=false)

        cause= date_from!=false ?  " AND LAST_UPDATE_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND LAST_UPDATE_DATE<='#{date_to} 23:59:59' ": "")

        cause_ht = (number!=false ?  " AND ( DISTCN_NO='#{number}' or FRAME_NO='#{number}' ) ": "")
        cause_ro = (number!=false ?  " AND ( INVOICE_CN_NO='#{number}' or FRAME_NO='#{number}' ) ": "")

        query_ht="SELECT
            case when SUPPLIER_KBN='01' then 'honda'
                when SUPPLIER_KBN='02' then 'dealer'
                when SUPPLIER_KBN='12' then 'branch'
                else null end as TYPE,
            upper(DISTCN_NO) as INVOICE_NO,
            TRUE_RECEIVING_NO as PICK_NO,
            SUPPLIER_CD as PARTNER_NO,
            RECEIVING_CANCEL_DATE AS DATE_DONE,
            FRAME_NO,
            MTOC_CD,
            RECEIVING_CR as PRICE_UNIT
            FROM ST_PURCHASEHISTORY
            WHERE ACTIVE_FLG=0
            #{cause} #{cause_ht}
         order by DISTCN_NO"

        query_ro="
            SELECT
            case
                when SUPPLIER_KBN='01' then 'honda'
                when SUPPLIER_KBN='02' then 'dealer'
                when SUPPLIER_KBN='12' then 'branch'
                else null end as TYPE,
            upper(INVOICE_CN_NO) as INVOICE_NO,
            INVOICE_NO as PICK_NO,
            SUPPLIER_NO as PARTNER_NO,
            REGISTRATION_DATE as DATE_DONE,
            FRAME_NO,
            MTOC_CD,
            SALES_CR as PRICE_UNIT
                FROM ST_RORDERINV
                WHERE ACTIVE_FLG=0
            #{cause} #{cause_ro}
            order by INVOICE_CN_NO
        "
        rows=[]
        #sqls=[query_ht,query_ro]
        sqls=[query_ht]
        for sql in sqls
            rows+=@dms.query(sql)
        end

        return rows
    end

    def import_by_no(doc_no)
        res=self.query(doc_no)
        raise "Purchase Return not found: #{doc_no}" if not res
        self.import_rows(res)
    end

    def import_by_date(date_from,date_to)
        res =self.query(false,date_from,date_to)
        self.import_rows res
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d"
        res=self.query(false,t0)
        self.import_rows res,progress
    end

    def check_import(date_from,date_to)
        puts "Check_import_cars_returned_supplier",date_from,date_to
        rows=self.query(false,date_from,date_to)
        #rows=@dms.query q
        miss=Array.new
        for row in rows
            pick_no=row["PICK_NO"].strip
            inv_no=row["INVOICE_NO"].strip
            origin=inv_no+" "+pick_no

            pick_id=@erp.get_picking_id pick_no,"out",@company_id

            if not pick_id
                pick_id=@erp.get_picking_id,origin,"out",@company_id
            end
            if not pick_id
                miss.push(invoice)
            end
        end
        puts "################################################"
        return miss
    end

    def import_row(row)
        pick_no=row["PICK_NO"].strip
        inv_no=row["INVOICE_NO"].strip
        if inv_no.empty?
            return
        end
        origin=inv_no+" "+pick_no

        res=@erp.execute "stock.picking","search",[["origin","=",pick_no],['company_id','=',@company_id]]
        if res.empty?
            res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
        end

        if not res.empty?
            @logger.info "UPDATED: Picking already created: #{origin}"
            #@erp.update_picking(res,inv_no,@company_id)
            return
        end

        partner_no=row["PARTNER_NO"].strip
        date_done=row["DATE_DONE"].strftime "%Y-%m-%d %H:%M:%S"
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true
        address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)
        if not address_id
            address_id=@erp.get_partner_address(partner_id,:required=>true)
        end

        type="out"
        location_dest_id=@erp.get_location("Suppliers",:required=>true)

        if row["TYPE"]=='branch'
            type="internal"
            location_dest_id= @erp.get_location(@branch_code+"-Transfer",:required=>true)
        end

        pick_vals={
            "origin"=>origin,
            "date"=>date_done,
            "date_done"=>date_done,
            "move_type"=>"direct",
            "company_id"=>@company_id,
            "invoice_state"=>"none",
            "type"=>type,
            "address_id"=>address_id,
            "stock_journal_id"=>@erp.get_stock_journal("CARS_RETURNED_DEALER_JOURNAL",@company_id), #CAR_S2_SP
        }

        move_name=pick_no+" "+partner_no
        frame_no=row["FRAME_NO"].strip
        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true
        prodlot_id=@erp.get_prodlot frame_no
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true
        line_vals={
            "name"=>frame_no,
            "date_expected"=>date_done,
            "date"=>date_done,
            "product_id"=>product_id,
            "product_uom"=>@erp.get_uom("PCE",:required=>true),
            "product_qty"=>1,
            "location_id"=>@erp.get_location(@branch_code+"-Stock",:required=>true),
            "location_dest_id"=>location_dest_id,
            "state"=>"assigned",
            "prodlot_id"=>prodlot_id,
            "vehicle_id"=>vehicle_id,
            "price_unit"=>row["PRICE_UNIT"]
        }
        pick_vals["move_lines"]=[[0,0,line_vals]]
        pp(pick_vals)
        pick_id=@erp.execute "stock.picking","create",pick_vals
        @logger.info "CREATED: picking #{pick_no}=>#{pick_id}"
        @erp.exec_workflow "stock.picking","button_confirm",pick_id
        @erp.exec_workflow "stock.picking","button_done",pick_id
        pick_id
    end
end
