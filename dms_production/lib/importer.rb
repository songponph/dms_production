require "logger"

class Importer
    def start_import(params)
        puts "Importer.start_import"
        pp params
        @dms=DMS.get_connection params["branch_code"]
        @erp=ERP.get_connection
        pp "ERP connected"
        @branch_code=params["branch_code"]
        @no_update=params["no_update"]
        @company_id=@erp.get_company @branch_code,:required=>true
        @parent_company_id=@erp.get_parent_company @company_id,:required=>true
        @logger=DMS_OpenERP.get_logger
    end

    def import_realtime(from_t,progress=nil)
    end

    def import_rows(rows,progress=nil)
        rows.each_with_index do |row,i|
            begin
                if progress and i%10==0
                    progress.call(i,rows.length)
                end
                puts "###############################################################"
                puts "Importing row #{i}/#{rows.length}"
                t0=Time.now()
                id=self.import_row row
                t1=Time.now()
                puts "Imported row in #{t1-t0} s"
            rescue
                logger=DMS_OpenERP.get_logger
                logger.error "Failed to import row: #{row.inspect}"
                logger.error "error: #{$!}"
                logger.error "backtrace: #{$!.backtrace.join "\n"}"
            end
        end
    end

    def check_import(date_from,date_to)
        return nil
    end

    def check_missing(dms_docnos,erp_docnos)
        erp_docnos=erp_docnos.to_set 
        missing=Array.new
        dms_docnos.each do |doc_no|
            if not erp_docnos.include? doc_no
                missing.push(doc_no)
            end
        end
        return missing
    end

    def set_payment_method(invoice_id,pmt_method)
        # use for invoice

        @erp.execute("account.invoice","unlink_method",invoice_id)

        # set only cash
        vals={}
        # loop for all methods
        cash_moves=[]
        for r in pmt_method[1]

            case r["method"]
                when "cash"
                    cash_moves << [0,0,r["value"]]

                when "creditcard"

                    res=@erp.execute("account.invoice","read",invoice_id,["partner_id"])
                    partner_id=res["partner_id"][0]


                    r["value"]["partner_id"] = partner_id # Need partner_id to set because in get_payment have no partner_id
                    vals["cheques"]=[[0,0,r["value"]]]
            end
        end
        vals["cash_moves"] = cash_moves

        @erp.execute("account.invoice","write",[invoice_id],vals)

        return invoice_id

    end

end
