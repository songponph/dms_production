require "dms"
require "erp"
require "importer"

class Importer_CarProduct < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND REGISTRATION_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND REGISTRATION_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( MTOC_CD='#{number}' ) ": "")

        query="SELECT * FROM SM_MTOC WHERE ACTIVE_FLG=1 #{cause}"

        return query
    end

    def import_by_no(model_no)
        q=self.query_get(model_no)
        res=@dms.get q
        raise "Car model not found: #{model_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        #t0=(Time.now-3600*24*2).strftime "%Y-%m-%d"
        #q=self.query_get(false,t0)
        #rows=@dms.query q
        #self.import_rows rows,progress
        return
    end

    #def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d"
        #q=self.query_get(false,t0)
        #rows=@dms.query q
        #self.import_rows rows,progress
    #end

    def import_row(row)
        puts "Importer_CarProduct.import_row"
        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product(model_no)
        if product_id and @no_update
            @logger.info "Car model #{model_no} already created"
            return
        end
        vals={
            "default_code"=>model_no,
            "name"=>(row["MTO_NM"] or row["MODEL_NM"] or "/").strip,
            "sale_ok"=>false,
            "purchase_ok"=>false,
            "car_model_name"=> row["MODEL_NM"],
            "variants"=>(row["COLOR_NM"] or "").strip,
            "type"=>"product",
            "cost_method"=>"lot",
            "valuation"=>"real_time",
            "categ_id"=>@erp.get_product_category("Cars",:required=>true),
            "list_price"=>row["CARLISTPRICE_CR"]||0.0,
            "car_color_cd"=>row["COLOR_CD"]||"",
            "car_color"=>row["COLOR_NM"]||"",
            "car_type"=>row["TYPE_NM"]||"",
            "car_grade"=>row["GRADE_NM"]||"",
            "car_model_year"=>row["MODELYEAR"]||"",
            "car_displacement"=>row["DISPLACEMENT"]||"",
            "car_fuel"=>row["FUEL_NM"]||"",
            "car_doors"=>row["DOORS_CNT"]||"",
            "car_interior_cd"=>row["INTERIOR_CD"]||"",
            "car_interior_nm"=>row["INTERIOR_NM"]||"",
            "car_price"=>row["CARPRICE_CR"]||"",
            "car_vat"=>row["CARVAT_CR"]||"",
            "car_ac_price"=>row["ACPRICE_CR"]||"",
            "car_ac_vat"=>row["ACVAT_CR"]||"",
            "car_wholeprice"=>row["WHOLEPRICE_CR"]||"",
            "car_wholevat"=>row["WHOLEVAT_CR"]||"",
            "car_listprice"=>row["CARLISTPRICE_CR"]||"",
            "car_listvat"=>row["CARLISTVAT_CR"]||"",
            "car_ac_listprice"=>row["ACLISTPRICE_CR"]||"",
            "car_ac_listvat"=>row["ACLISTVAT_CR"]||"",
            "car_wholelistvat"=>row["WHOLELISTVAT_CR"]||"",
            "car_transportation_price"=>row["TRANSPORTATION_CR"]||"",
            "car_insurance_price"=>row["INSURANCE_CR"]||"",
        }
        if product_id
            @erp.execute "product.product","write",[product_id],vals
            @logger.info "SKIPPED: Car model #{model_no} updated => #{product_id}"
        else
            new_id=@erp.execute "product.product","create",vals
            @logger.info "CREATED : Car model #{model_no} created => #{new_id}"
        end
        product_id
    end
end
