require "dms"
require "erp"
require "importer"
require "pp"

class Importer_PartsDeliveredCustomerService < Importer
    #TODO: improve speed

    def query_get(number,date_from=false,date_to=false,last_update=false)
        cause= date_from!=false ?  " AND p.DLVR_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND p.DLVR_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( p.RO_NO='#{number}' or p.FRAME_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND p.LAST_UPDATE_DATE>='#{last_update}' ": "")
        list=[]
        for table in ["RR","RT"]
            list<<"
            (
            select p.RO_NO as ORIGIN,
            rtrim(UPPER(p.CUSTOMER_NO)) as PARTNER_NO,
            p.DLVR_DATE AS DATE_DONE,
            p.FOC_FLG,
            p.ACC_FLG,
            p.FRAME_NO,
            RTRIM(p.JOB_MEMO) AS NOTE,
            pt.CAR_NM,
            pt.INPORT_FLG as IMPORT_FLG,

            m.PARTS_NO as PRODUCT_ID,m.PARTS_SEQ,m.INCOME_QTY as PRODUCT_QTY, coalesce(m.COST_PRC,0.0) as PRICE_UNIT

            FROM #{table}_REPAIRORDER p
            JOIN #{table}_REPAIR_P m on ( m.RO_NO = p.RO_NO )
            JOIN SM_VEHICLEINFO v on (v.FRAME_NO=p.FRAME_NO)
            JOIN SM_MTOC pt on (pt.MTOC_CD=v.MTOC_CD)
            WHERE p.DLVR_DATE is not null and m.INCOME_QTY!=0
            and m.RETURN_QTY=0
            #{cause}
            )
            "
        end
        sub_query = list.map! { |k| "#{k}" }.join(" UNION ")
        query = "SELECT * from (#{sub_query}) p ORDER BY p.DATE_DONE,p.ORIGIN"
        return query
    end

    def import_by_no(doc_no)
        q=self.query_get(doc_no)
        res=@dms.query q
        raise "Job order not found: #{doc_no}" if not res
        self.import_row(res)
        return
    end

    def import_by_date(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_row rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d 00:00:00"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_row rows
    end

    def import_realtime(from_t,progress=nil)

        t0=from_t.strftime "%Y-%m-%d %H:%M:00"

        # try from the begining of the day
        #t0=from_t.strftime "%Y-%m-%d 00:00:00"
        #
        # 1 hour back
        #t0=(from_t-3600).strftime "%Y-%m-%d %H:%M:00"

        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_row rows
    end

    def check_import(date_from,date_to)
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        miss=Array.new
        for row in rows
            invoice=row["ORIGIN"].strip
            inv_id=@erp.get_picking_id invoice,"out",@company_id
            if not inv_id
                miss.push(invoice)
            end
        end
        return miss
    end

    def format_row(rows)
        summary = {}
        rows.each do |rec|
          key= rec.values_at("DATE_DONE","ORIGIN","PARTNER_NO","FOC_FLG","ACC_FLG","FRAME_NO","CAR_NM","IMPORT_FLG","NOTE")
          summary[key] ||= []
          summary[key] << rec
        end
        return summary
    end


    def import_row(row)
        result=[]
        datas =self.format_row(row)
        for d in datas.keys.sort
            date,origin, partner_no,foc_flg,acc_flg,frame_no,car_nm,import_flg,note= d
            res=@erp.execute "stock.picking","search",[["origin","=",origin],['company_id','=',@company_id]]
            if not res.empty?
                @logger.info "SKIPPED : Picking already created: #{origin} - #{date.strftime("%Y-%m-%d")}"
                next
            end

            partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

            address_id=@erp.get_partner_address(partner_id,:type=>"delivery",:required=>true)
            if not address_id
                address_id=@erp.get_partner_address(partner_id,:required=>true)
            end

            date=date.strftime "%Y-%m-%d %H:%M:%S"

            notes=[]
            analytic_id=false
            if foc_flg == 1
                notes+=["FOC"]

                analytic_id=@erp.get_analytic_from_code "FOC_" , car_nm ,@company_id,import_flg
            end
            if acc_flg == 1
                notes+=["ACC"]
            end
            if not note.empty?
                notes << note
            end
            note = notes.map! { |k| "#{k}" }.join("\n")
            note = note.sub("\u001E","")

            pick_vals={
                "origin"=>origin,
                "date"=>date,
                "date_done"=>date,
                "move_type"=>"direct",
                "company_id"=>@company_id,
                "invoice_state"=>"none",
                "type"=>"out",
                "address_id"=>address_id,
                "stock_journal_id"=>@erp.get_stock_journal("PARTS_DELIVERED_CUSTOMER_SERVICE_JOURNAL",@company_id),#PART_OUT
                "note"=>note,
            }

            frame_no=frame_no.strip
            vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

            lines=get_lines(datas[d],vehicle_id)
            if lines.empty?
                @logger.info "SKIPPED : No parts for job #{origin} => Don't create picking"
                next
            end
            pick_vals["move_lines"]=lines.collect {|line_vals| [0,0,line_vals]}

            pick_id=@erp.execute "stock.picking","create",pick_vals
            @logger.info "CREATED : picking #{origin}=>#{pick_id} : #{date}"
            @erp.exec_workflow "stock.picking","button_confirm",pick_id
            @erp.exec_workflow "stock.picking","button_done",pick_id
            result << pick_id
        end
        return result
    end

    def get_lines(rows,vehicle_id)
        lines=[]
        for row in rows
            date=row["DATE_DONE"].strftime "%Y-%m-%d %H:%M:%S"
            part_no=row["PRODUCT_ID"].strip.upcase
            product_id=@erp.get_product part_no,@branch_code,"part_product",:required=>true
            line_no=row["PARTS_SEQ"].to_s
            qty=row["PRODUCT_QTY"]
            vals={
                "name"=>row["ORIGIN"]+" "+line_no,
                "date"=>date,
                "date_expected" => date,
                "product_id"=>product_id,
                "product_uom"=>@erp.get_uom("PCE",:required=>true),
                "product_qty"=>qty,
                "location_id"=>@erp.get_location(@branch_code+"-Production",:required=>true),
                "location_dest_id"=>@erp.get_location("Customers",:required=>true),
                "state"=>"assigned",
                "vehicle_id"=>vehicle_id,
                "price_unit"=>row["PRICE_UNIT"],
            }
            lines << vals
        end
        lines
    end
end
