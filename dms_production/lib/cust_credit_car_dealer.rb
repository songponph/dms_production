require "dms"
require "erp"
require "importer"
require "pp"
#S2 credit note for deale
#SZ Invoice for dealer
#SUPPLIER_KBN='02' Dealer(use this)

class Importer_CreditCarDealer < Importer

    def query_get(number,date_from=false,date_to=false,last_update=false)
        #puts "CustInvoiceService query get", number ,date_from, date_to,
        cause= date_from!=false ?  " AND INV_CN_PRINT_DATE>='#{date_from} 00:00:00' " : ""
        cause << (date_to!=false ? " AND INV_CN_PRINT_DATE<='#{date_to} 23:59:59' ": "")
        cause << (number!=false ?  " AND ( INVOICE_CN_NO='#{number}' ) ": "")
        cause << (last_update!=false ? " AND LAST_UPDATE_DATE>='#{last_update}' ": "")
        query="
            SELECT INVOICE_CN_NO,RCVORDER_NO
            ,CLAIM_CUST_NO,MTOC_CD
            ,INV_CN_PRINT_DATE
            ,INVOICE_NO,FRAME_NO,CANCEL_CD,STAFF_CD,TOTAL_CR
            FROM ST_RORDERINV
            WHERE SUPPLIER_KBN='02' and ACTIVE_FLG=0
        "+cause+" ORDER BY INVOICE_CN_NO "
        return query
    end

    def import_by_no(creditnote_no)
        q=self.query_get(creditnote_no)
        res=@dms.get q
        raise "Invoice not found: #{doc_no}" if not res
        self.import_row(res)
    end

    def import_by_date(date_from,date_to="2100-01-01")
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        self.import_rows rows
    end

    def import_daily(progress=nil)
        t0=(Time.now-3600*24*2).strftime "%Y-%m-%d %H:%M:%S"
        q=self.query_get(false,false,false,t0)
        rows=@dms.query q
        self.import_rows rows,progress
    end

    def import_realtime(from_t,progress=nil)
        #t0=from_t.strftime "%Y-%m-%d %H:%M:%S"
        #q=self.query_get(false,false,false,t0)
        #rows=@dms.query q
        #self.import_rows rows,progress
        return
    end

    def check_import(date_from,date_to)
        puts "Check_import_S2",date_from,date_to
        q=self.query_get(false,date_from,date_to)
        rows=@dms.query q
        dms_docnos=rows.collect {|row| row["INVOICE_CN_NO"].strip}
        erp_docnos=@erp.get_invoice_docnos "out_refund",date_from,date_to,"S2%",@company_id
        miss=check_missing dms_docnos,erp_docnos
        return miss
    end

    def import_row(row)
        puts "Importer_CustCreditCarDealer.import_row"
        pp "row:",row
        invoice_id=self.create_invoice(row)
        return invoice_id
    end

    def create_invoice(row)
        puts "Customer credit car dealer....."
        origin=row["INVOICE_CN_NO"].strip+" "+row["RCVORDER_NO"].strip

        res=@erp.execute "account.invoice","search",[["origin","=",origin]]
        if not res.empty?
            @logger.info "Invoice already created:#{origin}"
            return
        end

        partner_no=row["CLAIM_CUST_NO"].strip.upcase
        partner_id=@erp.get_partner partner_no,@company_id,@branch_code,:required=>true

        model_no=row["MTOC_CD"].strip
        product_id=@erp.get_product model_no,@branch_code,"car_product",:required=>true

        res=@erp.execute "product.product","read",[product_id],["name","variants"]
        name=res[0]["name"]
        variant=(res[0]["variants"] or "")
        prod_name=name+" "+variant

        tax_id=@erp.get_tax "OUT_VAT7_INCL",:required=>true

        date_invoice=row["INV_CN_PRINT_DATE"].strftime("%m/%d/%y")

        original_invoice=row["INVOICE_NO"].strip+" " +row["RCVORDER_NO"].strip
        original_invoice_id=@erp.get_origin_invoice original_invoice,@company_id,@branch_code,"out_invoice","cust_invoice_car_dealer",row["INVOICE_NO"].strip,:required=>true

        res=@erp.execute "account.invoice","read",[original_invoice_id],["address_invoice_id","user_id"]
        address_id=res[0]["address_invoice_id"][0]

        frame_no=row["FRAME_NO"]
        vehicle_id=@erp.get_vehicle frame_no,@branch_code,:required=>true

        cancel_cd=row["CANCEL_CD"]
        credit_reason=@dms.get_credit_note_reason cancel_cd,:required=>true
        user_code=row["STAFF_CD"].strip
        user_id=@erp.get_user user_code,:required=>true

        vals_={
            "origin" => origin,
            "reference" => row["INVOICE_CN_NO"].strip,
            "comment"=>credit_reason,
            "journal_id" => @erp.get_journal("CUST_CREDIT_CAR_DEALER_JOURNAL",@company_id,:required=>true),
            "partner_id" => partner_id,
            "company_id" => @company_id,
            "type" => "out_refund",
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_DEALER_RECEIVABLE",@parent_company_id,:required=>true),
            "date_invoice" => date_invoice,
            "doc_date"=>date_invoice,
            "user_id"=>user_id,
            "orig_invoice_id" => original_invoice_id,
            "address_invoice_id" => address_id,
            "vehicle_id"=>vehicle_id,
        }

        lines_= {
            "uos_id" => @erp.get_uom("PCE",:required=>true),
            "account_id" => @erp.get_account("CUST_CREDIT_CAR_DEALER_INCOME",@parent_company_id,:required=>true),
            "name" => prod_name,
            "quantity" => 1,
            "price_unit" => row["TOTAL_CR"],
            "invoice_line_tax_id" => [[6,0,[tax_id]]],
            "product_id" => product_id
        }

        vals_["invoice_line"]=[[0,0,lines_]]

        pp "vals:",vals_
        pp "lines:",lines_

        inv_id=@erp.execute "account.invoice","create",vals_
        @logger.info "invoice created: #{origin}=>#{inv_id}"

        puts "Computing invoice.........."
        @erp.execute "account.invoice","button_compute",[inv_id]

        res=@erp.execute "account.invoice","read",[inv_id],["amount_total"]
        amt=res[0]["amount_total"]
        if (amt-row["TOTAL_CR"]).abs>0.25
            msg="wrong total in #{origin}: #{amt}/#{row["TOTAL_CR"]}"
            @logger.error msg
            raise msg
        end
        puts "Posting invoice............."
        @erp.exec_workflow "account.invoice","invoice_open",inv_id
        return inv_id
    end
end
    
