#!/bin/bash
cd ~/dms/dms_production/scripts

#docs="
#sublets_received_purchase
#part_excess
#part_loss
#parts_delivered_customer_counter
#parts_returned_customer_service
#parts_returned_jobs
#parts_used_jobs
#parts_received_suppliers
#parts_returned_supplier
#parts_returned_branch
#parts_delivered_branch
#parts_received_other_branch
#"

docs="
sublets_received_purchase
parts_delivered_customer_counter
parts_delivered_customer_service
parts_returned_customer_service
parts_returned_jobs
parts_received_suppliers
parts_returned_supplier
parts_returned_branch
parts_delivered_branch
parts_received_other_branch
"

#docs="
#sublets_received_purchase
#"
#parts_delivered_customer_service

#date_start="$(date -d 'last month' +%Y-%m-%d)"
date_start="$(date -d 'last week' +%Y-%m-%d)"
date_end="$(date -d 'yesterday' +%Y-%m-%d)"

for d in $docs;
    do
    date=`date +%Y-%m-%d\ %H:%M`
    echo $1 $d $date
    #./import_dms.sh  -t $d -s $date_start -e $date_end
    #./import_dms.sh  -t $d -s 2016-12-02 -e $date_end
    #./import_dms.sh  -t $d -s 2018-05-30 -e 2018-05-31
    ./import_dms.sh  -t $d -b SW -s 2018-05-30 -e 2018-05-31
    #./import_dms.sh  -t $d -b SW -s 2018-05-01 -e 2018-05-31 -n MRHGM6620KP100608

    done
