#!/bin/bash
cd ~/dms/dms_production/scripts

docs="
cars_received_supplier
cars_returned_supplier
cars_received_branch
cars_returned_branch
cars_delivered_customer
cars_delivered_dealer
cars_delivered_branch
cars_returned_customer
cars_returned_dealer
"

#date_start="$(date -d 'last month' +%Y-%m-%d)"
date_start="$(date -d 'last week' +%Y-%m-%d)"
date_end="$(date -d 'yesterday' +%Y-%m-%d)"

for d in $docs;do
    echo $d
    #./import_dms.sh -t $d -s $date_start -e $date_end
    ./import_dms.sh -t $d -s 2017-12-17 -e 2017-12-22

done
