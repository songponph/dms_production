#!/bin/bash
cd ~/dms/dms_production/scripts

docs="
partner_tax_info
"

#date_start="$(date -d 'last month' +%Y-%m-%d)"
date_start="$(date -d 'last week' +%Y-%m-%d)"
date_end="$(date -d 'yesterday' +%Y-%m-%d)"

for d in $docs;do
    echo $d
    ./import_dms.sh -t $d -s $date_start -e $date_end

done
