#!/usr/bin/env ruby
require "rubygems"
require "dms_openerp"
require "optparse"
require "pp"
require "dms"
require "json"
require "sequel"

options={}

optparse=OptionParser.new do |opts|
    opts.on("-t","--type DOC_TYPE","Document type") do |v|
        options["doc_type"]=v
    end
    opts.on("-b","--branch BRANCH_CODE","Branch code") do |v|
        options["branch_code"]=v
    end
    opts.on("-d","--daily","Daily import") do ||
        options["daily"]=true
    end
    opts.on("-n","--number NUMBERS","Document numbers") do |v|
        options["doc_no"]=v
    end
    opts.on("-f","--file FILE","Number file") do |v|
        options["doc_no_file"]=v
    end
    opts.on("-s","--start DATE","Start date") do |v|
        options["date_from"]=v
    end
    opts.on("-e","--end DATE","End date") do |v|
        options["date_to"]=v
    end
    opts.on("-r","--reverse","Reverse date order") do |v|
        options["reverse"]=true
    end
    opts.on("-p","--period PERIOD","Period (month/week/day)") do |v|
        options["period"]=v
    end
    opts.on("-c","--no_update","No-update") do |v|
        options["no_update"]=true
    end
    opts.on("-l","--realtime","Real-time import") do |v|
        options["realtime"]=true
    end
    opts.on("-C","--check","Check import") do |v|
        options["check"]=true
    end
end.parse!
puts "options:",options.inspect

if options["doc_type"]
    doc_types=options["doc_type"].split ","
    puts doc_types
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
else
    doc_types=DMS_OpenERP.get_all_doc_types
end

if options["branch_code"]
    branches=options["branch_code"].split ","
    puts branches
else
    branches=DMS_OpenERP.get_all_branches
end

if options["check"]
    puts "Checking import"
    puts "#############################################################################"
    if not options["date_from"]
        raise "Do you want to check from which date to date, please put -s stat_date -e end_date.If you miss end date, it will automatically assume today date"
    end
    date_ranges=[]
    date_from=Date.strptime options["date_from"]
    date_to=Date.strptime(options["date_to"]||Date.today.to_s)
    d0=date_from 
    while d0<=date_to # split date range in periods in order to use less memory
        if options["period"]=="day"
            d1=d0
        elsif options["period"]=="week"
            d1=d0+6
        else
            d1=(d0>>1)-1
        end
        if d1>=ndate_to
            d1=date_to
        end
        date_ranges<<[d0.to_s,d1.to_s]
        d0=d1+1
    end
    pp "date_ranges:",date_ranges

    log=Logger.new("/tmp/check_import.log")
    for d0,d1 in date_ranges
        for branch in branches
            for doc_type in doc_types
                imp=DMS_OpenERP.get_importer doc_type
                imp.start_import "branch_code"=>branch
                missing=imp.check_import d0,d1
                if missing.nil?
                    next
                end
                log.info "Checking import: branch=#{branch} doc_type=#{doc_type} date_from=#{d0} date_to=#{d1}"
                if missing.empty?
                    log.info "==> Nothing is missing"
                else
                    log.info "==> Missing documents:"
                    log.info missing.join(",")
                end
            end
        end
    end
elsif options["date_from"] or options["date_to"]
    date_ranges=[]
    date_from=Date.strptime options["date_from"]
    date_to=Date.strptime(options["date_to"]||Date.today.to_s)
    d0=date_from
    while d0<=date_to # split date range in periods in order to use less memory
        if options["period"]=="day"
            d1=d0
        elsif options["period"]=="week"
            d1=d0+6
        else
            d1=(d0>>1)-1
        end
        if d1>=date_to
            d1=date_to
        end
        date_ranges<<[d0.to_s,d1.to_s]
        d0=d1+1
    end
    date_ranges.reverse! if options["reverse"]
    pp "date_ranges:",date_ranges

    for d0,d1 in date_ranges
        for branch in branches
            for doc_type in doc_types
                imp=DMS_OpenERP.get_importer doc_type
                puts branch
                puts "#############################################################################"
                imp.start_import "branch_code"=>branch,"no_update"=>options["no_update"]
                puts "Importing rows: branch=#{branch} doc_type=#{doc_type} date_from=#{d0} date_to=#{d1}"
                imp.import_by_date d0,d1
                puts "!!!!!!!!!!!!!!!!!!!"
            end
        end
    end
elsif options["daily"]
    log=Logger.new("/tmp/daily.log")
    for branch in branches
        log.info "branch: #{branch}"
        for doc_type in doc_types
            if doc_type=="partner"
                next
                end

            log.info "  doc_type: #{doc_type}"
            imp=DMS_OpenERP.get_importer doc_type
            puts "#############################################################################"
            puts "Importing rows: branch=#{branch} doc_type=#{doc_type} daily"
            no_update=options["no_update"]
            if doc_type=="part_product" or doc_type=="car_product"
                no_update=true # XXX: otherwise too slow
            end
            imp.start_import "branch_code"=>branch,"no_update"=>no_update
            progress=Proc.new do |curr,total|
                log.info "    #{curr} / #{total}"
            end
            imp.import_daily progress
        end
    end
elsif options["doc_no"]
    puts "Importing doc_type #{doc_type}..."
    doc_nos=options["doc_no"].split ","
    imp=DMS_OpenERP.get_importer doc_types[0]
    imp.start_import "branch_code"=>branches[0],"no_update"=>options["no_update"]
    doc_nos.each_with_index do |doc_no,i|
        puts "X#############################################################################"
        puts "Importing row #{i}/#{doc_nos.length}: branch=#{branch} doc_type=#{doc_type} doc_no=#{doc_no}"
        imp.import_by_no doc_no
    end
elsif options["doc_no_file"]
    puts "Importing doc_type #{doc_type}..."
    f=File.open(options["doc_no_file"],"rb")
    doc_nos=f.read.split "\n"
    f.close
    doc_nos.uniq!
    imp=DMS_OpenERP.get_importer doc_types[0]
    imp.start_import "branch_code"=>branches[0],"no_update"=>options["no_update"]
    doc_nos.each_with_index do |doc_no,i|
        begin
            puts "#############################################################################"
            puts "Importing row #{i}/#{doc_nos.length}: branch=#{branch} doc_type=#{doc_type} doc_no=#{doc_no}"
            imp.import_by_no doc_no
        rescue
            logger=DMS_OpenERP.get_logger
            logger.error "Import failed: #{doc_no} #{$!}"
        end
    end
elsif options["realtime"]

    while true
        t=Time.now.strftime "%H:%M:%S"

        if t<"07:30:00" or t>"19:00:00"
            sleep 60
            next
        end

        for branch in branches

            log=Logger.new("/tmp/realtime_#{branch}.log")

            DB = Sequel.connect('jdbc:sqlite:realtime.db')
            realtime = DB[:realtime]
            b = realtime[:name => branch ]

            last_time = ''

            if b.nil?
                # if not branch import info exist, create one
                last_time = Time.new.strftime("%Y-%m-%d 00:00:00")
                realtime << {:name => branch, :time => last_time }
            else
                last_time = b[:time]
            end
            last_time =  Time.strptime(last_time, "%Y-%m-%d %H:%M:%S")

            begin
                log.info "branch: #{branch}"
                dms=DMS.get_connection(branch)

                res=dms.get "select current_timestamp as t"
                dms_time =res["t"] #get time stamp of DMS mssql

                for doc_type in doc_types
                    log.info "  doc_type: #{doc_type}"
                    imp=DMS_OpenERP.get_importer doc_type
                    puts "#############################################################################"
                    puts "Importing rows: branch=#{branch} doc_type=#{doc_type} realtime"
                    no_update=options["no_update"]
                    if doc_type=="part_product" or doc_type=="car_product"
                        no_update=true # XXX: otherwise too slow
                    end
                    imp.start_import "branch_code"=>branch,"no_update"=>no_update
                    progress=Proc.new do |curr,total|
                        log.info "    #{curr} / #{total}"
                    end
                    imp.import_realtime last_time,progress  # start import with last import time
                end


                puts "***************************************************************************"
                puts "***************************************************************************"
                puts "***************************************************************************"
                puts "Current Import Time #{dms_time.strftime("%Y-%m-%d %H:%M:%S")} "

                # after finish import set last import time
                # using dms timestamp
                realtime.where(:name=>branch).update(:time => dms_time.strftime("%Y-%m-%d %H:%M:%S"))
                DB.disconnect

            rescue
                log.error "Import failed for branch #{branch}!"
                log.error "#{$!}"
                log.error "#{$@}"
                sleep 60
            end
        end
        log.info "sleeping..."
        sleep 30
    end
else
    raise "Missing import method"
end

