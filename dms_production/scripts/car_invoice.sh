#!/bin/bash
cd ~/dms/dms_production/scripts

docs="cust_invoice_car_booking
cust_credit_car_booking
cust_invoice_car_down
cust_credit_car_down
cust_invoice_car_full
cust_credit_car_full
cust_invoice_redplate
cust_invoice_car_dealer
cust_credit_car_dealer"

#date_start="$(date -d 'last month' +%Y-%m-%d)"
date_start="$(date -d 'last week' +%Y-%m-%d)"
date_end="$(date -d 'yesterday' +%Y-%m-%d)"

for d in $docs;do
date=`date +%Y-%m-%d\ %H:%M`
echo $b1 $d $date

    #./import_dms.sh -t $d -s $date_start -e $date_end
    ./import_dms.sh -t $d -s 2017-12-15 -e 2017-12-31

done
