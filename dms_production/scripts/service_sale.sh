#!/bin/bash
cd ~/dms/dms_production/scripts

docs="
cust_invoice_service
cust_credit_service
cust_invoice_ult
cust_credit_ult
cust_invoice_claim_free_service
cust_invoice_part_deposit
cust_credit_part_deposit
cust_invoice_parts
cust_credit_parts
cust_invoice_part_branch
cust_credit_parts_branch
cust_invoice_paysave
cust_credit_paysave
"

#date_start="$(date -d 'last month' +%Y-%m-%d)"
date_start="$(date -d 'yesterday' +%Y-%m-%d)"
date_end="$(date -d 'yesterday' +%Y-%m-%d)"

for d in $docs;do
    echo $d
    #./import_dms.sh -t $d -s $date_start -e $date_end
    ./import_dms.sh -t $d -b YR -s 2018-07-03 -e 2018-07-04

done
