#!/bin/bash

cd ~/dms/dms_production/scripts

docs="
supp_invoice_car
supp_credit_car
supp_invoice_redplate
supp_invoice_part_branch
supp_invoice_parts
sublets_received_purchase"

#date_start="$(date -d 'last month' +%Y-%m-%d)"
date_start="$(date -d 'last week' +%Y-%m-%d)"
date_end="$(date -d 'yesterday' +%Y-%m-%d)"

for d in $docs;do
date=`date +%Y-%m-%d\ %H:%M`
echo $b1 $d $date

    #./import_dms.sh -t $d -s $date_start -e $date_end
    ./import_dms.sh -b SP -t $d -s 2018-08-21 -e 2018-08-23
    #./import_dms.sh -b SP -t $d -n PF1805V0523
    #./import_dms.sh -t $d -s 2018-04-29 -e 2018-05-02

done
