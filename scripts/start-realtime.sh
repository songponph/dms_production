#!/bin/bash

#sess=realtime
#dir=/home/almacom/dms

#if [[ $(screen -ls | grep $sess) ]] ; then
    #echo "session $sess already running"
    #exit 1
#fi

#screen -d -m -S $sess -t main /bin/bash

#branches="SP BM YR DHA PHC SW R3N"
#for b in $branches ; do
    #echo starting $b
    #screen -r $sess -X screen -t $b /bin/bash
    #screen -r $sess -p $b -X stuff "supervise $dir/svc/${b,,}"
    #sleep 5
#done

#echo "starting session"

sess=dms


if [ "`tmux ls | grep $sess`" != "" ]  ; then

    echo "session $sess already running"
    exit 1
fi

echo "starting session"

tmux new-session -d -s $sess



branches="sp bm yr dha phc sw r3n"
for b in $branches ; do
    echo starting $b

    tmux new-window -n $b

    tmux send-keys 'supervise /home/almacom/dms/svc/'$b 'C-m'

    sleep 5
done


sleep 2

