#!/bin/sh
branch=$1
cd /home/almacom/dms_production/scripts
date_from=`date -d "1 day ago" +%Y-%m-%d`
jruby -J-Xmx100m  import_dms.rb -C -b $branch -s $date_from
