<h3>DMS Import Check</h3>

<?
$data=file_get_contents("/home/almacom/realtime/last_import.json");
$last_imp=json_decode($data,true);

function get_import_time($branch) {
    global $last_imp;
    return $last_imp[$branch];
}

function get_check_time($branch) {
    return date ("Y-m-d H:i:s", filemtime("$branch.html"));
}
?>

<ul>
<? foreach(array("SP","PHC","R3N","BM","DHA","SW") as $branch) { ?>
<li>
<a href="<? echo $branch; ?>.html"><? echo $branch; ?></a>
    <ul>
    <li>
    last import time: <? echo get_import_time($branch) ?>
    </li>
    <li>
    last check time: <? echo get_check_time($branch) ?>
    </li>
    </ul>
</li>
<? } ?>
</ul>
