#!/usr/bin/python
import smtplib
import time
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import re
import sys

if len(sys.argv)>1:
    branch=sys.argv[1]
    branches=[branch]
else:
    branches=[
    "SP",
    "PHC",
    "R3N",
    "BM",
    "DHA",
    "SW",
    ]

for branch in branches:
    print "******************************************************************"
    print "Checking branch %s"%branch
    os.system("rm /tmp/check_import.log")
    os.system("/home/almacom/check_import.sh %s"%branch)

    log=file("/tmp/check_import.log").read()

    from_addr="donotreply@almacom.co.th"
    to_addrs=[
        "hd.support@almacom.co.th",
    ]
    today=time.strftime("%Y-%m-%d")

    html="""
    <h3>Import check for %s <small>(check time: %s)</small></h3>
    """%(branch,time.strftime("%Y-%m-%d %H:%M:%S"))

    html+='<table border="1">'

    has_missing=False
    is_empty=True
    for l in log.split("\n"):
        m=re.search("branch=(.*?) doc_type=(.*?) date_from=(.*?) date_to=(.*?)",l)
        if m:
            rest=m.group(0)
            html+="<tr>"
            html+="<td>%s</td>"%rest
            html+="</tr>"
        else:
            i=l.find("-- :")
            if i==-1:
                continue
            rest=l[i+4:]
            if re.search("Nothing is missing",l):
                html+='<tr><td colspan="4" style="background-color:green">%s</td></tr>'%rest
                is_empty=False
            elif re.search("Missing documents",l):
                has_missing=True
                html+='<tr><td colspan="4" style="background-color:red;font-weight:bold">%s</td></tr>'%rest
                is_empty=False
            else:
                html+='<tr><td colspan="4" style="background-color:red;font-weight:bold">%s</td></tr>'%rest

    html+="</table>"

    if not is_empty:
        file("/home/almacom/check_import/%s.html"%branch,"w").write(html)

    msg=MIMEMultipart("alternative")
    subj="Subject: Import check for %s"%branch
    if has_missing:
        subj+=" (MISSING)"
    if is_empty:
        subj+=" (CHECK FAILURE)"
    msg["Subject"]=subj
    msg["From"]=from_addr
    msg["To"]=",".join(to_addrs)

    part=MIMEText(html,"html")
    msg.attach(part)

    server=smtplib.SMTP("smtp.mailgun.org",26)
    server.sendmail(from_addr,to_addrs,msg.as_string())
    server.quit()
